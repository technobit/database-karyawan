<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		$this->authCheck();
		
		$this->load->model('app_model', 'model');
		$this->load->model('laporan/absen_tk_model', 'absen');

    }
	
	public function index(){
		$this->page->subtitle = 'Dashboard';
		$this->page->menu = 'dashboard';
        $this->breadcrumb->title = 'Dashboard';;
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = false;
		$data = array();
		$data['k1'] = $this->model->get_akhir_kontrak1();
		$data['k2'] = $this->model->get_akhir_kontrak2();
		//$data['tk'] = 0;//$this->model->get_tk();
		$this->render_view('index', $data, false);
	}

	/*public function list(){
		$this->authCheckDetailAccess('r'); 

		$filter_start		= date('Y-m-d 00:00:00', strtotime(date('Y-m-d'). ' -29 days'));
		$filter_end			= date('Y-m-d 23:59:59');

		//$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        //$filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$return_tk = array();
		$ldata = $this->absen->list($filter_start, $filter_end);

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$arr_tgl = explode(",", str_replace(" ", "", $d->tanggal));
			$arr_kode = explode(",", str_replace(" ", "", $d->var_kode_pokjakd));
			$check_tk = $this->check_tk($arr_tgl, $arr_kode);

			if(!empty($check_tk)){
				$i++;
				$return_tk[] = $check_tk;
				$data[] = array($i, $d->var_nik, $d->var_nama, $check_tk['count_tk'].' Hari', implode("; ",str_replace(" ", "",$check_tk['tgl_tk'])), implode("; ",$check_tk['data_tk']));
			}
		}
		$count_kar_tk = $i;
		$this->set_json(array( 'stat' => TRUE,
								'aaData' => $data,
								'karyawan_tk' => $count_kar_tk,
								'return_tk' => $return_tk,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}


	public function check_tk($arr_tgl, $arr_kode){
		$val_tk = 0;
		$data  = array();
		$data_tk = array();
		$tgl_tk = array();

		$xlimit = count($arr_tgl);
		for ($x = 0 ; $x < $xlimit; $x++){
			$date_now = $arr_tgl[$x];

			if(isset($arr_tgl[$x-1])){ $date_mundur1 = $arr_tgl[$x-1];
				$cek_mundur1 = $this->diff_check($date_now, $date_mundur1);
			}else{ $cek_mundur1 = 0; }

			if(isset($arr_tgl[$x+1])){ $date_maju1 = $arr_tgl[$x+1];
				$cek_maju1 = $this->diff_check($date_now, $date_maju1);
			}else{ $cek_maju1 = 0; }

			if(isset($arr_tgl[$x-2])){ $date_mundur2 = $arr_tgl[$x-2];
				$cek_mundur2 = $this->diff_check($date_now, $date_mundur2);
			}else{ $cek_mundur2 = 0; }

			if(isset($arr_tgl[$x+2])){ $date_maju2 = $arr_tgl[$x+2];
				$cek_maju2 = $this->diff_check($date_now, $date_maju2);
			}else{ $date_maju2 = 0; }


			if(($cek_mundur1  == '1' && $cek_maju1 == '1') || ($cek_mundur1  == '1' && $cek_mundur2 == '2') || ($cek_maju1  == '1' && $cek_maju2 == '2')){
				array_push($data_tk,$arr_kode[$x]);
				array_push($tgl_tk, idn_date($arr_tgl[$x], "d/m/Y"));
				$val_tk = $val_tk + 1;
			}
		}

		if($val_tk >= 3){
			$count_arr = array_count_values($data_tk);
			
			if(isset($count_arr['TK'])){
				$count_tk = $count_arr['TK'];
				if($count_tk >= 3){
					$data['count_tk'] = $count_tk;
					$data['data_tk'] = $data_tk;
					$data['tgl_tk'] = $tgl_tk;
					return $data;
				}
			}
		}
		return false;
	}
	
	function diff_check($dt1 = '', $dt2 = ''){
		if($dt1 != '' || $dt2 != ''){
			$date1 = date_create($dt1);
			$date2 = date_create($dt2);
			$diff = date_diff($date1,$date2);
			return $diff->format("%a");
		}else{
			return 0;
		}
	}*/
}
