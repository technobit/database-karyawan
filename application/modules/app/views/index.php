<?//=print_r($data)?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">

      <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title mt-1">
              Informasi Akhir Kontrak
            </h3>
            <div class="card-tools">
              <a href="<?=site_url("rekap_kontrak")?>">
                <button type="button" class="btn btn-sm btn-warning"><i class="fas fa-list-alt"></i> Detail</i></button>
              </a>
            </div>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-lg-6 col-6">
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3><?=$k1?></h3>
                  <p>Kontrak 1 Akan Berakhir</p>
                </div>
                <div class="icon">
                  <i class="fas fa-file-signature"></i>
                </div>
            </div>
            </div>
            <div class="col-lg-6 col-6">
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3><?=$k2?></h3>
                  <p>Kontrak 2 Akan Berakhir</p>
                </div>
                <div class="icon">
                  <i class="fas fa-file-signature"></i>
                </div>
              </div>
            </div>
            <div style="margin-top:0px;border-top:1px solid">*) Hari ini hingga 30 hari lagi</div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>