<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    var get_search_value = '';
    function exportList(th){
        var table = $('#table_data').DataTable();
        var info = table.page.info();   
        var data_per_page = info.length;
        var start_record = info.page * data_per_page;
        var get_order = table.order();
        var get_order_column = get_order[0][0];
        var get_order_dir = get_order[0][1];

        if($('.pekerjaan_filter').val() != ""){
            if(data_per_page > 100){
                formmsg = {};
                formmsg.stat = false;
                formmsg.msg = "Total Data Tidak Bisa Melebihi 100 Data";
                setFormMessage('.form-message', formmsg);
            }else{
                $('.form-message').html('');
                let blc = $(th).data('block');
                blockUI(blc);
                $.AjaxDownloader({
                    url  : $(th).data('url'),
                    data : {
                        <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                        date_filter_kontrak : $('.date_filter_kontrak').val(),
                        pekerjaan_filter : $('.pekerjaan_filter').val(),
                        batch_filter : $('.batch_filter').val(),
                        departement_filter : $('.departement_filter').val(),
                        mp_filter : $('.mp_filter').val(),
                        kontrak_filter : $('.kontrak_filter').val(),
                        start : start_record,
                        length : data_per_page,
                        order_column : get_order_column,
                        order_dir : get_order_dir,
                        search_value : get_search_value

                    }
                });
                setTimeout(function(){unblockUI(blc)}, 10000);
            }
        }else{
            formmsg = {};
            formmsg.stat = false;
            formmsg.msg = "Jenis Pekerjaan Harus Dipilih";
            setFormMessage('.form-message', formmsg);
            //alert('jenis pekerjaan')
        }
    }

    function exportData(th){
                
        var table = $('#table_data').DataTable();
        var info = table.page.info();
        var data_per_page = info.length;
        var start_record = info.page * data_per_page;
        var get_order = table.order();
        var get_order_column = get_order[0][0];
        var get_order_dir = get_order[0][1];


        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                    date_filter_kontrak : $('.date_filter_kontrak').val(),
                    pekerjaan_filter : $('.pekerjaan_filter').val(),
                    batch_filter : $('.batch_filter').val(),
                    departement_filter : $('.departement_filter').val(),
                    mp_filter : $('.mp_filter').val(),
                    kontrak_filter : $('.kontrak_filter').val(),
                    start : start_record,
                    length : data_per_page,
                    total : info.recordsDisplay,
                    order_column : get_order_column,
                    order_dir : get_order_dir,
                    search_value : get_search_value

            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }
    
    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 100,
            "lengthMenu": [[100, 500, -1], [100, 500, "All"]],
            "scrollY": 320,
            "scrollX": true,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter_kontrak = $('.date_filter_kontrak').val();
					d.pekerjaan_filter = $('.pekerjaan_filter').val();
					d.batch_filter = $('.batch_filter').val();
					d.departement_filter = $('.departement_filter').val();
					d.mp_filter = $('.mp_filter').val();
					d.kontrak_filter = $('.kontrak_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "class": "text-right",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "class": "text-right",
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "class": "text-right",
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "95",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [
                {
                    "aTargets": [13],
                    "mRender": function(data, type, row, meta) {
                        return '<button type="button" class="btn btn-xs btn-danger" onclick="exportData(this)" data-block="body" data-url="<?=site_url("export/{$routeURL}/") ?>' + data + '" ><i class="fas fa fa-clipboard-check"></i> Form Evaluasi</button>';
                    }
                }
            ]
        });

        $('.pekerjaan_filter').change(function(){
            dataTable.draw();
        });

        $('.batch_filter').change(function(){
            dataTable.draw();
        });

        $('.departement_filter').change(function(){
            dataTable.draw();
        });

        $('.mp_filter').change(function(){
            dataTable.draw();
        });

        $('.kontrak_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
                get_search_value = $(this).val();
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>