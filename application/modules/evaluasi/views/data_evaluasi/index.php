<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
            <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="form-message text-center"></div>
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom" style="font-size:11px !important">
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="kontrak_filter" class="col-md-4 col-form-label">Status Kontrak</label>
									<div class="col-md-8">
                                        <select id="kontrak_filter" name="kontrak_filter" class="form-control form-control-sm kontrak_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA KONTRAK -</option>
                                            <option value="KONTRAK 1">KONTRAK 1</option>
                                            <option value="KONTRAK 2">KONTRAK 2</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="pekerjaan_filter" class="col-md-4 col-form-label">Pekerjaan</label>
									<div class="col-md-8">
                                        <select id="pekerjaan_filter" name="pekerjaan_filter" class="form-control form-control-sm pekerjaan_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA PEKERJAAN -</option>
                                            <option value="ADMINISTRASI">ADMINISTRASI</option>
                                            <option value="ESO">ESO</option>
                                            <option value="OPERATOR">OPERATOR</option>
                                            <option value="TEKNISI">TEKNISI</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="batch_filter" class="col-md-4 col-form-label">Batch</label>
									<div class="col-md-8">
                                        <select id="batch_filter" name="batch_filter" class="form-control form-control-sm batch_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA BATCH -</option>
                                            <?php 
                                                foreach($batch_list as $bl){
                                                    echo '<option value="'.$bl->var_batch.'">'.$bl->var_batch.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-4">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="date_filter_kontrak" class="col-md-4 col-form-label">Akhir Kontrak</label>
									<div class="col-md-8">
										<input type="text" name="date_filter_kontrak" class="form-control form-control-sm date_filter_kontrak date_filter_next">
									</div>
								</div>
							</div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="mp_filter" class="col-md-4 col-form-label">Status MP</label>
									<div class="col-md-8">
                                        <select id="mp_filter" name="mp_filter" class="form-control form-control-sm mp_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA MP -</option>
                                            <option value="ACTIVE">ACTIVE</option>
                                            <option value="FAILED">FAILED</option>
                                            <option value="RESIGN">RESIGN</option>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="departement_filter" class="col-md-4 col-form-label">Dept.</label>
									<div class="col-md-8">
                                        <select id="departement_filter" name="departement_filter" class="form-control form-control-sm departement_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA DEPARTEMENT -</option>
                                            <?php 
                                                foreach($departement_list as $dl){
                                                    echo '<option value="'.$dl->var_departement.'">'.$dl->var_departement.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
						</div>
                    </div>
                    <div id="action" class="form-horizontal filter-date p-2 border-bottom text-right" style="font-size:11px !important">
                        <button type="button" class="btn btn-sm btn-success" onclick="exportList(this)" data-block="body" data-url="<?php echo $url_export ?>_data" ><i class="fas fa-download"></i> Export Data <i class="fas fa-file-excel"></i></button>
                        <button type="button" class="btn btn-sm btn-danger" onclick="exportList(this)" data-block="body" data-url="<?php echo $url_export ?>_form" ><i class="fas fa-download"></i> Form Evaluasi <i class="fas fa-file-excel"></i></button>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data" style="font-size:12px">
                        <thead>
                            <tr>
                                <th style="vertical-align:middle">No</th>
                                <th style="vertical-align:middle">Batch</th>
                                <th style="vertical-align:middle">Nama</th>
                                <th style="vertical-align:middle">NIK</th>
                                <th style="vertical-align:middle">Dept.</th>
                                <th style="vertical-align:middle">Section</th>
                                <th style="vertical-align:middle">Carline</th>
                                <th style="vertical-align:middle">Job</th>
                                <th style="vertical-align:middle">Pekerjaan</th>
                                <th style="vertical-align:middle">PIC</th>
                                <th style="vertical-align:middle">Status MP</th>
                                <th style="vertical-align:middle">Status Kerja</th>
                                <th style="vertical-align:middle">Akhir Kontrak</th>
                                <th style="vertical-align:middle">Evaluasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
