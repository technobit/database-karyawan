<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="action-form" width="100%">
<div id="modal-action" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1" style="margin-bottom:20px !important">
				<label for="var_nik" class="col-sm-4 col-form-label">Nomer Induk Karyawan</label>
				<div class="col-sm-6">
					<input type="text" class="form-control form-control-sm" id="var_nik" placeholder="Masukkan Nomer Induk Kependudukan (NIK/KTP)" name="var_nik" value="<?=isset($data->var_nik)? $data->var_nik : ''?>" <?=isset($data->var_nik)? 'disabled' : ''?>/>
				</div>
				<div class="col-sm-2">
					<?php if(!isset($data->var_nik)){?>
						<button type="button" class="btn btn-sm btn-primary" id="btn_check_nik" onclick="check_nik()">Cek !</button>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6" id="form_dokter_left" style="<?=isset($data->var_nik)? '' : 'display:none'?>">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Identitas</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<dd class="col-sm-4">Batch</dd>
								<dd class="col-sm-1 text-right">:</dd>
								<dt class="col-sm-7" id="var_batch"><?=isset($data->var_batch)? $data->var_batch : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-4">Nama</dd>
								<dd class="col-sm-1 text-right">:</dd>
								<dt class="col-sm-7" id="var_nama"><?=isset($data->var_nama)? $data->var_nama : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-4">Dept./Section</dd>
								<dd class="col-sm-1 text-right">:</dd>
								<dt class="col-sm-7">
									<?=isset($data->var_departement)? $data->var_departement : ''?>
								</dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-4">Carline</dd>
								<dd class="col-sm-1 text-right">:</dd>
								<dt class="col-sm-7" id="var_line"><?=isset($data->var_line)? $data->var_line : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-4">Job</dd>
								<dd class="col-sm-1 text-right">:</dd>
								<dt class="col-sm-7" id="var_job"><?=isset($data->var_job)? $data->var_job : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-4">Pekerjaan</dd>
								<dd class="col-sm-1 text-right">:</dd>
								<dt class="col-sm-7" id="var_pekerjaan"><?=isset($data->var_pekerjaan)? $data->var_pekerjaan : ''?></dt>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Absensi</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<label for="int_absen_k2_i" class="col-sm-6 col-form-label">Ijin</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm currency" id="int_absen_k2_i" placeholder="Ijin" name="int_absen_k2_i" value="<?=isset($data->int_absen_k2_i)? $data->int_absen_k2_i : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="int_absen_k2_s" class="col-sm-6 col-form-label">Sakit (Tanpa Surat Dokter)</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm currency" id="int_absen_k2_s" placeholder="Sakit" name="int_absen_k2_s" value="<?=isset($data->int_absen_k2_s)? $data->int_absen_k2_s : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="int_absen_k2_sd" class="col-sm-6 col-form-label">Sakit (Surat Dokter)</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm currency" id="int_absen_k2_sd" placeholder="Sakit (SD)" name="int_absen_k2_sd" value="<?=isset($data->int_absen_k2_sd)? $data->int_absen_k2_sd : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="int_absen_k2_so" class="col-sm-6 col-form-label">Sakit (Surat Opname)</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm currency" id="int_absen_k2_so" placeholder="Sakit (SO)" name="int_absen_k2_so" value="<?=isset($data->int_absen_k2_so)? $data->int_absen_k2_so : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="int_absen_k2_tk" class="col-sm-6 col-form-label">Tanpa Keterangan</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm currency" id="int_absen_k2_tk" placeholder="Tanpa Keterangan" name="int_absen_k2_tk" value="<?=isset($data->int_absen_k2_tk)? $data->int_absen_k2_tk : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="int_grand_total" class="col-sm-6 col-form-label">Total Absen</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm currency" id="int_grand_total" placeholder="Total Absen" name="int_grand_total" value="<?=isset($data->int_grand_total)? $data->int_grand_total : ''?>" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6" id="form_dokter_right" style="<?=isset($data->var_nik)? '' : 'display:none'?>">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Kualifikasi/Spesialisasi</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<div class="col-sm-12">
									<select id="int_spesialis_id" name="int_spesialis_id" class="form-control form-control-sm select-2" style="width: 100%;">
										<option value="">- Pilih Kualifikasi/Spesialisasi -</option>
										<?php 
											foreach($list_spesialis as $ls){
												echo '<option value="'.$ls->int_spesialis_id.'">'.$ls->var_gelar.' ('.$ls->var_spesialis.')</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Surat Tanda Registrasi (STR)</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<label for="var_no_str" class="col-sm-4 col-form-label">Nomor STR</label>
								<div class="col-sm-8">
									<input type="text" class="form-control form-control-sm" id="var_no_str" placeholder="Nomor Surat Tanda Registrasi" name="var_no_str" value="<?=isset($data->var_no_str)? $data->var_no_str : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="var_no_berkas" class="col-sm-4 col-form-label">Nomor Berkas</label>
								<div class="col-sm-8">
									<input type="text" class="form-control form-control-sm" id="var_no_berkas" placeholder="Nomor Berkas STR" name="var_no_berkas" value="<?=isset($data->var_no_berkas)? $data->var_no_berkas : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="dt_penetapan_str" class="col-sm-4 col-form-label">Tanggal Penetapan</label>
								<div class="col-sm-8">
									<input type="text" class="form-control form-control-sm date_picker" id="dt_penetapan_str" placeholder="Tanggal Penetapan STR" name="dt_penetapan_str" value="<?=isset($data->dt_penetapan_str)? $data->dt_penetapan_str : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<label for="dt_masa_berlaku_str" class="col-sm-4 col-form-label">Masa Berlaku</label>
								<div class="col-sm-8">
									<input type="text" class="form-control form-control-sm date_picker" id="dt_masa_berlaku_str" placeholder="Masa Berlaku STR" name="dt_masa_berlaku_str" value="<?=isset($data->dt_masa_berlaku_str)? $data->dt_masa_berlaku_str : ''?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h3 class="card-title">Status Pada Fasilitas Kesehatan</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<div class="col-sm-4">
									<div class="icheck-primary d-inline is_faskes_aktif">
										<input type="radio" id="radio_btn1" name="is_faskes_aktif" value="1" <?=isset($data->is_faskes_aktif)? (($data->is_faskes_aktif == 1)? 'checked' : '') : ''?>>
											<label for="radio_btn1">Aktif </label>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="icheck-danger d-inline is_faskes_aktif">
										<input type="radio" id="radio_btn2" name="is_faskes_aktif" value="0" <?=isset($data->is_faskes_aktif)? (($data->is_faskes_aktif == 0)? 'checked' : '') : '' ?>>
										<label for="radio_btn2">Tidak Aktif</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--<input type="hidden" id="int_exsisting " name="int_exsisting" value="0"/>-->
			<input type="hidden" id="faskes_dokter_id " name="faskes_dokter_id" value=""/>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success" id="save_button" style="<?=isset($data->var_nik)? '' : 'display:none'?>">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.select2').select2();
		$('.date_picker').daterangepicker(datepickModal);
		bsCustomFileInput.init();
		$('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
		$("#action-form").validate({
			rules: {
				var_nik:{
					required: true,
					digits: true,
					minlength: 1,
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#user-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>