<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class data_evaluasi extends MX_Controller {
	private $input_file_name = 'data_evaluasi';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'EVALUASI'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'evaluasi';
		$this->routeURL = 'evaluasi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('data_evaluasi_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Evaluasi Karyawan';
		$this->page->menu 	  = 'evaluasi';
		//$this->page->submenu1 = 'data_evaluasi';
		$this->breadcrumb->title = 'Data Evaluasi Karyawan';
		$this->breadcrumb->card_title = 'Data Evaluasi Karyawan';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Karyawan', 'Data Evaluasi'];
		$this->js = true;
		$data['pekerjaan_list']		= $this->model->pekerjaan_list();
		$data['batch_list']			= $this->model->batch_list();
		$data['departement_list']	= $this->model->departement_list();
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}");
		$this->render_view('data_evaluasi/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		if(strpos($this->input->post('date_filter_kontrak', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter_kontrak', true));
        }

		$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
		$filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';
		
		$pekerjaan_filter	= $this->input->post('pekerjaan_filter', true);
		$batch_filter		= $this->input->post('batch_filter', true);
		$departement_filter	= $this->input->post('departement_filter', true);
		$mp_filter			= $this->input->post('mp_filter', true);
		$kontrak_filter		= $this->input->post('kontrak_filter', true);

		$data  = array();
		$total = $this->model->listCount($pekerjaan_filter, $batch_filter, $departement_filter, $mp_filter, $kontrak_filter, $filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($pekerjaan_filter, $batch_filter, $departement_filter, $mp_filter, $kontrak_filter, $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			if($d->var_status_kerja == 'KONTRAK 1' || $d->var_status_kerja == 'KONTRAK 1 (IN CLASS)'){
				$akhir_kontrak = idn_date($d->dt_tanggal_akhir_k1, "j F Y");
			}else if($d->var_status_kerja == 'KONTRAK 2'){
				$akhir_kontrak = idn_date($d->dt_tanggal_akhir_k2, "j F Y");
			}else{
				$akhir_kontrak = '-';
			}
			$i++;
			$data[] = array($i, $d->var_batch, $d->var_nama, $d->var_nik, $d->var_departement, $d->var_section, $d->var_line, $d->var_job, $d->var_pekerjaan, $d->var_pic_spv, $d->var_status_mp, $d->var_status_kerja, $akhir_kontrak, $d->int_karyawan_kontrak_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}
	
	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Tambah Data Karyawan';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('kontrak/index_action', $data, true);
		
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('var_nik', 'NIK', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $query = $this->user->create($this->input->post());
			$this->set_json([  'stat' => true, 
								'mc' => $query, //modal close
								'msg' => "Data berhasil dibuat",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_input_evaluasi_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_input_evaluasi_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_input_evaluasi_id");
			$data['title']	= 'Edit Data Karyawan';
			$this->load_view('data_evaluasi/index_action', $data);
		}
	}

	public function save_import(){
		$this->authCheckDetailAccess('c');
		
		$start = start_time();
		$this->form_validation->set_rules('mulai', 'Mulai', 'required|integer');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			if(isset($_FILES[$this->input_file_name])){
				$config['upload_path']   = "./{$this->import_dir}"; 
				$config['allowed_types'] = 'xlsx|xls'; 
				$config['encrypt_name']  = true; 
				$config['max_size']      = 4096;  
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload($this->input_file_name)){
					$status  = $this->model->import($this->input->post(), $this->upload->data());
					$this->set_json([  'stat' => ($status !== false), 
								'mc' => false,//($status !== false), //modal close
								'time' => finish_time($start),
								'msg' => ($status !== false)? "Data berhasil di-import dengan {$status} baris data." : 'Data gagal di-import',
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
				} else {
					$this->set_json([  'stat' => false, 
										'mc' => false, //modal close
										'msg' => "Data gagal di-import. ".$this->upload->display_errors('',''),
										'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
									]);
				}
			} else {
				$this->set_json([  'stat' => false, 
									'mc' => false, //modal close
									'msg' => "Data gagal di-import",
									'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
								]);
			}

        }
	}

	public function export_data(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		if(strpos($this->input->post('date_filter_kontrak', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter_kontrak', true));
        }

		$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
		$filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';
		
		$pekerjaan_filter	= $this->input->post('pekerjaan_filter', true);
		$batch_filter		= $this->input->post('batch_filter', true);
		$departement_filter	= $this->input->post('departement_filter', true);
		$mp_filter			= $this->input->post('mp_filter', true);
		$kontrak_filter		= $this->input->post('kontrak_filter', true);	

		$ldata = $this->model->list($pekerjaan_filter, $batch_filter, $departement_filter, $mp_filter, $kontrak_filter, $filter_start, $filter_end, $this->input_post('search_value', TRUE), $this->input_post('order_column', true), $this->input_post('order_dir'), $this->input_post('length', true), $this->input_post('start', true));

        $title    = 'Daftar Evaluasi';

        $filename = 'Daftar Evaluasi '.idn_date($filter_start, 'j F Y').'- '.idn_date($filter_end, 'j F Y').'.xlsx';



		$input_file = 'assets/export/xlsx/evaluasi_data.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0);

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 1;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
            $i++;
			$x++;

            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_batch);
            $sheet->setCellValue('C'.$x, $d->var_nama);
            $sheet->setCellValue('D'.$x, $d->var_nik);
            $sheet->setCellValue('E'.$x, $d->var_departement);
            $sheet->setCellValue('F'.$x, $d->var_section);
            $sheet->setCellValue('G'.$x, $d->var_line);
			$sheet->setCellValue('H'.$x, $d->var_job);
			$sheet->setCellValue('I'.$x, $d->var_pekerjaan);
			$sheet->setCellValue('J'.$x, $d->var_status_mp);
			$sheet->setCellValue('K'.$x, $d->var_status_kerja);
			if($d->var_status_kerja == 'KONTRAK 1'){
				if($d->dt_tanggal_akhir_k1 != '1970-01-01'){
				//$sheet->setCellValue('K'.$x, $d->dt_tanggal_akhir_k1);
				$sheet->setCellValue('L'.$x, date_diff(date_create($d->dt_tanggal_akhir_k1),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('L'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
				}
			}else if($d->var_status_kerja == 'KONTRAK 2'){
				if($d->dt_tanggal_akhir_k2 != '1970-01-01'){
					//$sheet->setCellValue('H'.$x, $d->dt_tanggal_akhir_k2);
					$sheet->setCellValue('L'.$x, date_diff(date_create($d->dt_tanggal_akhir_k2),date_create("1900-01-01"))->format("%a") + 2);
					$sheet->getStyle('L'.$x)->getNumberFormat()
							->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY);
				}
			}
        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}

	public function export_form(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		if(strpos($this->input->post('date_filter_kontrak', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter_kontrak', true));
        }

		$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
		$filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';
		
		$pekerjaan_filter	= $this->input->post('pekerjaan_filter', true);
		$batch_filter		= $this->input->post('batch_filter', true);
		$departement_filter	= $this->input->post('departement_filter', true);
		$mp_filter			= $this->input->post('mp_filter', true);
		$kontrak_filter		= $this->input->post('kontrak_filter', true);	

		$ldata = $this->model->list($pekerjaan_filter, $batch_filter, $departement_filter, $mp_filter, $kontrak_filter, $filter_start, $filter_end, $this->input_post('search_value', TRUE), $this->input_post('order_column', true), $this->input_post('order_dir'), $this->input_post('length', true), $this->input_post('start', true));
        $title    = 'Daftar Evaluasi';

        $filename = 'Daftar Evaluasi '.strtolower($pekerjaan_filter).' '.idn_date($filter_start, 'j F Y').'- '.idn_date($filter_end, 'j F Y').'.xlsx';

		if($pekerjaan_filter == 'OPERATOR' || $pekerjaan_filter == 'ADMINISTRASI' || $pekerjaan_filter == 'ESO' || $pekerjaan_filter == 'TEKNISI'){
			$excel_filename = strtolower($pekerjaan_filter);
			$input_file = 'assets/export/xlsx/evaluasi_'.strtolower($pekerjaan_filter).'100.xlsx';
		}else{
			$input_file = 'assets/export/xlsx/evaluasi_operator100.xlsx';
		}

		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0);

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 0;
        foreach($ldata as $d){
            $i++;
			if($d->var_status_kerja == 'KONTRAK 1'){
				$judul_evaluasi = 'FORM EVALUASI KONTRAK I ( SATU )';
			}else if($d->var_status_kerja == 'KONTRAK 2'){
				$judul_evaluasi = 'FORM EVALUASI KONTRAK II ( DUA )';
			}else{
				$judul_evaluasi = 'FORM EVALUASI';
			}
	
			$sheet->setCellValue('A'.strval(($x * 52) + 4), 'Tahun '.date("Y"));
			$sheet->setCellValue('A'.strval(($x * 52) + 5), $judul_evaluasi);
			$sheet->setCellValue('A'.strval(($x * 52) + 8), 'Nama : '.$d->var_nama);
			$sheet->setCellValue('A'.strval(($x * 52) + 9), 'NIK     : '.$d->var_nik);
			$sheet->setCellValue('B'.strval(($x * 52) + 6), $d->var_pekerjaan);
			$sheet->setCellValue('I'.strval(($x * 52) + 8), ': '.$d->var_jabatan);
			$sheet->setCellValue('I'.strval(($x * 52) + 9), ': '.$d->var_section);
			$sheet->setCellValue('H'.strval(($x * 52) + 1), 'BATCH : '.$d->var_batch);
			$sheet->setCellValue('H'.strval(($x * 52) + 2), 'PIC : '.$d->var_pic_spv);
			if($pekerjaan_filter == 'OPERATOR'){
				$sheet->setCellValue('H'.strval(($x * 52) + 3), 'Job Desc/Line : '.$d->var_job);
				$sheet->setCellValue('H'.strval(($x * 52) + 4), '              : '.$d->var_line);
			}
			if($pekerjaan_filter != 'ESO'){
				$sheet->setCellValue('B'.strval(($x * 52) + 50), 'Jumlah SP : '.$d->var_jumlah_sp);
				$sheet->setCellValue('B'.strval(($x * 52) + 51), 'Jenis SP : '.$d->var_jenis_sp);
			}
			//$inv = $inv + 52;
			$x++;
        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}

	public function export($int_karyawan_kontrak_id){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		$res = $this->model->get($int_karyawan_kontrak_id);

        $title    = 'Evaluasi';

		$filename = 'Form Evaluasi '.$res->var_nik.' - '.strtolower($res->var_pekerjaan).'.xlsx';

		if($res->var_pekerjaan == 'OPERATOR' || $res->var_pekerjaan == 'ADMINISTRASI' || $res->var_pekerjaan == 'ESO' || $res->var_pekerjaan == 'TEKNISI'){
			$excel_filename = strtolower($res->var_pekerjaan);
			$input_file = 'assets/export/xlsx/evaluasi_'.strtolower($res->var_pekerjaan).'.xlsx';
		}else{
			$input_file = 'assets/export/xlsx/evaluasi_operator.xlsx';
		}
		//
		if($res->var_status_kerja == 'KONTRAK 1' || $res->var_status_kerja == 'KONTRAK 1 (IN CLASS)'){
			$judul_evaluasi = 'FORM EVALUASI KONTRAK I ( SATU )';
		}else if($res->var_status_kerja == 'KONTRAK 2'){
			$judul_evaluasi = 'FORM EVALUASI KONTRAK II ( DUA )';
		}else{
			$judul_evaluasi = 'FORM EVALUASI';
		}

		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0);

		$sheet = $spreadsheet->getActiveSheet();

		$sheet->setCellValue('A4', 'Tahun '.date("Y"));
		$sheet->setCellValue('A5', $judul_evaluasi);
		$sheet->setCellValue('A8', 'Nama : '.$res->var_nama);
		$sheet->setCellValue('A9', 'NIK     : '.$res->var_nik);
		$sheet->setCellValue('B6', $res->var_pekerjaan);
		$sheet->setCellValue('I8', ': '.$res->var_jabatan);
		$sheet->setCellValue('I9', ': '.$res->var_section);
		$sheet->setCellValue('H1', 'BATCH : '.$res->var_batch);
		$sheet->setCellValue('H2', 'PIC : '.$res->var_pic_spv);
		if($res->var_pekerjaan == 'OPERATOR'){
			$sheet->setCellValue('H3', 'Job Desc/Line : '.$res->var_job);
			$sheet->setCellValue('H4', '                           '.$res->var_line);
		}
		if($res->var_pekerjaan != 'ESO'){
			$sheet->setCellValue('B50', 'Jumlah SP : '.$res->var_jumlah_sp);
			$sheet->setCellValue('B51', 'Jenis SP : '.$res->var_jenis_sp);
		}

		/*$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A4', 'Tahun '.date("Y"))
					->setCellValue('A5', $judul_evaluasi)
					->setCellValue('A8', 'Nama : '.$res->var_nama)
					->setCellValue('A9', 'NIK : '.$res->var_nik)
					->setCellValue('B6', $res->var_pekerjaan)
					->setCellValue('I8', ': '.$res->var_jabatan)
					->setCellValue('I9', ': '.$res->var_section)
					->setCellValue('H1', 'BATCH : '.$res->var_batch)
					->setCellValue('H2', 'PIC : '.$res->var_pic_spv)
					->setCellValue('B50', 'Jumlah SP : '.$res->var_jumlah_sp)
					->setCellValue('B51', 'Jenis SP : '.$res->var_jenis_sp);*/

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}