<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['evaluasi']['get']                   = 'evaluasi/data_evaluasi';
$route['evaluasi']['post']                  = 'evaluasi/data_evaluasi/list';
$route['export/evaluasi_data']['post']      = 'evaluasi/data_evaluasi/export_data';
$route['export/evaluasi_form']['post']      = 'evaluasi/data_evaluasi/export_form';
$route['export/evaluasi/([a-zA-Z0-9]+)']['post']= 'evaluasi/data_evaluasi/export/$1';