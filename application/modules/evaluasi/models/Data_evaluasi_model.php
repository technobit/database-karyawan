<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Data_evaluasi_model extends MY_Model {

	public function pekerjaan_list(){
		$this->db->select("var_pekerjaan")
			->from($this->m_karyawan_kontrak)
			->group_by("var_pekerjaan");
		return $this->db->order_by('var_pekerjaan', 'ASC')->get()->result();
	}

	public function batch_list(){
		$this->db->select("var_batch")
			->from($this->m_karyawan_kontrak)
			->group_by("var_batch");
		return $this->db->order_by('var_batch', 'ASC')->get()->result();
	}

	public function departement_list(){
		$this->db->select("var_departement")
			->from($this->m_karyawan_kontrak)
			->group_by("var_departement");
		return $this->db->order_by('var_departement', 'ASC')->get()->result();
	}

	public function list($pekerjaan_filter = "", $batch_filter = "", $departement_filter = "", $mp_filter = "", $kontrak_filter = "", $filter_start = NULL, $filter_end = NULL, $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$expired_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'));

		$this->db->select("*")
				->from($this->m_karyawan_kontrak." mkk")
				->where("mkk.int_status", 1);

		if($kontrak_filter != ""){ // filter
			$this->db->where('mkk.var_status_kerja', $kontrak_filter);
		}else{
			$this->db->where("mkk.var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'TETAP', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3', 'TETAP (EXPATRIAT)')");
		}

		if($pekerjaan_filter != ""){ // filter
			$this->db->where('mkk.var_pekerjaan', $pekerjaan_filter);
		}

		if($batch_filter != ""){ // filter
			$this->db->where('mkk.var_batch', $batch_filter);
		}

		if($departement_filter != ""){ // filter
			$this->db->where('mkk.var_departement', $departement_filter);
		}

		if($mp_filter != ""){ // filter
			$this->db->where('mkk.var_status_mp', $mp_filter);
		}
		
		if(!empty($filter_start) && !empty($filter_end)){
			$this->db->where('((mkk.dt_tanggal_akhir_k1 BETWEEN "'.$filter_start.'" AND "'.$filter_end.'") OR (mkk.dt_tanggal_akhir_k2 BETWEEN "'.$filter_start.'" AND "'.$filter_end.'"))');
			//$this->whereBetweenDate('dt_periode_kontrak1_awal', $filter_start_kontrak, $filter_end_kontrak);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mkk.var_batch', $filter)
					->or_like('mkk.var_nama', $filter)
					->or_like('mkk.var_nik', $filter)
					->or_like('mkk.var_departement', $filter)
					->or_like('mkk.var_line', $filter)
					->or_like('mkk.var_job', $filter)
					->or_like('mkk.var_pekerjaan', $filter)
					->or_like('mkk.var_pic_spv', $filter)
					->group_end();
		}

		$order = 'int_karyawan_kontrak_id';
		switch($order_by){
			case 1 : $order = 'dt_tanggal_akhir_k1 '; break;
			case 2 : $order = 'dt_tanggal_akhir_k2 '; break;
			case 3 : $order = 'var_batch '; break;
			case 4 : $order = 'var_nama '; break;
			case 5 : $order = 'var_nik '; break;
			case 6 : $order = 'var_departement '; break;
			case 7 : $order = 'var_line '; break;
			case 8 : $order = 'var_job '; break;
			case 9 : $order = 'var_pekerjaan '; break;
			case 10 : $order = 'var_pic_spv '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($pekerjaan_filter = "", $batch_filter = "", $departement_filter = "", $mp_filter = "", $kontrak_filter = "", $filter_start = NULL, $filter_end = NULL, $filter = NULL){
		$expired_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'));

		$this->db->select("*")
				->from($this->m_karyawan_kontrak." mkk")
				->where("mkk.int_status", 1);

		if($kontrak_filter != ""){ // filter
			$this->db->where('mkk.var_status_kerja', $kontrak_filter);
		}else{
			$this->db->where("mkk.var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'TETAP', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3', 'TETAP (EXPATRIAT)')");
		}

		if($pekerjaan_filter != ""){ // filter
			$this->db->where('mkk.var_pekerjaan', $pekerjaan_filter);
		}

		if($batch_filter != ""){ // filter
			$this->db->where('mkk.var_batch', $batch_filter);
		}

		if($departement_filter != ""){ // filter
			$this->db->where('mkk.var_departement', $departement_filter);
		}

		if($mp_filter != ""){ // filter
			$this->db->where('mkk.var_status_mp', $mp_filter);
		}
		
		if(!empty($filter_start) && !empty($filter_end)){
			$this->db->where('((mkk.dt_tanggal_akhir_k1 BETWEEN "'.$filter_start.'" AND "'.$filter_end.'") OR (mkk.dt_tanggal_akhir_k2 BETWEEN "'.$filter_start.'" AND "'.$filter_end.'"))');
			//$this->whereBetweenDate('dt_periode_kontrak1_awal', $filter_start_kontrak, $filter_end_kontrak);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mkk.var_batch', $filter)
					->or_like('mkk.var_nama', $filter)
					->group_end();
		}
		
		return $this->db->count_all_results();
	}

	public function get($int_karyawan_kontrak_id){
		return $this->db->select("*")
					->get_where($this->m_karyawan_kontrak, ['int_karyawan_kontrak_id' => $int_karyawan_kontrak_id])->row();
	}

	public function update($int_input_evaluasi_id, $upd){
		$this->db->trans_begin();

		$this->db->where('int_input_evaluasi_id', $int_input_evaluasi_id);
		$this->db->update($this->t_input_evaluasi_karyawan, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function import($in, $file){
		$user 	= $this->session->userdata('username');
		$var_batch = 'B';
		$var_nama = 'C';
		$var_nik = 'D';
		$var_departement  = 'E';
		$var_carline  = 'F';
		$var_job  = 'G';
		$var_pekerjaan  = 'H';
		$int_absen_k2_i  = 'I';
		$int_absen_k2_s  = 'J';
		$int_absen_k2_sd  = 'K';
		$int_absen_k2_so  = 'L';
		$int_absen_k2_tk  = 'M';
		$int_grand_total  = 'N';
		$int_point_1 = 'O';
		$int_point_2 = 'P';
		$int_point_3 = 'Q';
		$int_point_4 = 'R';
		$int_point_5 = 'S';
		$int_point_6 = 'T';
		$int_point_7 = 'U';
		$int_point_8 = 'V';
		$int_point_9 = 'W';
		$int_point_10 = 'X';
		$int_point_11 = 'Y';
		$int_point_12 = 'Z';
		$int_point_13 = 'AA';
		$dec_nilai = 'AB';
		$var_kriteria = 'AC';
		$var_keterangan = 'AD';
		$int_lulus_k2 = 'AE';
		$int_failed = 'AF';
		$var_pic = 'AG';

		$filterSubset = new MyReadFilter($in['mulai'],
						[$var_batch,
						$var_nama,
						$var_nik,
						$var_departement,
						$var_carline,
						$var_job,
						$var_pekerjaan,
						$int_absen_k2_i,
						$int_absen_k2_s,
						$int_absen_k2_sd,
						$int_absen_k2_so,
						$int_absen_k2_tk,
						$int_grand_total,
						$int_point_1,
						$int_point_2,
						$int_point_3,
						$int_point_4,
						$int_point_5,
						$int_point_6,
						$int_point_7,
						$int_point_8,
						$int_point_9,
						$int_point_10,
						$int_point_11,
						$int_point_12,
						$int_point_13,
						$dec_nilai,
						$var_kriteria,
						$var_keterangan,
						$int_lulus_k2,
						$int_failed,
						$var_pic]
					);
		$reader = IOFactory::createReader(ucfirst(ltrim($file['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, false, false, true);
		
		//INSERT IGNORE INTO-> 0 row affected if duplicate
		//REPLACE INTO -> replace old duplicate entry
		$ins_evaluasi = "REPLACE INTO {$this->t_input_evaluasi_karyawan} 
						(`dt_periode_absen_awal`, `dt_periode_absen_akhir`, `dt_periode_kontrak1_awal`, `dt_periode_kontrak1_akhir`, `var_batch`, `var_nama`, `var_nik`, `var_departement`, `var_carline`, `var_job`, `var_pekerjaan`, `int_absen_k2_i`, `int_absen_k2_s`, `int_absen_k2_sd`, `int_absen_k2_so`, `int_absen_k2_tk`, `int_grand_total`, `int_point_1`, `int_point_2`, `int_point_3`, `int_point_4`, `int_point_5`, `int_point_6`, `int_point_7`, `int_point_8`, `int_point_9`, `int_point_10`, `int_point_11`, `int_point_12`, `int_point_13`, `dec_nilai`, `var_kriteria`, `var_keterangan`, `int_lulus_k2`, `int_failed`, `var_pic`) VALUES ";
		$total = 0;
		foreach($data as $i => $d){
			if($i > ($in['mulai'] - 1)){
				$total++;

				$d_var_batch = $this->db->escape(trim($d[$var_batch]));
				$d_var_nama = $this->db->escape(trim($d[$var_nama]));
				$d_var_nik = $this->db->escape(trim($d[$var_nik]));
				$d_var_departement = $this->db->escape(trim($d[$var_departement]));
				$d_var_carline = $this->db->escape(trim($d[$var_carline]));
				$d_var_job = $this->db->escape(trim($d[$var_job]));
				$d_var_pekerjaan = $this->db->escape(trim($d[$var_pekerjaan]));
				$d_int_absen_k2_i = $this->db->escape(trim($d[$int_absen_k2_i]));
				$d_int_absen_k2_s = $this->db->escape(trim($d[$int_absen_k2_s]));
				$d_int_absen_k2_sd = $this->db->escape(trim($d[$int_absen_k2_sd]));
				$d_int_absen_k2_so = $this->db->escape(trim($d[$int_absen_k2_so]));
				$d_int_absen_k2_tk = $this->db->escape(trim($d[$int_absen_k2_tk]));
				$d_int_grand_total = $this->db->escape(trim($d[$int_grand_total]));
				$d_int_point_1 = $this->db->escape(trim($d[$int_point_1]));
				$d_int_point_2 = $this->db->escape(trim($d[$int_point_2]));
				$d_int_point_3 = $this->db->escape(trim($d[$int_point_3]));
				$d_int_point_4 = $this->db->escape(trim($d[$int_point_4]));
				$d_int_point_5 = $this->db->escape(trim($d[$int_point_5]));
				$d_int_point_6 = $this->db->escape(trim($d[$int_point_6]));
				$d_int_point_7 = $this->db->escape(trim($d[$int_point_7]));
				$d_int_point_8 = $this->db->escape(trim($d[$int_point_8]));
				$d_int_point_9 = $this->db->escape(trim($d[$int_point_9]));
				$d_int_point_10 = $this->db->escape(trim($d[$int_point_10]));
				$d_int_point_11 = $this->db->escape(trim($d[$int_point_11]));
				$d_int_point_12 = $this->db->escape(trim($d[$int_point_12]));
				$d_int_point_13 = $this->db->escape(trim($d[$int_point_13]));
				$d_dec_nilai = $this->db->escape(number_format($d[$dec_nilai],2,".",""));
				$d_var_kriteria = $this->db->escape(trim($d[$var_kriteria]));
				$d_var_keterangan = $this->db->escape(trim($d[$var_keterangan]));
				$d_int_lulus_k2 = $this->db->escape(isset($d[$int_lulus_k2])? 1 : 0);
				$d_int_failed = $this->db->escape(isset($d[$int_failed])? 1 : 0);
				$d_var_pic = $this->db->escape(trim($d[$var_pic]));

				$ins_evaluasi .= "('{$in['dt_periode_absen_awal']}', '{$in['dt_periode_absen_akhir']}', '{$in['dt_periode_kontrak1_awal']}', '{$in['dt_periode_kontrak1_akhir']}', {$d_var_batch}, {$d_var_nama}, {$d_var_nik}, {$d_var_departement}, {$d_var_carline}, {$d_var_job}, {$d_var_pekerjaan}, {$d_int_absen_k2_i}, {$d_int_absen_k2_s}, {$d_int_absen_k2_sd}, {$d_int_absen_k2_so}, {$d_int_absen_k2_tk}, {$d_int_grand_total}, {$d_int_point_1}, {$d_int_point_2}, {$d_int_point_3}, {$d_int_point_4}, {$d_int_point_5}, {$d_int_point_6}, {$d_int_point_7}, {$d_int_point_8}, {$d_int_point_9}, {$d_int_point_10}, {$d_int_point_11}, {$d_int_point_12}, {$d_int_point_13}, {$d_dec_nilai}, {$d_var_kriteria}, {$d_var_keterangan}, {$d_int_lulus_k2}, {$d_int_failed}, {$d_var_pic}),";
			}
		}
		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['var_username' => $user,
										  'var_file_name' => $file['orig_name'],
										  'txt_direktori' => $file['full_path'],
										  'dt_import_date' => date("Y-m-d H:i:s"),
										  'int_total' => $total]);
		
		$ins_evaluasi = rtrim($ins_evaluasi, ',').';';
		$this->db->query($ins_evaluasi);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $total;
		}
	}

	function format_tanggal_excel($str_date){
		$int_date = $str_date - 2;
		$add_date = date('Y-m-d', strtotime('1900-01-01'. ' + '.$int_date.' days'));
		$strtotime = strtotime($add_date);
		return date('Y-m-d',$strtotime);
	}

	function format_jam_excel($str_time){
		$add_sec = 86400 * floatval($str_time);
		$datetime = date("1900-01-01 00:00:00");
		// Convert datetime to Unix timestamp
		$timestamp = strtotime($datetime);
		// Subtract time from datetime
		$time = $timestamp + $add_sec;
		// Date and time after subtraction
		return date("H:i:s", $time);
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
