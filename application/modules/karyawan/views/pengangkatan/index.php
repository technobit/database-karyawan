<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" data-block="body" class="btn btn-sm btn-success ajax_modal" data-url="<?=$url?>/import" ><i class="fas fa-file-excel"></i> Import Excel</button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <!--<div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="masa_filter" class="col-md-6 col-form-label">Status Kontrak</label>
									<div class="col-md-6">
                                        <select id="int_status_id" name="int_status_id" class="form-control form-control-sm masa_filter select2" style="width: 100%;">
                                            <option value="">- Semua Status-</option>
                                            <option value="1">Kontrak < 1 Bulan</option>
                                            <option value="2">Kontrak > 1 Bulan</option>
                                        </select>
									</div>
								</div>
                            </div>
						</div>
                    </div>-->
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIM</th>
                            <th>NIK KTP</th>
                            <th>Nama</th>
                            <th>Departemen</th>
                            <th>Section</th>
                            <th>Job</th>
                            <th>Pekerjaan</th>
                            <th>Line</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
