<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>

    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 100,
            "lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					//d.masa_filter = $('.masa_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "70",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [
                {
                    "aTargets": [9],
                    "mRender": function(data, type, row, meta) {
                        return  '<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '" class="ajax_modal btn btn-xs btn-primary tooltips" data-placement="top" data-original-title="Kontrak" ><i class="fa fa-file-signature"></i> Kontrak</a>';
                    }
                }
            ]
        });

        /*$('.masa_filter').change(function(){
            dataTable.draw();
        });*/

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>