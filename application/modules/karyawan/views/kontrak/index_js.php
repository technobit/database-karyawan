<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    var get_search_value = '';
    function exportData(th){

        var table = $('#table_data').DataTable();
        var info = table.page.info();   
        var data_per_page = info.length;
        var start_record = info.page * data_per_page;
        var get_order = table.order();
        var get_order_column = get_order[0][0];
        var get_order_dir = get_order[0][1];

        if(document.getElementById("tanggal_masuk_filter").disabled){var d_tanggal_masuk_filter = '';
        }else{d_tanggal_masuk_filter = $('.tanggal_masuk_filter').val();}

        if(document.getElementById("tanggal_awal_k1_filter").disabled){var d_tanggal_awal_k1_filter = '';
        }else{d_tanggal_awal_k1_filter = $('.tanggal_awal_k1_filter').val();}

        if(document.getElementById("tanggal_akhir_k1_filter").disabled){var d_tanggal_akhir_k1_filter = '';
        }else{d_tanggal_akhir_k1_filter = $('.tanggal_akhir_k1_filter').val();}

        if(document.getElementById("tanggal_awal_k2_filter").disabled){var d_tanggal_awal_k2_filter = '';
        }else{d_tanggal_awal_k2_filter = $('.tanggal_awal_k2_filter').val();}

        if(document.getElementById("tanggal_akhir_k2_filter").disabled){var d_tanggal_akhir_k2_filter = '';
        }else{d_tanggal_akhir_k2_filter = $('.tanggal_akhir_k2_filter').val();}

        if(document.getElementById("tanggal_tetap_filter").disabled){var d_tanggal_tetap_filter = '';
        }else{d_tanggal_tetap_filter = $('.tanggal_tetap_filter').val();}

        if(document.getElementById("tanggal_out_filter").disabled){var d_tanggal_out_filter = '';
        }else{d_tanggal_out_filter = $('.tanggal_out_filter').val();}

        if(document.getElementById("tanggal_sp_filter").disabled){var d_tanggal_sp_filter = '';
        }else{d_tanggal_sp_filter = $('.tanggal_sp_filter').val();}

        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                batch_filter : $('.batch_filter').val(),
                jabatan_filter : $('.jabatan_filter').val(),
                pekerjaan_filter : $('.pekerjaan_filter').val(),
                tanggal_masuk_filter : d_tanggal_masuk_filter,
                tanggal_awal_k1_filter : d_tanggal_awal_k1_filter,
                tanggal_akhir_k1_filter : d_tanggal_akhir_k1_filter,
                tanggal_awal_k2_filter : d_tanggal_awal_k2_filter,
                tanggal_akhir_k2_filter : d_tanggal_akhir_k2_filter,
                tanggal_tetap_filter : d_tanggal_tetap_filter,
                status_kerja_filter : $('.status_kerja_filter').val(),
                status_mp_filter : $('.status_mp_filter').val(),
                tanggal_out_filter : d_tanggal_out_filter,
                status_foh_filter : $('.status_foh_filter').val(),
                departement_filter : $('.departement_filter').val(),
                section_filter : $('.section_filter').val(),
                shift_filter : $('.shift_filter').val(),
                area_filter : $('.area_filter').val(),
                jenis_kelamin_filter : $('.jenis_kelamin_filter').val(),
                line_filter : $('.line_filter').val(),
                pic_filter : $('.pic_filter').val(),
                jumlah_sp_filter : $('.jumlah_sp_filter').val(),
                jenis_sp_filter : $('.jenis_sp_filter').val(),
                tanggal_sp_filter : d_tanggal_sp_filter,
                status_dl_idl_filter : $('.status_dl_idl_filter').val(),
                aktif_filter : $('.aktif_filter').val(),
                start : start_record,
                length : data_per_page,
                order_column : get_order_column,
                order_dir : get_order_dir,
                search_value : get_search_value
            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }

    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        $('.date_picker').daterangepicker(datepickModal);
        dataTable = $('#table_data').DataTable({
            //"scrollY": 200,
            //"scrollX": true,
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, "All"]],
            "scrollY": 450,
            "scrollX": true,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.batch_filter = $('.batch_filter').val();
					d.jabatan_filter = $('.jabatan_filter').val();
					d.pekerjaan_filter = $('.pekerjaan_filter').val();

                    if(document.getElementById("tanggal_masuk_filter").disabled){d.tanggal_masuk_filter = '';
                    }else{d.tanggal_masuk_filter = $('.tanggal_masuk_filter').val();}

                    if(document.getElementById("tanggal_awal_k1_filter").disabled){d.tanggal_awal_k1_filter = '';
                    }else{d.tanggal_awal_k1_filter = $('.tanggal_awal_k1_filter').val();}

                    if(document.getElementById("tanggal_akhir_k1_filter").disabled){d.tanggal_akhir_k1_filter = '';
                    }else{d.tanggal_akhir_k1_filter = $('.tanggal_akhir_k1_filter').val();}

                    if(document.getElementById("tanggal_awal_k2_filter").disabled){d.tanggal_awal_k2_filter = '';
                    }else{d.tanggal_awal_k2_filter = $('.tanggal_awal_k2_filter').val();}

                    if(document.getElementById("tanggal_akhir_k2_filter").disabled){d.tanggal_akhir_k2_filter = '';
                    }else{d.tanggal_akhir_k2_filter = $('.tanggal_akhir_k2_filter').val();}

                    if(document.getElementById("tanggal_tetap_filter").disabled){d.tanggal_tetap_filter = '';
                    }else{d.tanggal_tetap_filter = $('.tanggal_tetap_filter').val();}

					d.status_kerja_filter = $('.status_kerja_filter').val();
					d.status_mp_filter = $('.status_mp_filter').val();

                    if(document.getElementById("tanggal_out_filter").disabled){d.tanggal_out_filter = '';
                    }else{d.tanggal_out_filter = $('.tanggal_out_filter').val();}

					d.status_foh_filter = $('.status_foh_filter').val();
					d.departement_filter = $('.departement_filter').val();
					d.section_filter = $('.section_filter').val();
					d.shift_filter = $('.shift_filter').val();
					d.area_filter = $('.area_filter').val();
					d.jenis_kelamin_filter = $('.jenis_kelamin_filter').val();
					d.line_filter = $('.line_filter').val();
					d.pic_filter = $('.pic_filter').val();
					d.jumlah_sp_filter = $('.jumlah_sp_filter').val();
					d.jenis_sp_filter = $('.jenis_sp_filter').val();

                    if(document.getElementById("tanggal_sp_filter").disabled){d.tanggal_sp_filter = '';
                    }else{d.tanggal_sp_filter = $('.tanggal_sp_filter').val();}

					d.status_dl_idl_filter = $('.status_dl_idl_filter').val();
					d.aktif_filter = $('.aktif_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [21],//post type
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case '1':
                                return '<span class="badge bg-success">Ada</span>';
                                break;
                            case '0':
                                return '<span class="badge bg-danger">Tidak Ada</span>';
                                break;
                        }
                    }
                },
                {
                    "aTargets": [22],
                    "mRender": function(data, type, row, meta) {
                        return  '<div style="display:block;width:150px"><a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i> Ubah</a> ' +
                                '<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" ><i class="fa fa-ban"></i> Hapus</a></div>';
                    }
                }
            ]
        });

        $('.show-data').click(function(){
            dataTable.draw();
        });
        
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
                get_search_value = $(this).val();
				dataTable.search($(this).val()).draw();
			}
        });

    });

    $('#tanggal_masuk_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_masuk_filter")
        }else{
            disable_input("tanggal_masuk_filter")
        }
    });

    $('#tanggal_awal_k1_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_awal_k1_filter")
        }else{
            disable_input("tanggal_awal_k1_filter")
        }
    });
    
    $('#tanggal_akhir_k1_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_akhir_k1_filter")
        }else{
            disable_input("tanggal_akhir_k1_filter")
        }
    });
    
    $('#tanggal_awal_k2_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_awal_k2_filter")
        }else{
            disable_input("tanggal_awal_k2_filter")
        }
    });
    
    $('#tanggal_akhir_k2_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_akhir_k2_filter")
        }else{
            disable_input("tanggal_akhir_k2_filter")
        }
    });
    
    $('#tanggal_tetap_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_tetap_filter")
        }else{
            disable_input("tanggal_tetap_filter")
        }
    });
    
    $('#tanggal_out_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_out_filter")
        }else{
            disable_input("tanggal_out_filter")
        }
    });
    
    $('#tanggal_sp_checkbox').change(function() {
        if(this.checked) {
            enable_input("tanggal_sp_filter")
        }else{
            disable_input("tanggal_sp_filter")
        }
    });
    
    function disable_input(elm_id) {
        document.getElementById(elm_id).disabled = true;
    }

    function enable_input(elm_id) {
        document.getElementById(elm_id).disabled = false;
    }

</script>