<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="action-form" width="100%">
<div id="modal-import" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hilabelen="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="tab-content pt-1" id="custom-content-below-tabContent">
				<div class="form-message text-center"></div>
				<div class="row">
					<div class="col-md-6">
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">NIM</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nim" placeholder="Nomor Induk Magang" name="var_nim" value="<?=isset($data->var_nim)? $data->var_nim : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">NIK</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nik" placeholder="Nomor Induk Karyawan" name="var_nik" value="<?=isset($data->var_nik)? $data->var_nik : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Batch</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_batch" placeholder="Batch" name="var_batch" value="<?=isset($data->var_batch)? $data->var_batch : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nama Karyawan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nama" placeholder="Nama Karyawan" name="var_nama" value="<?=isset($data->var_nama)? $data->var_nama : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Jabatan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_jabatan" placeholder="Jabatan" name="var_jabatan" value="<?=isset($data->var_jabatan)? $data->var_jabatan : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Pekerjaan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_pekerjaan" placeholder="Pekerjaan" name="var_pekerjaan" value="<?=isset($data->var_pekerjaan)? $data->var_pekerjaan : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Masuk</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_masuk" name="dt_tanggal_masuk" value="<?=isset($data->dt_tanggal_masuk) ? ($data->dt_tanggal_masuk == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_masuk) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Awal K1</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_awal_k1" name="dt_tanggal_awal_k1" value="<?=isset($data->dt_tanggal_awal_k1) ? ($data->dt_tanggal_awal_k1 == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_awal_k1) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Akhir K1</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_awal_k2" name="dt_tanggal_awal_k2" value="<?=isset($data->dt_tanggal_akhir_k1) ? ($data->dt_tanggal_awal_k2 == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_akhir_k1) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Awal K2</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_awal_k2" name="dt_tanggal_awal_k2" value="<?=isset($data->dt_tanggal_awal_k2) ? ($data->dt_tanggal_awal_k2 == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_awal_k2) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Akhir K2</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_akhir_k2" name="dt_tanggal_akhir_k2" value="<?=isset($data->dt_tanggal_akhir_k2) ? ($data->dt_tanggal_akhir_k2 == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_akhir_k2) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Tetap</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_tetap" name="dt_tanggal_tetap" value="<?=isset($data->dt_tanggal_tetap) ? ($data->dt_tanggal_tetap == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_tetap) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Status Kerja</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_status_kerja" placeholder="Status Kerja" name="var_status_kerja" value="<?=isset($data->var_status_kerja)? $data->var_status_kerja : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Status MP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_status_mp" placeholder="Status MP" name="var_status_mp" value="<?=isset($data->var_status_mp)? $data->var_status_mp : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Out</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_out" name="dt_tanggal_out" value="<?=isset($data->dt_tanggal_out) ? ($data->dt_tanggal_out == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_out) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Sumber Laporan MP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_sumber_data_laporan_mp" placeholder="Sumber Laporan MP" name="var_sumber_data_laporan_mp" value="<?=isset($data->var_sumber_data_laporan_mp)? $data->var_sumber_data_laporan_mp : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Alasan Resign</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_alasan_resign" placeholder="Alasan Resign" name="var_alasan_resign" value="<?=isset($data->var_alasan_resign)? $data->var_alasan_resign : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Status FOH</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_status_foh" placeholder="Status FOH" name="var_status_foh" value="<?=isset($data->var_status_foh)? $data->var_status_foh : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">Departement</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_departement" placeholder="Departement" name="var_departement" value="<?=isset($data->var_departement)? $data->var_departement : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Section</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_section" placeholder="Section" name="var_section" value="<?=isset($data->var_section)? $data->var_section : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Job</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_job" placeholder="Job" name="var_job" value="<?=isset($data->var_job)? $data->var_job : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Shift</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_shift" placeholder="Shift" name="var_shift" value="<?=isset($data->var_shift)? $data->var_shift : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Area</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_area" placeholder="Area" name="var_area" value="<?=isset($data->var_area)? $data->var_area : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Line</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_line" placeholder="Line" name="var_line" value="<?=isset($data->var_line)? $data->var_line : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Conveyor</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_conveyor" placeholder="Conveyor" name="var_conveyor" value="<?=isset($data->var_conveyor)? $data->var_conveyor : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">Dusun</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_dusun" placeholder="Dusun" name="var_dusun" value="<?=isset($data->var_dusun)? $data->var_dusun : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">RT/RW</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_rt_rw" placeholder="RT/RW" name="var_rt_rw" value="<?=isset($data->var_rt_rw)? $data->var_rt_rw : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Kelurahan/Desa</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_desa" placeholder="Kelurahan/Desa" name="var_desa" value="<?=isset($data->var_desa)? $data->var_desa : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Kecamatan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_kecamatan" placeholder="Kecamatan" name="var_kecamatan" value="<?=isset($data->var_kecamatan)? $data->var_kecamatan : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Kabupaten/Kota</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_kab_kota" placeholder="Kabupaten/Kota" name="var_kab_kota" value="<?=isset($data->var_kab_kota)? $data->var_kab_kota : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Alamat KTP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_alamat_ktp" placeholder="Alamat KTP" name="var_alamat_ktp" value="<?=isset($data->var_alamat_ktp)? $data->var_alamat_ktp : ''?>" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tempat Lahir</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_lokasi_lahir" placeholder="Tempat Lahir" name="var_lokasi_lahir" value="<?=isset($data->var_lokasi_lahir)? $data->var_lokasi_lahir : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Lahir</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_lahir" name="dt_tanggal_lahir" value="<?=isset($data->dt_tanggal_lahir) ? ($data->dt_tanggal_lahir == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_lahir) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">No. Telepon</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm text-right" id="var_telepon" placeholder="No. Telepon" name="var_telepon" value="<?=isset($data->var_telepon)? $data->var_telepon : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Jenis Kelamin</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_jenis_kelamin" placeholder="Jenis Kelamin" name="var_jenis_kelamin" value="<?=isset($data->var_jenis_kelamin)? $data->var_jenis_kelamin : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Status Perkawinan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_status_perkawinan" placeholder="Status Perkawinan" name="var_status_perkawinan" value="<?=isset($data->var_status_perkawinan)? $data->var_status_perkawinan : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Agama</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_agama" placeholder="Agama" name="var_agama" value="<?=isset($data->var_agama)? $data->var_agama : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Sekolah</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_sekolah" placeholder="Sekolah" name="var_sekolah" value="<?=isset($data->var_sekolah)? $data->var_sekolah : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Jurusan</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_jurusan" placeholder="Jurusan" name="var_jurusan" value="<?=isset($data->var_jurusan)? $data->var_jurusan : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nama Ibu</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nama_ibu" placeholder="Nama Ibu" name="var_nama_ibu" value="<?=isset($data->var_nama_ibu)? $data->var_nama_ibu : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nama Ayah</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nama_ayah" placeholder="Nama Ayah" name="var_nama_ayah" value="<?=isset($data->var_nama_ayah)? $data->var_nama_ayah : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Ket. Mp. Hamil</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_mp_hamil" placeholder="Keterangan Mp. hamil" name="var_ket_mp_hamil" value="<?=isset($data->var_ket_mp_hamil)? $data->var_ket_mp_hamil : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">Status CK/CLT</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_status_ck_clt" placeholder="Status CK/CLT" name="var_status_ck_clt" value="<?=isset($data->var_status_ck_clt)? $data->var_status_ck_clt : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Keterangan CK/CLT</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_keterangan_ck_clt" placeholder="Keterangan CK/CLT" name="var_keterangan_ck_clt" value="<?=isset($data->var_keterangan_ck_clt)? $data->var_keterangan_ck_clt : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal Mulai CK/CLT</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_mulai_ck_clt" name="dt_tanggal_mulai_ck_clt" value="<?=isset($data->dt_tanggal_mulai_ck_clt) ? ($data->dt_tanggal_mulai_ck_clt == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_mulai_ck_clt) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Perkiraan Selesai CK/CLT</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_perkiraan_selesai_ck_clt" name="dt_perkiraan_selesai_ck_clt" value="<?=isset($data->dt_perkiraan_selesai_ck_clt) ? ($data->dt_perkiraan_selesai_ck_clt == '1970-01-01' ? '0000-01-01' :  $data->dt_perkiraan_selesai_ck_clt) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Sumber Data CK/CLT</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_sumber_data_ck_clt" placeholder="Sumber Data CK/CLT" name="var_sumber_data_ck_clt" value="<?=isset($data->var_sumber_data_ck_clt)? $data->var_sumber_data_ck_clt : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">Ket. Masa Kerja Eks SAI</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_masakerja_eks_sai" placeholder="Keterangan Masa Kerja Eks SAI" name="var_ket_masakerja_eks_sai" value="<?=isset($data->var_ket_masakerja_eks_sai)? $data->var_ket_masakerja_eks_sai : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Job Eks</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_job_eks" placeholder="Job Eks" name="var_job_eks" value="<?=isset($data->var_job_eks)? $data->var_job_eks : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Ket. Difabel</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_ket_difabel" placeholder="Keterangan Difabel" name="var_ket_difabel" value="<?=isset($data->var_ket_difabel)? $data->var_ket_difabel : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nomor KTP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_no_ktp" placeholder="Nomor KTP" name="var_no_ktp" value="<?=isset($data->var_no_ktp)? $data->var_no_ktp : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">No. Kartu Keluarga</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_no_kk" placeholder="Nomor Kartu Keluarga" name="var_no_kk" value="<?=isset($data->var_no_kk)? $data->var_no_kk : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Alamat Tinggal</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_alamat_tinggal" placeholder="Alamat Tinggal" name="var_alamat_tinggal" value="<?=isset($data->var_alamat_tinggal)? $data->var_alamat_tinggal : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">NPWP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_npwp" placeholder="NPWP" name="var_npwp" value="<?=isset($data->var_npwp)? $data->var_npwp : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nama Suami/Istri</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nama_suami_istri" placeholder="Nama Suami/Istri" name="var_nama_suami_istri" value="<?=isset($data->var_nama_suami_istri)? $data->var_nama_suami_istri : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nama Anak Ke-1</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nama_anak_1" placeholder="Nama Anak Ke-1" name="var_nama_anak_1" value="<?=isset($data->var_nama_anak_1)? $data->var_nama_anak_1 : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nama Anak Ke-2</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nama_anak_2" placeholder="Nama Anak Ke-2" name="var_nama_anak_2" value="<?=isset($data->var_nama_anak_2)? $data->var_nama_anak_2 : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Nama Anak Ke-3</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_nama_anak_3" placeholder="Nama Anak Ke-3" name="var_nama_anak_3" value="<?=isset($data->var_nama_anak_3)? $data->var_nama_anak_3 : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Email</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_email" placeholder="Email" name="var_email" value="<?=isset($data->var_email)? $data->var_email : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Jumlah Anak</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm text-right" id="var_jumlah_anak" placeholder="Jumlah Anak" name="var_jumlah_anak" value="<?=isset($data->var_jumlah_anak)? $data->var_jumlah_anak : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
								<div class="form-group row mb-1">
									<label class="col-sm-4">PIC SPV</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_pic_spv" placeholder="PIC SPV" name="var_pic_spv" value="<?=isset($data->var_pic_spv)? $data->var_pic_spv : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Jumlah SP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm text-right" id="var_jumlah_sp" placeholder="Jumlah SP" name="var_jumlah_sp" value="<?=isset($data->var_jumlah_sp)? $data->var_jumlah_sp : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Jenis SP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_jenis_sp" placeholder="Jenis SP" name="var_jenis_sp" value="<?=isset($data->var_jenis_sp)? $data->var_jenis_sp : ''?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Tanggal SP</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm date_picker text-right" id="dt_tanggal_sp" name="dt_tanggal_sp" value="<?=isset($data->dt_tanggal_sp) ? ($data->dt_tanggal_sp == '1970-01-01' ? '0000-01-01' :  $data->dt_tanggal_sp) : '0000-01-01'?>" />
									</div>
								</div>
								<div class="form-group row mb-1">
									<label class="col-sm-4">Status DL/IDL</label>
									<div class="col-sm-8" >
										<input type="text" class="form-control form-control-sm" id="var_status_dl_idl" placeholder="Status DL/IDL" name="var_status_dl_idl" value="<?=isset($data->var_status_dl_idl)? $data->var_status_dl_idl : ''?>" />
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row mb-1">
							<label for="Status" class="col-sm-2 col-form-label">Status Data</label>
							<div class="col-sm-10 mt-1">
								<div class="icheck-primary d-inline mr-2">
									<input type="radio" id="radioPrimary1" name="int_status" value="1" <?=isset($data->int_status)? (($data->int_status == 1)? 'checked' : '') : 'checked' ?>>
										<label for="radioPrimary1">Aktif </label>
								</div>
								<div class="icheck-danger d-inline">
									<input type="radio" id="radioPrimary2" name="int_status" value="0" <?=isset($data->int_status)? (($data->int_status == 0)? 'checked' : '') : '' ?>>
									<label for="radioPrimary2">Tidak Aktif</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.select2').select2();
		$('.date_picker').daterangepicker(datepickModal);
		bsCustomFileInput.init();
		$('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
		$("#action-form").validate({
			rules: {
				var_nik:{
					required: true,
					digits: true,
					minlength: 1,
				},
				var_nim:{
					required: true,
					digits: true,
					minlength: 1,
				},
				int_status: {
					required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#action-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>