<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="batch_filter" class="col-md-5 col-form-label">Batch</label>
									<div class="col-md-7">
                                        <select id="batch_filter" name="batch_filter" class="form-control form-control-sm batch_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA BATCH -</option>
                                            <?php 
                                                foreach($batch_list as $bl){
                                                    echo '<option value="'.$bl->var_batch.'">'.$bl->var_batch.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="jabatan_filter" class="col-md-5 col-form-label">Jabatan</label>
									<div class="col-md-7">
                                        <select id="jabatan_filter" name="jabatan_filter" class="form-control form-control-sm jabatan_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA JABATAN -</option>
                                            <option value="DEPUTY FACTORY MANAGER">DEPUTY FACTORY MANAGER</option>
                                            <option value="FACTORY MANAGER">FACTORY MANAGER</option>
                                            <option value="FOREMAN">FOREMAN</option>
                                            <option value="GENERAL MANAGER">GENERAL MANAGER</option>
                                            <option value="GL">GL</option>
                                            <option value="JR. MANAGER BUSINESS DEV.">JR. MANAGER BUSINESS DEV.</option>
                                            <option value="JUNIOR MANAGER ">JUNIOR MANAGER </option>
                                            <option value="JUNIOR SUPERVISOR">JUNIOR SUPERVISOR</option>
                                            <option value="LINE LEADER">LINE LEADER</option>
                                            <option value="MANAGER ">MANAGER </option>
                                            <option value="PRESIDENT DIRECTUR">PRESIDENT DIRECTUR</option>
                                            <option value="SENIOR MANAGER">SENIOR MANAGER</option>
                                            <option value="SENIOR SUPERVISOR">SENIOR SUPERVISOR</option>
                                            <option value="SUPERVISOR">SUPERVISOR</option>
                                            <option value="VICE PRESIDENT DIRECTUR">VICE PRESIDENT DIRECTUR</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="pekerjaan_filter" class="col-md-5 col-form-label">Pekerjaan</label>
									<div class="col-md-7">
                                        <select id="pekerjaan_filter" name="pekerjaan_filter" class="form-control form-control-sm pekerjaan_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA PEKERJAAN -</option>
                                            <option value="ADMINISTRASI">ADMINISTRASI</option>
                                            <option value="CASHIER">CASHIER</option>
                                            <option value="ESO">ESO</option>
                                            <option value="MEDIS">MEDIS</option>
                                            <option value="OPERATOR">OPERATOR</option>
                                            <option value="SECURITY">SECURITY</option>
                                            <option value="TEKNISI">TEKNISI</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_masuk_filter" class="col-md-5 col-form-label">Tanggal Masuk</label>
									<div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_masuk_checkbox">
                                            <label class="custom-control-label" for="tanggal_masuk_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_masuk_filter" id="tanggal_masuk_filter" class="form-control form-control-sm tanggal_masuk_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_awal_k1_filter" class="col-md-5 col-form-label">Tanggal Awal K1</label>
									<div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_awal_k1_checkbox">
                                            <label class="custom-control-label" for="tanggal_awal_k1_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_awal_k1_filter" id="tanggal_awal_k1_filter" class="form-control form-control-sm tanggal_awal_k1_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_akhir_k1_filter" class="col-md-5 col-form-label">Tanggal Akhir K1</label>
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_akhir_k1_checkbox">
                                            <label class="custom-control-label" for="tanggal_akhir_k1_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_akhir_k1_filter" id="tanggal_akhir_k1_filter" class="form-control form-control-sm tanggal_akhir_k1_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_awal_k2_filter" class="col-md-5 col-form-label">Tanggal Awal K2</label>
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_awal_k2_checkbox">
                                            <label class="custom-control-label" for="tanggal_awal_k2_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_awal_k2_filter" id="tanggal_awal_k2_filter" class="form-control form-control-sm tanggal_awal_k2_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_akhir_k2_filter" class="col-md-5 col-form-label">Tanggal Akhir K2</label>
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_akhir_k2_checkbox">
                                            <label class="custom-control-label" for="tanggal_akhir_k2_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_akhir_k2_filter" id="tanggal_akhir_k2_filter" class="form-control form-control-sm tanggal_akhir_k2_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_tetap_filter" class="col-md-5 col-form-label">Tanggal Tetap</label>
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_tetap_checkbox">
                                            <label class="custom-control-label" for="tanggal_tetap_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_tetap_filter" id="tanggal_tetap_filter" class="form-control form-control-sm tanggal_tetap_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="status_kerja_filter" class="col-md-5 col-form-label">Status Kerja</label>
									<div class="col-md-7">
                                        <select id="status_kerja_filter" name="status_kerja_filter" class="form-control form-control-sm status_kerja_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA -</option>
                                            <option value="IN CLASS">IN CLASS</option>
                                            <option value="KONTRAK 1">KONTRAK 1</option>
                                            <option value="KONTRAK 1 (IN CLASS)">KONTRAK 1 (IN CLASS)</option>
                                            <option value="KONTRAK 2">KONTRAK 2</option>
                                            <option value="KONTRAK 3">KONTRAK 3</option>
                                            <option value="MAGANG">MAGANG</option>
                                            <option value="MAGANG 1">MAGANG 1</option>
                                            <option value="MAGANG 2">MAGANG 2</option>
                                            <option value="TETAP">TETAP</option>
                                            <option value="TETAP (EXPATRIAT)">TETAP (EXPATRIAT)</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="status_mp_filter" class="col-md-5 col-form-label">Status MP</label>
									<div class="col-md-7">
                                        <select id="status_mp_filter" name="status_mp_filter" class="form-control form-control-sm status_mp_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA -</option>
                                            <option value="ACTIVE">ACTIVE</option>
                                            <option value="FAILED">FAILED</option>
                                            <option value="RESIGN">RESIGN</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_out_filter" class="col-md-5 col-form-label">Tanggal Out</label>
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_out_checkbox">
                                            <label class="custom-control-label" for="tanggal_out_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_out_filter" id="tanggal_out_filter" class="form-control form-control-sm tanggal_out_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="status_foh_filter" class="col-md-5 col-form-label">Status FOH</label>
									<div class="col-md-7">
                                        <select id="status_foh_filter" name="status_foh_filter" class="form-control form-control-sm status_foh_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA FOH -</option>
                                            <option value="BKN FOH">BKN FOH</option>
                                            <option value="BKN FOH (IN CLASS )">BKN FOH (IN CLASS )</option>
                                            <option value="BKN FOH(GENBA)">BKN FOH(GENBA)</option>
                                            <option value="FOH">FOH</option>
                                            <option value="GENBA(INCLASS)">GENBA(INCLASS)</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4 pt-5"></div>
                            <div class="col-md-4 pt-5"></div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="departement_filter" class="col-md-5 col-form-label">Departement</label>
									<div class="col-md-7">
                                        <select id="departement_filter" name="departement_filter" class="form-control form-control-sm departement_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA DEPARTEMENT -</option>
                                            <option value="ACCOUNTING">ACCOUNTING</option>
                                            <option value="ENGINEERING">ENGINEERING</option>
                                            <option value="FA">FA</option>
                                            <option value="FINANCE">FINANCE</option>
                                            <option value="MAINTENANCE">MAINTENANCE</option>
                                            <option value="NYS">NYS</option>
                                            <option value="PGA">PGA</option>
                                            <option value="PPIC">PPIC</option>
                                            <option value="PRODUCTION PREPARATION">PRODUCTION PREPARATION</option>
                                            <option value="PRODUKSI">PRODUKSI</option>
                                            <option value="QUALITY ASSURANCE">QUALITY ASSURANCE</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="section_filter" class="col-md-5 col-form-label">Section</label>
									<div class="col-md-7">
                                        <select id="section_filter" name="section_filter" class="form-control form-control-sm section_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA SECTION -</option>
                                            <option value="ACCOUNTING">ACCOUNTING</option>
                                            <option value="ACCOUNTING ( TAX )">ACCOUNTING ( TAX )</option>
                                            <option value="ACCOUNTING (PRICING)">ACCOUNTING (PRICING)</option>
                                            <option value="DESIGN & STANDARD ENGINEERING">DESIGN & STANDARD ENGINEERING</option>
                                            <option value="ENGINEERING">ENGINEERING</option>
                                            <option value="EQUIPMENT CONTROL">EQUIPMENT CONTROL</option>
                                            <option value="EXIM">EXIM</option>
                                            <option value="FA">FA</option>
                                            <option value="FINANCE">FINANCE</option>
                                            <option value="GENERAL AFFAIR">GENERAL AFFAIR</option>
                                            <option value="HUMAN RESOURCES">HUMAN RESOURCES</option>
                                            <option value="IC">IC</option>
                                            <option value="INDUSTRIAL RELATION">INDUSTRIAL RELATION</option>
                                            <option value="INFORMATION TECHNOLOGI">INFORMATION TECHNOLOGI</option>
                                            <option value="INTERPRETER">INTERPRETER</option>
                                            <option value="MAINTENANCE">MAINTENANCE</option>
                                            <option value="MAINTENANCE & EQUIPMENT CONTROL">MAINTENANCE & EQUIPMENT CONTROL</option>
                                            <option value="MPC">MPC</option>
                                            <option value="NYS">NYS</option>
                                            <option value="PGA">PGA</option>
                                            <option value="PPC">PPC</option>
                                            <option value="PPIC">PPIC</option>
                                            <option value="PPIC & PURCHASE">PPIC & PURCHASE</option>
                                            <option value="PROCESS ENGINEERING">PROCESS ENGINEERING</option>
                                            <option value="PRODUCTION ENGINEERING">PRODUCTION ENGINEERING</option>
                                            <option value="PRODUCTION PREPARATION">PRODUCTION PREPARATION</option>
                                            <option value="PRODUCTION PREPARATION ">PRODUCTION PREPARATION </option>
                                            <option value="PRODUKSI">PRODUKSI</option>
                                            <option value="PURCHASE ENGINEERING">PURCHASE ENGINEERING</option>
                                            <option value="Q.S.A">Q.S.A</option>
                                            <option value="QC - CHECK WH">QC - CHECK WH</option>
                                            <option value="QC - INPECTION">QC - INPECTION</option>
                                            <option value="QC - MSA">QC - MSA</option>
                                            <option value="QC.  MSA">QC.  MSA</option>
                                            <option value="QC. CHECK W/H">QC. CHECK W/H</option>
                                            <option value="QC. INSPECT STANDARD">QC. INSPECT STANDARD</option>
                                            <option value="QC. LABORATORY">QC. LABORATORY</option>
                                            <option value="QC. RECEIVING">QC. RECEIVING</option>
                                            <option value="QSA">QSA</option>
                                            <option value="QUALITY ASSURANCE">QUALITY ASSURANCE</option>
                                            <option value="SECURITY">SECURITY</option>
                                            <option value="TRAINEE IN CLASS">TRAINEE IN CLASS</option>
                                            <option value="TRAINING">TRAINING</option>
                                            <option value="WAREHOUSE">WAREHOUSE</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="shift_filter" class="col-md-5 col-form-label">Shift</label>
									<div class="col-md-7">
                                        <select id="shift_filter" name="shift_filter" class="form-control form-control-sm shift_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA SHIFT -</option>
                                            <option value="SHIFT A">SHIFT A</option>
                                            <option value="SHIFT B">SHIFT B</option>
                                            <option value="NON SHIFT">NON SHIFT</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="area_filter" class="col-md-5 col-form-label">Area</label>
									<div class="col-md-7">
                                        <select id="area_filter" name="area_filter" class="form-control form-control-sm area_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA AREA -</option>
                                            <option value="FINAL ASSY">FINAL ASSY</option>
                                            <option value="INSPEC FINAL ASSY">INSPEC FINAL ASSY</option>
                                            <option value="INSPEC PRE ASSY">INSPEC PRE ASSY</option>
                                            <option value="INSPECT FINAL ASSY">INSPECT FINAL ASSY</option>
                                            <option value="INSPECT FINAL ASSY ">INSPECT FINAL ASSY </option>
                                            <option value="INSPECT PRE ASSY">INSPECT PRE ASSY</option>
                                            <option value="INSPECT PRE ASSY ">INSPECT PRE ASSY </option>
                                            <option value="OFFICE">OFFICE</option>
                                            <option value="PRE ASSY">PRE ASSY</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="jenis_kelamin_filter" class="col-md-5 col-form-label">Jenis Kelamin</label>
									<div class="col-md-7">
                                        <select id="jenis_kelamin_filter" name="jenis_kelamin_filter" class="form-control form-control-sm jenis_kelamin_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA -</option>
                                            <option value="LAKI - LAKI">LAKI - LAKI</option>
                                            <option value="PEREMPUAN">PEREMPUAN</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="line_filter" class="col-md-5 col-form-label">Status Perkawinan</label>
									<div class="col-md-7">
                                        <select id="line_filter" name="line_filter" class="form-control form-control-sm line_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA Status -</option>
                                            <option value="DUDA">DUDA</option>
                                            <option value="JANDA">JANDA</option>
                                            <option value="KAWIN">KAWIN</option>
                                            <option value="SINGLE">SINGLE</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="pic_filter" class="col-md-5 col-form-label">PIC</label>
									<div class="col-md-7">
                                        <select id="pic_filter" name="pic_filter" class="form-control form-control-sm pic_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA PIC -</option>
                                            <option value="AEP">AEP</option>
                                            <option value="AES">AES</option>
                                            <option value="BSU">BSU</option>
                                            <option value="DFH">DFH</option>
                                            <option value="DFH/RWU">DFH/RWU</option>
                                            <option value="DKP">DKP</option>
                                            <option value="ENS">ENS</option>
                                            <option value="LMA">LMA</option>
                                            <option value="LNA">LNA</option>
                                            <option value="NAF">NAF</option>
                                            <option value="NUR">NUR</option>
                                            <option value="OVI">OVI</option>
                                            <option value="PPT">PPT</option>
                                            <option value="RAY">RAY</option>
                                            <option value="RWU">RWU</option>
                                            <option value="SNM">SNM</option>
                                            <option value="SSI">SSI</option>
                                            <option value="TSD">TSD</option>
                                            <option value="TWI">TWI</option>
                                            <option value="VSU">VSU</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="jumlah_sp_filter" class="col-md-5 col-form-label">Jumlah SP</label>
									<div class="col-md-7">
                                        <select id="jumlah_sp_filter" name="jumlah_sp_filter" class="form-control form-control-sm jumlah_sp_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA -</option>
                                            <?php 
                                                foreach($jumlah_sp_list as $sp){
                                                    echo '<option value="'.$sp->var_jumlah_sp.'">'.$sp->var_jumlah_sp.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="jenis_sp_filter" class="col-md-5 col-form-label">Jenis SP</label>
									<div class="col-md-7">
                                        <select id="jenis_sp_filter" name="jenis_sp_filter" class="form-control form-control-sm jenis_sp_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA JENIS SP -</option>
                                            <option value="SP LISAN">SP LISAN</option>
                                            <option value="SP 1">SP 1</option>
                                            <option value="SP 2">SP 2</option>
                                            <option value="SP 3">SP 3</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_sp_filter" class="col-md-5 col-form-label">Tanggal SP</label>
                                    <div class="col-md-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_sp_checkbox">
                                            <label class="custom-control-label" for="tanggal_sp_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="tanggal_sp_filter" id="tanggal_sp_filter" class="form-control form-control-sm tanggal_sp_filter date_picker text-right" disabled>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="status_dl_idl_filter" class="col-md-5 col-form-label">Status DL/IDL</label>
									<div class="col-md-7">
                                        <select id="status_dl_idl_filter" name="status_dl_idl_filter" class="form-control form-control-sm status_dl_idl_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA STATUS -</option>
                                            <option value="DL">DL</option>
                                            <option value="IDL">IDL</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="aktif_filter" class="col-md-5 col-form-label">Status Database</label>
									<div class="col-md-7">
                                        <select id="aktif_filter" name="aktif_filter" class="form-control form-control-sm aktif_filter select2" style="width: 100%;">
                                            <option value="1">Ada</option>
                                            <option value="0">Tidak Ada</option>
                                        </select>
									</div>
								</div>
                            </div>
						</div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="button" class="btn btn-sm btn-warning" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-download"></i> Export Excel</button>
                    <button type="button" data-block="body" class="btn btn-sm btn-success ajax_modal" data-url="<?=$url?>/import" style="margin-right:20px;"><i class="fas fa-file-excel"></i> Import Excel</button>
                    <button type="button" data-block="body" class="btn btn-sm btn-secondary ajax_modal" data-url="<?=$url?>/add" ><i class="fas fa-plus"></i> Tambah Data</button>
                    <button type="button" data-block="body" class="btn btn-sm btn-primary show-data" id="show_data" style="margin-right:20px;"><i class="fas fa-list-alt"></i> Tampilkan Data</button>
                    <button type="button" data-block="body" class="btn btn-sm btn-danger ajax_modal" data-url="<?=$url?>/del_custom"><i class="fas fa-trash"></i></button>
                </div>
            </div>
            <div class="card card-outline">
                <div class="card-body p-0" style="font-size:10px">
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIM</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th><span style="width:100px;display:inline-block">Sumber Lap. MP</span></th>
                            <th>Job</th>
                            <th>Line</th>
                            <th>Conveyor</th>
                            <th>Dusun</th>
                            <th>RT/RW</th>
                            <th>Kelurahan/Desa</th>
                            <th>Kecamatan</th>
                            <th>Kabupaten/Kota</th>
                            <th>Alamat</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th><span style="width:70px;display:inline-block">No. Telepon</span></th>
                            <th>Agama</th>
                            <th>No. KTP</th>
                            <th>No. KK</th>
                            <th>Email</th>
                            <th><span style="width:60px;display:inline-block">Status DB</span></th>
                            <th>#</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
