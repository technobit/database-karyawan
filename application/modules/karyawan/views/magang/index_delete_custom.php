<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="delete-form" width="80%">
<div id="modal-delete" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<input type="hidden" id="user_id" name="user_id" value="1"/>
			<div class="card card-warning">
				<div class="card-header">
					<h3 class="card-title">Hapus NIK/NIM Tertentu</h3>
				</div>
				<div class="card-body">
					<div class="form-group row mb-1">
						<textarea class="form-control form-control-sm textarea" name="var_custom" id="var_custom" placeholder="Contoh : 000001, 000022, 000333, 004444, 055555, 666666"></textarea>
					</div>
					<div class="callout callout-warning mb-0">
						<p>Gunakan tanda koma ( <b>,</b> ) sebagai pemisah antar nomor. Hanya bisa menggunakan angka dengan format 6 (enam) digit<br>
						Contoh : <b>000001, 000022, 000333, 004444, 055555, 666666</b></p>
					</div>
				</div>
			</div>

			<div class="card card-danger">
				<div class="card-header">
					<h3 class="card-title">Hapus <i>Range</i> NIK/NIM Tertentu</h3>
				</div>
				<div class="card-body">
					<div class="form-group row mb-1">
						<label for="var_range" class="col-sm-2 col-form-label">Mulai</label>
						<div class="col-sm-4">
							<input type="text" id="var_range_start" name="var_range_start" class="form-control form-control-sm">
						</div>
						<label for="var_range" class="col-sm-2 col-form-label">Hingga</label>
						<div class="col-sm-4">
							<input type="text" id="var_range_end" name="var_range_end" class="form-control form-control-sm">
						</div>
					</div>
					<div class="callout callout-danger mb-0">
					<p>Hanya bisa menggunakan angka dengan format 6 (enam) digit<br>
						Contoh : <b>000001, 000022, 000333, 004444, 055555, 666666</b></p>
					</div>
				</div>
			</div>

			<div class="alert alert-danger alert-dismissible">
				<h5><i class="icon fas fa-exclamation-triangle"></i> Peringataan</h5>
				Operasi penghapusan data karyawan ini akan menghapus data karyawan pada sitem atau <i>database</i> yang secara permanen.
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" class="btn btn-danger">Hapus</button>
			<button type="button" data-dismiss="modal" class="btn btn-success">Batal</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){

		$("#delete-form").validate({
			rules: {
			    user_id:{
			        required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-delete';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#delete-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>