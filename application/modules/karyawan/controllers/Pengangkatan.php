<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Pengangkatan extends MX_Controller {
	private $input_file_name = 'data_ktp';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'PENGANGKATAN'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'karyawan';
		$this->routeURL = 'pengangkatan';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('pengangkatan_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Karyawan Magang';
		$this->page->menu 	  = 'karyawan';
		$this->page->submenu1 = 'pengangkatan';
		$this->breadcrumb->title = 'Pengangkatan Karyawan Kontrak';
		$this->breadcrumb->card_title = 'Data Karyawan Magang';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Karyawan', 'Pengangkatan Karyawan Kontrak'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}");
		$this->render_view('pengangkatan/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_nim, $d->var_no_ktp, $d->var_nama, $d->var_departement, $d->var_section, $d->var_job, $d->var_pekerjaan, $d->var_line, $d->int_karyawan_magang_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function save($int_karyawan_magang_id){
		$this->authCheckDetailAccess('c');

        $this->form_validation->set_rules('var_nik', 'Nomor Induk Karyawan', "required|is_unique[{$this->model->m_karyawan_kontrak}.var_nik]|min_length[1]");
        $this->form_validation->set_rules('var_nim', 'Nomor Induk Magang', "required|is_unique[{$this->model->m_karyawan_magang}.var_nik]|min_length[1]");
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $query = $this->model->create($int_karyawan_magang_id, $this->input->post());
			$this->set_json([  'stat' => true, 
								'mc' => $query, //modal close
								'msg' => "Data berhasil dibuat",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_karyawan_magang_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_karyawan_magang_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_karyawan_magang_id");
			$data['title']	= 'Pengangkatan Karyawan Kontrak';
			$this->load_view('pengangkatan/index_action', $data);
		}
		
	}
	
	public function import(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save-import");
		$data['title']      = 'Import Data';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('magang/index_import', $data, true);
		
	}

	public function save_import(){
		$this->authCheckDetailAccess('c');
		
		$start = start_time();
		$this->form_validation->set_rules('mulai', 'Mulai', 'required|integer');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			if(isset($_FILES[$this->input_file_name])){
				$config['upload_path']   = "./{$this->import_dir}"; 
				$config['allowed_types'] = 'xlsx|xls'; 
				$config['encrypt_name']  = true; 
				$config['max_size']      = 4096;  
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload($this->input_file_name)){
					$status  = $this->model->import($this->input->post(), $this->upload->data());
					$this->set_json([  'stat' => ($status !== false), 
								'mc' => false,//($status !== false), //modal close
								'time' => finish_time($start),
								'msg' => ($status !== false)? "Data berhasil di-import dengan {$status} baris data." : 'Data gagal di-import',
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
				} else {
					$this->set_json([  'stat' => false, 
										'mc' => false, //modal close
										'msg' => "Data gagal di-import. ".$this->upload->display_errors('',''),
										'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
									]);
				}
			} else {
				$this->set_json([  'stat' => false, 
									'mc' => false, //modal close
									'msg' => "Data gagal di-import",
									'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
								]);
			}

        }
	}
}
