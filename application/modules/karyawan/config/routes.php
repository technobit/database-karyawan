<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['kontrak']['get']                    = 'karyawan/kontrak';
$route['kontrak']['post']                   = 'karyawan/kontrak/list';
$route['kontrak/import']['get']             = 'karyawan/kontrak/import';
$route['kontrak/add']['get']                = 'karyawan/kontrak/add';
$route['kontrak/save']['post']              = 'karyawan/kontrak/save';
$route['kontrak/save-import']['post']       = 'karyawan/kontrak/save_import';
$route['kontrak/([a-zA-Z0-9]+)']['get']     = 'karyawan/kontrak/get/$1';
$route['kontrak/([a-zA-Z0-9]+)']['post']    = 'karyawan/kontrak/update/$1';
$route['kontrak/([a-zA-Z0-9]+)/del']['get'] = 'karyawan/kontrak/confirm/$1';
$route['kontrak/([a-zA-Z0-9]+)/del']['post']= 'karyawan/kontrak/delete/$1';
$route['kontrak/del_custom']['get']         = 'karyawan/kontrak/delete_form';
$route['kontrak/del_custom']['post']        = 'karyawan/kontrak/delete_custom';
$route['export/kontrak']['post']            = 'karyawan/kontrak/export';


$route['magang']['get']                     = 'karyawan/magang';
$route['magang']['post']                    = 'karyawan/magang/list';
$route['magang/import']['get']              = 'karyawan/magang/import';
$route['magang/add']['get']                 = 'karyawan/magang/add';
$route['magang/save']['post']               = 'karyawan/magang/save';
$route['magang/save-import']['post']        = 'karyawan/magang/save_import';
$route['magang/([a-zA-Z0-9]+)']['get']      = 'karyawan/magang/get/$1';
$route['magang/([a-zA-Z0-9]+)']['post']     = 'karyawan/magang/update/$1';
$route['magang/([a-zA-Z0-9]+)/del']['get']  = 'karyawan/magang/confirm/$1';
$route['magang/([a-zA-Z0-9]+)/del']['post'] = 'karyawan/magang/delete/$1';
$route['magang/del_custom']['get']          = 'karyawan/magang/delete_form';
$route['magang/del_custom']['post']         = 'karyawan/magang/delete_custom';
$route['export/magang']['post']             = 'karyawan/magang/export';

$route['pengangkatan']['get']                     = 'karyawan/pengangkatan';
$route['pengangkatan']['post']                    = 'karyawan/pengangkatan/list';
$route['pengangkatan/import']['get']              = 'karyawan/pengangkatan/import';
$route['pengangkatan/save-import']['post']        = 'karyawan/pengangkatan/save_import';
$route['pengangkatan/([a-zA-Z0-9]+)']['get']      = 'karyawan/pengangkatan/get/$1';
$route['pengangkatan/([a-zA-Z0-9]+)']['post']     = 'karyawan/pengangkatan/save/$1';