<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Magang_model extends MY_Model {

	public function batch_list(){
		$this->db->select("var_batch")
			->from($this->m_karyawan_magang)
			->group_by("var_batch");
			return $this->db->order_by('var_batch', 'ASC')->get()->result();
	}
		
	public function jumlah_sp_list(){
		$this->db->select("var_jumlah_sp")
			->from($this->m_karyawan_magang)
			->group_by("var_jumlah_sp");
			return $this->db->order_by('var_jumlah_sp', 'ASC')->get()->result();
	}
	
	public function list($batch_filter = '',
						$jabatan_filter = '',
						$pekerjaan_filter = '',
						$tanggal_masuk_filter = '',
						$status_kerja_filter = '',
						$status_mp_filter = '',
						$tanggal_out_filter = '',
						$status_foh_filter = '',
						$departement_filter = '',
						$section_filter = '',
						$shift_filter = '',
						$area_filter = '',
						$jenis_kelamin_filter = '',
						$line_filter = '',
						$status_dl_idl_filter = '',
						$aktif_filter = '',
	 					$filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){

		$this->db->select("*")
				->from($this->m_karyawan_magang." mkm")
				->where("(CHAR_LENGTH(var_nik) = 6 AND CHAR_LENGTH(var_nim) = 6) OR (var_nik = '' OR var_nim = '' OR var_nik = '-' OR var_nim = '-')");

		if($batch_filter != ''){$this->db->where('mkm.var_batch', $batch_filter);}
		if($jabatan_filter != ''){$this->db->where('mkm.var_jabatan', $jabatan_filter);}
		if($pekerjaan_filter != ''){$this->db->where('mkm.var_pekerjaan', $pekerjaan_filter);}
		if($tanggal_masuk_filter != ''){$this->db->where('mkm.dt_tanggal_masuk', $tanggal_masuk_filter);}
		if($status_kerja_filter != ''){$this->db->where('mkm.var_status_kerja', $status_kerja_filter);}
		if($status_mp_filter != ''){$this->db->where('mkm.var_status_mp', $status_mp_filter);}
		if($tanggal_out_filter != ''){$this->db->where('mkm.dt_tanggal_out', $tanggal_out_filter);}
		if($status_foh_filter != ''){$this->db->where('mkm.var_status_foh', $status_foh_filter);}
		if($departement_filter != ''){$this->db->where('mkm.var_departement', $departement_filter);}
		if($section_filter != ''){$this->db->where('mkm.var_section', $section_filter);}
		if($shift_filter != ''){$this->db->where('mkm.var_shift', $shift_filter);}
		if($area_filter != ''){$this->db->where('mkm.var_area', $area_filter);}
		if($jenis_kelamin_filter != ''){$this->db->where('mkm.var_jenis_kelamin', $jenis_kelamin_filter);}
		if($line_filter != ''){$this->db->where('mkm.var_line', $line_filter);}
		if($status_dl_idl_filter != ''){$this->db->where('mkm.var_status_dl_idl', $status_dl_idl_filter);}
		if($aktif_filter != ''){$this->db->where('mkm.int_status', $aktif_filter);}
		

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mkm.var_nim', $filter)
					->or_like('mkm.var_nik', $filter)
					->or_like('mkm.var_nama', $filter)
					->or_like('mkm.var_batch', $filter)
					->or_like('mkm.var_sumber_data_laporan_mp', $filter)
					->or_like('mkm.var_job', $filter)
					->or_like('mkm.var_line', $filter)
					->or_like('mkm.var_conveyor', $filter)
					->or_like('mkm.var_dusun', $filter)
					->or_like('mkm.var_rt_rw', $filter)
					->or_like('mkm.var_desa', $filter)
					->or_like('mkm.var_kecamatan', $filter)
					->or_like('mkm.var_kab_kota', $filter)
					->or_like('mkm.var_alamat_ktp', $filter)
					->or_like('mkm.var_lokasi_lahir', $filter)
					->or_like('mkm.dt_tanggal_lahir', $filter)
					->or_like('mkm.var_telepon', $filter)
					->or_like('mkm.var_agama', $filter)
					->or_like('mkm.var_no_ktp', $filter)
					->or_like('mkm.var_no_kk', $filter)
					->or_like('mkm.var_email', $filter)
					->group_end();
		}

		$order = 'var_nik ';
		switch($order_by){
			case 1 : $order = 'var_nim '; break;
			case 2 : $order = 'var_nik '; break;
			case 3 : $order = 'var_nama'; break;
			case 4 : $order = 'var_sumber_data_laporan_mp'; break;
			case 5 : $order = 'var_job'; break;
			case 6 : $order = 'var_line'; break;
			case 7 : $order = 'var_conveyor'; break;
			case 8 : $order = 'var_dusun'; break;
			case 9 : $order = 'var_rt_rw'; break;
			case 10 : $order = 'var_desa'; break;
			case 11 : $order = 'var_kecamatan'; break;
			case 12 : $order = 'var_kab_kota'; break;
			case 13 : $order = 'var_alamat_ktp'; break;
			case 14 : $order = 'var_lokasi_lahir'; break;
			case 15 : $order = 'dt_tanggal_lahir'; break;
			case 16 : $order = 'var_telepon'; break;
			case 17 : $order = 'var_agama'; break;
			case 18 : $order = 'var_no_ktp'; break;
			case 19 : $order = 'var_no_kk'; break;
			case 20 : $order = 'var_email'; break;

		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($batch_filter = '',
							$jabatan_filter = '',
							$pekerjaan_filter = '',
							$tanggal_masuk_filter = '',
							$status_kerja_filter = '',
							$status_mp_filter = '',
							$tanggal_out_filter = '',
							$status_foh_filter = '',
							$departement_filter = '',
							$section_filter = '',
							$shift_filter = '',
							$area_filter = '',
							$jenis_kelamin_filter = '',
							$line_filter = '',
							$status_dl_idl_filter = '',
							$aktif_filter = '',
							$filter = NULL){

		$this->db->select("*")
				->from($this->m_karyawan_magang." mkm")
				->where("(CHAR_LENGTH(var_nik) = 6 AND CHAR_LENGTH(var_nim) = 6) OR (var_nik = '' OR var_nim = '' OR var_nik = '-' OR var_nim = '-')");

		if($batch_filter != ''){$this->db->where('mkm.var_batch', $batch_filter);}
		if($jabatan_filter != ''){$this->db->where('mkm.var_jabatan', $jabatan_filter);}
		if($pekerjaan_filter != ''){$this->db->where('mkm.var_pekerjaan', $pekerjaan_filter);}
		if($tanggal_masuk_filter != ''){$this->db->where('mkm.dt_tanggal_masuk', $tanggal_masuk_filter);}
		if($status_kerja_filter != ''){$this->db->where('mkm.var_status_kerja', $status_kerja_filter);}
		if($status_mp_filter != ''){$this->db->where('mkm.var_status_mp', $status_mp_filter);}
		if($tanggal_out_filter != ''){$this->db->where('mkm.dt_tanggal_out', $tanggal_out_filter);}
		if($status_foh_filter != ''){$this->db->where('mkm.var_status_foh', $status_foh_filter);}
		if($departement_filter != ''){$this->db->where('mkm.var_departement', $departement_filter);}
		if($section_filter != ''){$this->db->where('mkm.var_section', $section_filter);}
		if($shift_filter != ''){$this->db->where('mkm.var_shift', $shift_filter);}
		if($area_filter != ''){$this->db->where('mkm.var_area', $area_filter);}
		if($jenis_kelamin_filter != ''){$this->db->where('mkm.var_jenis_kelamin', $jenis_kelamin_filter);}
		if($line_filter != ''){$this->db->where('mkm.var_line', $line_filter);}
		if($status_dl_idl_filter != ''){$this->db->where('mkm.var_status_dl_idl', $status_dl_idl_filter);}
		if($aktif_filter != ''){$this->db->where('mkm.int_status', $aktif_filter);}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mkm.var_nim', $filter)
					->or_like('mkm.var_nik', $filter)
					->or_like('mkm.var_nama', $filter)
					->or_like('mkm.var_batch', $filter)
					->or_like('mkm.var_sumber_data_laporan_mp', $filter)
					->or_like('mkm.var_job', $filter)
					->or_like('mkm.var_line', $filter)
					->or_like('mkm.var_conveyor', $filter)
					->or_like('mkm.var_dusun', $filter)
					->or_like('mkm.var_rt_rw', $filter)
					->or_like('mkm.var_desa', $filter)
					->or_like('mkm.var_kecamatan', $filter)
					->or_like('mkm.var_kab_kota', $filter)
					->or_like('mkm.var_alamat_ktp', $filter)
					->or_like('mkm.var_lokasi_lahir', $filter)
					->or_like('mkm.dt_tanggal_lahir', $filter)
					->or_like('mkm.var_telepon', $filter)
					->or_like('mkm.var_agama', $filter)
					->or_like('mkm.var_no_ktp', $filter)
					->or_like('mkm.var_no_kk', $filter)
					->or_like('mkm.var_email', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function create($ins){
        $this->db->trans_begin();
		$this->db->insert($this->m_karyawan_magang, $ins);
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get($int_karyawan_magang_id){
		return $this->db->select("*")
					->get_where($this->m_karyawan_magang, ['int_karyawan_magang_id' => $int_karyawan_magang_id])->row();
	}

	public function update($int_karyawan_magang_id, $upd){
		$this->db->trans_begin();

		$this->db->where('int_karyawan_magang_id', $int_karyawan_magang_id);
		$this->db->update($this->m_karyawan_magang, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_karyawan_magang_id){
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_karyawan_magang_id', $int_karyawan_magang_id);
		$this->db->update($this->m_karyawan_magang, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete_custom($var_custom = "", $var_range_start = "", $var_range_end = ""){
		if($var_custom != "" && $var_range_start != "" && $var_range_end != ""){
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->m_karyawan_magang} WHERE var_nim IN ({$var_custom})");
			$this->db->query("DELETE FROM {$this->m_karyawan_magang} WHERE var_nim BETWEEN '{$var_range_start}' AND '{$var_range_end}' ");
			if ($this->db->trans_status() == FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}
		}else if($var_custom == "" && $var_range_start != "" && $var_range_end != ""){
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->m_karyawan_magang} WHERE var_nim BETWEEN '{$var_range_start}' AND '{$var_range_end}' ");
			if ($this->db->trans_status() == FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}	
		}else if(
				($var_custom != "" && $var_range_start == "" && $var_range_end == "") || 
				($var_custom != "" && ($var_range_start == "" || $var_range_end == ""))){
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->m_karyawan_magang} WHERE var_nim IN ({$var_custom})");
			if ($this->db->trans_status() == FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}	
		}else{
			return false;
		}
	}
	
	public function import($in, $file){
		$user 	= $this->session->userdata('username');
		$var_nim = 'B';
		$var_nik = 'C';
		$var_batch = 'D';
		$var_nama = 'E';
		$var_jabatan  = 'F';
		$var_pekerjaan = 'G';
		$dt_tanggal_masuk = 'H';
		$dt_tanggal_awal_k1 = 'I';
		$dt_tanggal_akhir_k1 = 'J';
		$dt_tanggal_awal_k2 = 'K';
		$dt_tanggal_akhir_k2 = 'L';
		$dt_tanggal_tetap = 'M';
		$var_status_kerja = 'N';
		$var_status_mp = 'O';
		$dt_tanggal_out = 'P';
		$var_sumber_data_laporan_mp = 'Q';
		$var_alasan_resign = 'R';
		$var_status_foh = 'S';
		$var_departement = 'T';
		$var_section = 'U';
		$var_job = 'V';
		$var_shift = 'W';
		$var_area = 'X';
		$var_line = 'Y';
		$var_conveyor = 'Z';
		$var_dusun = 'AA';
		$var_rt_rw = 'AB';
		$var_desa = 'AC';
		$var_kecamatan = 'AD';
		$var_kab_kota = 'AE';
		$var_alamat_ktp = 'AF';
		$var_lokasi_lahir = 'AG';
		$dt_tanggal_lahir = 'AH';
		$var_telepon = 'AI';
		$var_jenis_kelamin = 'AJ';
		$var_status_perkawinan = 'AK';
		$var_agama = 'AL';
		$var_sekolah = 'AM';
		$var_jurusan = 'AN';
		$var_nama_ibu = 'AO';
		$var_nama_ayah = 'AP';
		$var_ket_mp_hamil = 'AQ';
		$var_status_ck_clt = 'AR';
		$var_keterangan_ck_clt = 'AS';
		$dt_tanggal_mulai_ck_clt = 'AT';
		$dt_perkiraan_selesai_ck_clt = 'AU';
		$var_sumber_data_ck_clt = 'AV';
		$var_ket_masakerja_eks_sai = 'AW';
		$var_job_eks= 'AX';
		$var_ket_difabel = 'AY';
		$var_no_ktp = 'AZ';
		$var_no_kk = 'BA';
		$var_alamat_tinggal = 'BB';
		$var_npwp = 'BC';
		$var_nama_suami_istri = 'BD';
		$var_nama_anak_1 = 'BE';
		$var_nama_anak_2 = 'BF';
		$var_nama_anak_3 = 'BG';
		$var_email = 'BH';
		$var_jumlah_anak = 'BI';
		$var_pic_spv = 'BJ';
		$var_jumlah_sp = 'BK';
		$var_jenis_sp = 'BL';
		$dt_tanggal_sp = 'BM';
		$var_status_dl_idl = 'BN';

		$filterSubset = new MyReadFilter($in['mulai'],
						[$var_nim,
						$var_nik,
						$var_batch,
						$var_nama,
						$var_jabatan,
						$var_pekerjaan,
						$dt_tanggal_masuk,
						$dt_tanggal_awal_k1,
						$dt_tanggal_akhir_k1,
						$dt_tanggal_awal_k2,
						$dt_tanggal_akhir_k2,
						$dt_tanggal_tetap,
						$var_status_kerja,
						$var_status_mp,
						$dt_tanggal_out,
						$var_sumber_data_laporan_mp,
						$var_alasan_resign,
						$var_status_foh,
						$var_departement,
						$var_section,
						$var_job,
						$var_shift,
						$var_area,
						$var_line,
						$var_conveyor,
						$var_dusun,
						$var_rt_rw,
						$var_desa,
						$var_kecamatan,
						$var_kab_kota,
						$var_alamat_ktp,
						$var_lokasi_lahir,
						$dt_tanggal_lahir,
						$var_telepon,
						$var_jenis_kelamin,
						$var_status_perkawinan,
						$var_agama,
						$var_sekolah,
						$var_jurusan,
						$var_nama_ibu,
						$var_nama_ayah,
						$var_ket_mp_hamil,
						$var_status_ck_clt,
						$var_keterangan_ck_clt,
						$dt_tanggal_mulai_ck_clt,
						$dt_perkiraan_selesai_ck_clt,
						$var_sumber_data_ck_clt,
						$var_ket_masakerja_eks_sai,
						$var_job_eks,
						$var_ket_difabel,
						$var_no_ktp,
						$var_no_kk,
						$var_alamat_tinggal,
						$var_npwp,
						$var_nama_suami_istri,
						$var_nama_anak_1,
						$var_nama_anak_2,
						$var_nama_anak_3,
						$var_email,
						$var_jumlah_anak,
						$var_pic_spv,
						$var_jumlah_sp,
						$var_jenis_sp,
						$dt_tanggal_sp,
						$var_status_dl_idl]
					);
		$reader = IOFactory::createReader(ucfirst(ltrim($file['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, false, false, true);
		
		//INSERT IGNORE INTO-> 0 row affected if duplicate
		//REPLACE INTO -> replace old duplicate entry
		$ins_karyawan_magang = "REPLACE INTO {$this->m_karyawan_magang} 
						(`var_nim`, `var_nik`, `var_batch`, `var_nama`, `var_jabatan`, `var_pekerjaan`, `dt_tanggal_masuk`, `dt_tanggal_awal_k1`, `dt_tanggal_akhir_k1`, `dt_tanggal_awal_k2`, 
						`dt_tanggal_akhir_k2`, `dt_tanggal_tetap`, `var_status_kerja`, `var_status_mp`, `dt_tanggal_out`, `var_sumber_data_laporan_mp`, `var_alasan_resign`,
						`var_status_foh`, `var_departement`, `var_section`, `var_job`, 
						`var_shift`, `var_area`, `var_line`, `var_conveyor`, `var_dusun`, `var_rt_rw`, `var_desa`, `var_kecamatan`, `var_kab_kota`, `var_alamat_ktp`, 
						`var_lokasi_lahir`, `dt_tanggal_lahir`, `var_telepon`, `var_jenis_kelamin`, `var_status_perkawinan`, `var_agama`, `var_sekolah`, `var_jurusan`, `var_nama_ibu`, `var_nama_ayah`,
						`var_ket_mp_hamil`, `var_status_ck_clt`, `var_keterangan_ck_clt`, `dt_tanggal_mulai_ck_clt`, `dt_perkiraan_selesai_ck_clt`, `var_sumber_data_ck_clt`, `var_ket_masakerja_eks_sai`, `var_job_eks`, `var_ket_difabel`, `var_no_ktp`, `var_no_kk`, 
						`var_alamat_tinggal`, `var_npwp`, `var_nama_suami_istri`, `var_nama_anak_1`, `var_nama_anak_2`, `var_nama_anak_3`, `var_email`,
						`var_jumlah_anak`, `var_pic_spv`, `var_jumlah_sp`, `var_jenis_sp`, `dt_tanggal_sp`, `var_status_dl_idl`) VALUES ";
		$total = 0;
		foreach($data as $i => $d){
			if($i > ($in['mulai'] - 1)){
				if($d[$var_status_kerja] == 'MAGANG 1' || $d[$var_status_kerja] == 'MAGANG 2' || $d[$var_status_kerja] == 'MAGANG' || $d[$var_status_kerja] == 'IN CLASS' || $d[$var_status_kerja] == 'MAGANG 1(IN CLASS)'){

					$format_tanggal_masuk = $this->format_tanggal_excel($d[$dt_tanggal_masuk]);
					$format_tanggal_awal_k1 = $this->format_tanggal_excel($d[$dt_tanggal_awal_k1]);
					$format_tanggal_akhir_k1 = $this->format_tanggal_excel($d[$dt_tanggal_akhir_k1]);
					$format_tanggal_awal_k2 = $this->format_tanggal_excel($d[$dt_tanggal_awal_k2]);
					$format_tanggal_akhir_k2 = $this->format_tanggal_excel($d[$dt_tanggal_akhir_k2]);
					$format_tanggal_tetap = $this->format_tanggal_excel($d[$dt_tanggal_tetap]);
					$format_tanggal_out = $this->format_tanggal_excel($d[$dt_tanggal_out]);
					$format_tanggal_lahir = $this->format_tanggal_excel($d[$dt_tanggal_lahir]);
					$format_tanggal_mulai_ck_clt = $this->format_tanggal_excel($d[$dt_tanggal_mulai_ck_clt]);
					$format_perkiraan_selesai_ck_clt = $this->format_tanggal_excel($d[$dt_perkiraan_selesai_ck_clt]);
					$format_tanggal_sp = $this->format_tanggal_excel($d[$dt_tanggal_sp]);
					
					$d_var_nim = $this->db->escape(trim($d[$var_nim]));
					$d_var_nik = $this->db->escape(trim($d[$var_nik]));
					$d_var_batch = $this->db->escape(trim($d[$var_batch]));
					$d_var_nama = $this->db->escape(trim($d[$var_nama]));
					$d_var_jabatan = $this->db->escape(trim($d[$var_jabatan]));
					$d_var_pekerjaan = $this->db->escape(trim($d[$var_pekerjaan]));
					$d_dt_tanggal_masuk = $this->db->escape($format_tanggal_masuk);						//tanggal
					$d_dt_tanggal_awal_k1 = $this->db->escape(trim($format_tanggal_awal_k1));			//tanggal
					$d_dt_tanggal_akhir_k1 = $this->db->escape(trim($format_tanggal_akhir_k1));			//tanggal
					$d_dt_tanggal_awal_k2 = $this->db->escape(trim($format_tanggal_awal_k2));			//tanggal
					$d_dt_tanggal_akhir_k2 = $this->db->escape(trim($format_tanggal_akhir_k2));			//tanggal
					$d_dt_tanggal_tetap = $this->db->escape(trim($format_tanggal_tetap));				//tanggal
					$d_var_status_kerja = $this->db->escape(trim($d[$var_status_kerja]));
					$d_var_status_mp = $this->db->escape(trim($d[$var_status_mp]));
					$d_dt_tanggal_out = $this->db->escape(trim($format_tanggal_out));					//tanggal
					$d_var_sumber_data_laporan_mp = $this->db->escape(trim($d[$var_sumber_data_laporan_mp]));
					$d_var_alasan_resign = $this->db->escape(trim($d[$var_alasan_resign]));
					$d_var_status_foh = $this->db->escape(trim($d[$var_status_foh]));
					$d_var_departement = $this->db->escape(trim($d[$var_departement]));
					$d_var_section = $this->db->escape(trim($d[$var_section]));
					$d_var_job = $this->db->escape(trim($d[$var_job]));
					$d_var_shift = $this->db->escape(trim($d[$var_shift]));
					$d_var_area = $this->db->escape(trim($d[$var_area]));
					$d_var_line = $this->db->escape(trim($d[$var_line]));
					$d_var_conveyor = $this->db->escape(trim($d[$var_conveyor]));
					$d_var_dusun = $this->db->escape(trim($d[$var_dusun]));
					$d_var_rt_rw = $this->db->escape(trim($d[$var_rt_rw]));
					$d_var_desa = $this->db->escape(trim($d[$var_desa]));
					$d_var_kecamatan = $this->db->escape(trim($d[$var_kecamatan]));
					$d_var_kab_kota = $this->db->escape(trim($d[$var_kab_kota]));
					$d_var_alamat_ktp = $this->db->escape(trim($d[$var_alamat_ktp]));
					$d_var_lokasi_lahir = $this->db->escape(trim($d[$var_lokasi_lahir]));
					$d_dt_tanggal_lahir = $this->db->escape(trim($format_tanggal_lahir));						//tanggal
					$d_var_telepon = $this->db->escape(trim($d[$var_telepon]));
					$d_var_jenis_kelamin = $this->db->escape(trim($d[$var_jenis_kelamin]));
					$d_var_status_perkawinan = $this->db->escape(trim($d[$var_status_perkawinan]));
					$d_var_agama = $this->db->escape(trim($d[$var_agama]));
					$d_var_sekolah = $this->db->escape(trim($d[$var_sekolah]));
					$d_var_jurusan = $this->db->escape(trim($d[$var_jurusan]));
					$d_var_nama_ibu = $this->db->escape(trim($d[$var_nama_ibu]));
					$d_var_nama_ayah = $this->db->escape(trim($d[$var_nama_ayah]));
					$d_var_ket_mp_hamil = $this->db->escape(trim($d[$var_ket_mp_hamil]));
					$d_var_status_ck_clt = $this->db->escape(trim($d[$var_status_ck_clt]));
					$d_var_keterangan_ck_clt = $this->db->escape(trim($d[$var_keterangan_ck_clt]));
					$d_dt_tanggal_mulai_ck_clt = $this->db->escape(trim($format_tanggal_mulai_ck_clt));			//tanggal
					$d_dt_perkiraan_selesai_ck_clt = $this->db->escape(trim($format_perkiraan_selesai_ck_clt));	//tanggal
					$d_var_sumber_data_ck_clt = $this->db->escape(trim($d[$var_sumber_data_ck_clt]));
					$d_var_ket_masakerja_eks_sai = $this->db->escape(trim($d[$var_ket_masakerja_eks_sai]));
					$d_var_job_eks = $this->db->escape(trim($d[$var_job_eks]));
					$d_var_ket_difabel = $this->db->escape(trim($d[$var_ket_difabel]));
					$d_var_no_ktp = $this->db->escape(trim($d[$var_no_ktp]));
					$d_var_no_kk = $this->db->escape(trim($d[$var_no_kk]));
					$d_var_alamat_tinggal = $this->db->escape(trim($d[$var_alamat_tinggal]));
					$d_var_npwp = $this->db->escape(trim($d[$var_npwp]));
					$d_var_nama_suami_istri = $this->db->escape(trim($d[$var_nama_suami_istri]));
					$d_var_nama_anak_1 = $this->db->escape(trim($d[$var_nama_anak_1]));
					$d_var_nama_anak_2 = $this->db->escape(trim($d[$var_nama_anak_2]));
					$d_var_nama_anak_3 = $this->db->escape(trim($d[$var_nama_anak_3]));
					$d_var_email = $this->db->escape(trim($d[$var_email]));
					$d_var_jumlah_anak = $this->db->escape(trim($d[$var_jumlah_anak]));
					$d_var_pic_spv = $this->db->escape(trim($d[$var_pic_spv]));
					$d_var_jumlah_sp = $this->db->escape(trim($d[$var_jumlah_sp]));
					$d_var_jenis_sp = $this->db->escape(trim($d[$var_jenis_sp]));
					$d_dt_tanggal_sp = $this->db->escape(trim($format_tanggal_sp));				//tanggal
					if(isset($d[$var_status_dl_idl])){
						$d_var_status_dl_idl = $this->db->escape(trim($d[$var_status_dl_idl]));
					}else{
						$d_var_status_dl_idl = "''";
					}

					$ins_karyawan_magang .= "({$d_var_nim}, {$d_var_nik}, {$d_var_batch}, {$d_var_nama}, {$d_var_jabatan}, {$d_var_pekerjaan}, {$d_dt_tanggal_masuk}, {$d_dt_tanggal_awal_k1}, {$d_dt_tanggal_akhir_k1}, {$d_dt_tanggal_awal_k2}, 
					{$d_dt_tanggal_akhir_k2}, {$d_dt_tanggal_tetap}, {$d_var_status_kerja}, {$d_var_status_mp}, {$d_dt_tanggal_out}, {$d_var_sumber_data_laporan_mp}, {$d_var_alasan_resign},
					{$d_var_status_foh}, {$d_var_departement}, {$d_var_section}, {$d_var_job}, 
					{$d_var_shift}, {$d_var_area}, {$d_var_line}, {$d_var_conveyor}, {$d_var_dusun}, {$d_var_rt_rw}, {$d_var_desa}, {$d_var_kecamatan}, {$d_var_kab_kota}, {$d_var_alamat_ktp}, 
					{$d_var_lokasi_lahir}, {$d_dt_tanggal_lahir}, {$d_var_telepon}, {$d_var_jenis_kelamin}, {$d_var_status_perkawinan}, {$d_var_agama}, {$d_var_sekolah}, {$d_var_jurusan}, {$d_var_nama_ibu}, {$d_var_nama_ayah},
					{$d_var_ket_mp_hamil}, {$d_var_status_ck_clt}, {$d_var_keterangan_ck_clt}, {$d_dt_tanggal_mulai_ck_clt}, {$d_dt_perkiraan_selesai_ck_clt}, {$d_var_sumber_data_ck_clt}, {$d_var_ket_masakerja_eks_sai}, {$d_var_job_eks}, {$d_var_ket_difabel}, {$d_var_no_ktp}, {$d_var_no_kk}, 
					{$d_var_alamat_tinggal}, {$d_var_npwp}, {$d_var_nama_suami_istri}, {$d_var_nama_anak_1}, {$d_var_nama_anak_2}, {$d_var_nama_anak_3}, {$d_var_email},
					{$d_var_jumlah_anak}, {$d_var_pic_spv}, {$d_var_jumlah_sp}, {$d_var_jenis_sp}, {$d_dt_tanggal_sp}, {$d_var_status_dl_idl}),";

					$total++;
				}
			}
		}

		if($total < 1){
			return 0;
		}

		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['var_username' => $user,
										  'var_file_name' => $file['orig_name'],
										  'txt_direktori' => $file['full_path'],
										  'dt_import_date' => date("Y-m-d H:i:s"),
										  'int_total' => $total]);
		
		$ins_karyawan_magang = rtrim($ins_karyawan_magang, ',').';';
		$this->db->query($ins_karyawan_magang);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $total;
		}
	}

	function format_tanggal_excel($str_date){
		if(is_numeric($str_date)){
			$int_date = $str_date - 2;
			$add_date = date('Y-m-d', strtotime('1900-01-01'. ' + '.$int_date.' days'));
			$strtotime = strtotime($add_date);
			return date('Y-m-d',$strtotime);
		}else{
			return '';
		}
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
