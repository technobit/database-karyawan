<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
	
class Presensi extends MX_Controller {
	private $input_file_name = 'data_absensi';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'PRESENSI'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'kehadiran';
		$this->routeURL = 'presensi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('presensi_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Presensi Karyawan';
		$this->page->menu 	  = 'presensi';
		//$this->page->submenu1 = 'presensi';
		$this->breadcrumb->title = 'Data Presensi Karyawan';
		$this->breadcrumb->card_title = 'Data Karyawan presensi';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Karyawan', 'presensi'];
		$this->js = true;
		$data['pokjakd_list']	= $this->model->pokjakd_list();
		$data['orgno_list']		= $this->model->orgno_list();
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}");
		$this->render_view('presensi/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('pokjakd_filter', true), $this->input->post('orgno_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('pokjakd_filter', true), $this->input->post('orgno_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_karno, '['.$d->var_kode_orgno.'] '.$d->var_orgno, $d->var_nama, $d->var_aktreknm, $d->var_aktrektp,  $d->var_kode_pokjakd, idn_date($d->dt_tanggal_trans), $d->time_masuk_akt, $d->time_pulang_akt);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}
	
	public function import(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save-import");
		$data['title']      = 'Import Data';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('presensi/index_import', $data, true);
		
	}

	public function delete(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/delete-data");
		$data['title']      = 'Hapus Data Presensi';
		$this->load_view('presensi/index_delete', $data, true);	
	}

	public function save_import(){
		$this->authCheckDetailAccess('c');
		
		$start = start_time();
		$this->form_validation->set_rules('mulai', 'Mulai', 'required|integer');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			if(isset($_FILES[$this->input_file_name])){
				$config['upload_path']   = "./{$this->import_dir}"; 
				$config['allowed_types'] = 'xlsx|xls'; 
				$config['encrypt_name']  = true; 
				$config['max_size']      = 4096;  
				$this->load->library('upload', $config);
				
				if($this->upload->do_upload($this->input_file_name)){
					$status  = $this->model->import($this->input->post(), $this->upload->data());
					$this->set_json([  'stat' => ($status !== false), 
								'mc' => false,//($status !== false), //modal close
								'time' => finish_time($start),
								'msg' => ($status !== false)? "Data berhasil di-import dengan {$status} baris data." : 'Data gagal di-import',
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
				} else {
					$this->set_json([  'stat' => false, 
										'mc' => false, //modal close
										'msg' => "Data gagal di-import. ".$this->upload->display_errors('',''),
										'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
									]);
				}
			} else {
				$this->set_json([  'stat' => false, 
									'mc' => false, //modal close
									'msg' => "Data gagal di-import",
									'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
								]);
			}

        }
	}

	public function delete_data(){
		$this->authCheckDetailAccess('d');

		if(strpos($this->input->post('date_filter_delete', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter_delete', true));
        }

		$filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$check = $this->model->delete($filter_start, $filter_end);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Presensi Berhasil Dihapus" : "Data Presensi Gagal Dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$ldata = $this->model->list($this->input->post('pokjakd_filter', true), $this->input->post('orgno_filter', true), $filter_start, $filter_end, $this->input_post('search_value', TRUE), $this->input_post('order_column', true), $this->input_post('order_dir'), $this->input_post('length', true), $this->input_post('start', true));

        $title    = 'Data Presensi Karyawan';

        $filename = 'Data Presensi Karyawan - '.idn_date($filter_start, 'j F Y H.i.s').' - '.idn_date($filter_end, 'j F Y H.i.s').'.xlsx';

		$input_file = 'assets/export/xlsx/data_presensi.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0);

		$sheet = $spreadsheet->getActiveSheet();

        $x = 1;
        foreach($ldata as $d){
			$x++;

            $sheet->setCellValue('A'.$x, $d->var_karno);
            $sheet->setCellValue('B'.$x, $d->var_kode_orgno);
            $sheet->setCellValue('C'.$x, $d->var_nama);
            $sheet->setCellValue('D'.$x, $d->var_aktreknm);
            $sheet->setCellValue('E'.$x, $d->var_aktrektp);
			$dt_tanggal_trans = date_create($d->dt_tanggal_trans);
			$dt_tanggal_trans = date_format($dt_tanggal_trans,"d-m-y");
            $sheet->setCellValue('F'.$x, $dt_tanggal_trans);
			$sheet->setCellValue('G'.$x, $d->var_kode_pokjakd);
			$sheet->setCellValue('H'.$x, $d->time_masuk_akt);
			$sheet->setCellValue('I'.$x, $d->time_pulang_akt);

			/*if($d->dt_tanggal_masuk == '1970-01-01'|| $d->dt_tanggal_masuk == '0000-00-00'){
				$sheet->setCellValue('H'.$x, '');
			}else{
				$sheet->setCellValue('H'.$x, date_diff(date_create($d->dt_tanggal_masuk),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('H'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			if($d->dt_tanggal_awal_k1 == '1970-01-01' || $d->dt_tanggal_awal_k1 == '0000-00-00'){
				$sheet->setCellValue('I'.$x, '');
			}else{
				$sheet->setCellValue('I'.$x, date_diff(date_create($d->dt_tanggal_awal_k1),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('I'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			$sheet->setCellValueExplicit('AI'.$x, $d->var_telepon,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);*/

        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}}
