<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['presensi']['get']                   = 'kehadiran/presensi';
$route['presensi']['post']                  = 'kehadiran/presensi/list';
$route['presensi/import']['get']            = 'kehadiran/presensi/import';
$route['presensi/del']['get']               = 'kehadiran/presensi/delete';
$route['presensi/save-import']['post']       = 'kehadiran/presensi/save_import';
$route['presensi/delete-data']['post']       = 'kehadiran/presensi/delete_data';

$route['presensi/save']['post']              = 'kehadiran/presensi/save';
$route['presensi/([a-zA-Z0-9]+)/del']['post']= 'kehadiran/presensi/delete/$1';

$route['export/presensi']['post']            = 'kehadiran/presensi/export';
