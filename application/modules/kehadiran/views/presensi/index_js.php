<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    var get_search_value = '';
    function exportData(th){

        var table = $('#table_data').DataTable();
        var info = table.page.info();   
        var data_per_page = info.length;
        var start_record = info.page * data_per_page;
        var get_order = table.order();
        var get_order_column = get_order[0][0];
        var get_order_dir = get_order[0][1];


        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                date_filter : $('.date_filter').val(),
                pokjakd_filter : $('.pokjakd_filter').val(),
                orgno_filter : $('.orgno_filter').val(),
                start : start_record,
                length : data_per_page,
                order_column : get_order_column,
                order_dir : get_order_dir,
                search_value : get_search_value
            }
        });

        setTimeout(function(){unblockUI(blc)}, 10000);
    }

    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 100,
            "lengthMenu": [[100, 250, 500, 1000, -1], [100, 250, 500, 1000, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.pokjakd_filter = $('.pokjakd_filter').val();
					d.orgno_filter = $('.orgno_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "class": "text-right",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "class": "text-right",
                },
                {
                    "sWidth": "auto",
                    "class": "text-right",
               },
                {
                    "sWidth": "auto"
                },

                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
            ]
        });

        $('.pokjakd_filter').change(function(){
            dataTable.draw();
        });

        $('.orgno_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
                get_search_value = $(this).val();
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>