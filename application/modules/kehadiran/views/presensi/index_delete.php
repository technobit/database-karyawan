<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="import-form" width="80%">
<div id="modal-import" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="date_filter_delete" class="col-md-4 col-form-label">Tanggal</label>
				<div class="col-md-8">
					<input type="text" name="date_filter_delete" class="form-control form-control-sm date_filter_delete">
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" class="btn btn-danger">Hapus</button>
			<button type="button" data-dismiss="modal" class="btn btn-success">Keluar</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	var daterangedeletepresensi = {
		startDate: moment(),//moment().startOf('month'),
		endDate: moment(),
		locale:{format: 'DD-MM-YYYY', separator: ' ~ '},
		ranges: {
		'Hari Ini': [moment(), moment()],
		'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		'Tgl. 1 - Sekarang': [moment().startOf('month'), moment()],
		'1 Minggu lalu': [moment().subtract(6, 'days'), moment()],
		'30 Hari lalu': [moment().subtract(29, 'days'), moment()],
		'Bulan ini': [moment().startOf('month'), moment().endOf('month')]
		}
	};
	$(document).ready(function(){
		$('.date_filter_delete').daterangepicker(daterangedeletepresensi);
		$("#import-form").validate({
			rules: {
				date_filter_delete: {
					required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI('#modal-import', 'progress', 20);
                //let blc = '#modal-import';
                //blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						setFormMessage('.form-message', data);
						if(data.stat){
							dataTable.draw();
							resetForm(form)
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>