<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-sm btn-warning" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-download"></i> Export Excel</button>
                        <button type="button" data-block="body" class="btn btn-sm btn-success ajax_modal" data-url="<?=$url?>/import"  style="margin-right:20px;"><i class="fas fa-file-excel"></i> Import Excel</button>
                        <button type="button" data-block="body" class="btn btn-sm btn-danger ajax_modal" data-url="<?=$url?>/del" ><i class="fas fa-trash"></i> Delete Data</button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-3">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="date_filter" class="col-md-4 col-form-label">Tanggal</label>
									<div class="col-md-8">
										<input type="text" name="date_filter" class="form-control form-control-sm date_filter">
									</div>
								</div>
							</div>
                            <div class="col-md-5">
								<div class="form-group row text-sm mb-0">
									<label for="pokjakd_filter" class="col-md-4 col-form-label">POKJAKD</label>
									<div class="col-md-8">
                                        <select id="var_kode_pokjakd" name="var_kode_pokjakd" class="form-control form-control-sm pokjakd_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA POKJAKD -</option>
                                            <?php 
                                                foreach($pokjakd_list as $pl){
                                                    echo '<option value="'.$pl->var_kode_pokjakd.'">'.$pl->var_kode_pokjakd.' - '.$pl->var_pokjakd.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="orgno_filter" class="col-md-4 col-form-label">ORGNO</label>
									<div class="col-md-8">
                                        <select id="var_kode_orgno" name="var_kode_orgno" class="form-control form-control-sm orgno_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA ORGNO -</option>
                                            <?php 
                                                foreach($orgno_list as $ol){
                                                    echo '<option value="'.$ol->var_kode_orgno.'">['.$ol->var_kode_orgno.'] '.$ol->var_orgno.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
						</div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>ORGNO</th>
                            <th>NAMA</th>
                            <th>ANTREKNM</th>
                            <th>ANTREKTP</th>
                            <th>POKJAKD</th>
                            <th>TANGGAL</th>
                            <th>MASUK</th>
                            <th>PULANG</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
