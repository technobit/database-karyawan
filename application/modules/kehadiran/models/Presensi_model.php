<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Presensi_model extends MY_Model {

	public function pokjakd_list(){
		$this->db->select("*")
			->from($this->m_pokjakd);
		return $this->db->order_by('int_pokjakd_id', 'ASC')->get()->result();
	}

	public function orgno_list(){
		$this->db->select("*")
			->from($this->m_orgno);
		return $this->db->order_by('int_kode_orgno_id', 'ASC')->get()->result();
	}

	public function list($pokjakd_filter = "", $orgno_filter = 0, $filter_start = null, $filter_end = null, $filter = NULL, $order_by = 0, $sort = 'DESC', $limit = 0, $ofset = 0){
		$expired_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'));

		$this->db->select("ta.*,mo.var_orgno")
				->from($this->t_absensi." ta")
				->join($this->m_orgno." mo", "mo.var_kode_orgno = ta.var_kode_orgno", "left");

		if(!empty($pokjakd_filter)){ // filter
			$this->db->where('ta.var_kode_pokjakd', ($pokjakd_filter));
		}

		if(!empty($orgno_filter)){ // filter
			$this->db->where('ta.var_kode_orgno', ($orgno_filter));
		}

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('ta.dt_tanggal_trans', $filter_start, $filter_end);
		}
		
		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('ta.var_nama', $filter)
					->or_like('ta.var_karno', $filter)
					->or_like('mo.var_orgno', $filter)
					->or_like('ta.var_aktreknm', $filter)
					->or_like('ta.var_aktrektp', $filter)
					->or_like('ta.var_kode_pokjakd', $filter)
					->group_end();
		}

		$order = 'dt_tanggal_trans';
		switch($order_by){
			case 1 : $order = 'ta.var_karno'; break;
			case 2 : $order = 'ta.var_kode_orgno'; break;
			case 3 : $order = 'ta.var_nama'; break;
			case 4 : $order = 'ta.var_aktreknm'; break;
			case 5 : $order = 'ta.var_aktrektp'; break;
			case 6 : $order = 'ta.var_kode_pokjakd'; break;
			case 7 : $order = 'ta.dt_tanggal_trans'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($pokjakd_filter = "", $orgno_filter = 0, $filter_start = null, $filter_end = null, $filter = NULL){
		$expired_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'));

		$this->db->select("ta.*,mo.var_orgno")
				->from($this->t_absensi." ta")
				->join($this->m_orgno." mo", "mo.var_kode_orgno = ta.var_kode_orgno", "left");

		if(!empty($pokjakd_filter)){ // filter
			$this->db->where('ta.var_kode_pokjakd', ($pokjakd_filter));
		}

		if(!empty($orgno_filter)){ // filter
			$this->db->where('ta.var_kode_orgno', ($orgno_filter));
		}

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('ta.dt_tanggal_trans', $filter_start, $filter_end);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('ta.var_nama', $filter)
					->or_like('ta.var_karno', $filter)
					->or_like('mo.var_orgno', $filter)
					->or_like('ta.var_aktreknm', $filter)
					->or_like('ta.var_aktrektp', $filter)
					->or_like('ta.var_kode_pokjakd', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function import($in, $file){
		$user 	= $this->session->userdata('username');
		$var_karno = 'A';
		$var_kode_orgno = 'B';
		$var_nama = 'C';
		$var_aktreknm = 'D';
		$var_aktrektp  = 'E';
		$dt_tanggal_trans = 'F';
		$var_kode_pokjakd = 'G';
		$time_masuk_akt = 'H';
		$time_pulang_akt = 'I';

		$filterSubset = new MyReadFilter($in['mulai'],
						[$var_karno,
						$var_kode_orgno,
						$var_nama,
						$var_aktreknm,
						$var_aktrektp,
						$dt_tanggal_trans,
						$var_kode_pokjakd,
						$time_masuk_akt,
						$time_pulang_akt]
					);
		$reader = IOFactory::createReader(ucfirst(ltrim($file['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, false, false, true);
		
		//INSERT IGNORE INTO-> 0 row affected if duplicate
		//REPLACE INTO -> replace old duplicate entry
		$ins_presensi = "REPLACE INTO {$this->t_absensi} 
						(`var_karno`, `var_kode_orgno`, `var_nama`, `var_aktreknm`, `var_aktrektp`, `dt_tanggal_trans`, `var_kode_pokjakd`, `time_masuk_akt`, `time_pulang_akt`) VALUES ";
		$total = 0;
		foreach($data as $i => $d){
			if($i > ($in['mulai'] - 1)){
				$total++;


				$format_tanggal_trans = $this->format_tanggal_excel($d[$dt_tanggal_trans]);
				$format_jam_masuk = $this->format_jam_excel($d[$time_masuk_akt]);
				$format_jam_pulang = $this->format_jam_excel($d[$time_pulang_akt]);
				
				$d_var_karno = $this->db->escape(trim($d[$var_karno]));
				$d_var_kode_orgno = $this->db->escape(trim($d[$var_kode_orgno]));
				$d_var_nama = $this->db->escape(trim($d[$var_nama]));
				$d_var_aktreknm = $this->db->escape(trim($d[$var_aktreknm]));
				$d_var_aktrektp = $this->db->escape(trim($d[$var_aktrektp]));
				$d_dt_tanggal_trans = $this->db->escape($format_tanggal_trans);
				$d_var_kode_pokjakd = $this->db->escape(trim($d[$var_kode_pokjakd]));
				$d_time_masuk_akt = $this->db->escape($format_jam_masuk);
				$d_time_pulang_akt = $this->db->escape($format_jam_pulang);

				$ins_presensi .= "({$d_var_karno},  {$d_var_kode_orgno},  {$d_var_nama},  {$d_var_aktreknm},  {$d_var_aktrektp},  {$d_dt_tanggal_trans},  {$d_var_kode_pokjakd},  {$d_time_masuk_akt},  {$d_time_pulang_akt}),";
			}
		}
		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['var_username' => $user,
										  'var_file_name' => $file['orig_name'],
										  'txt_direktori' => $file['full_path'],
										  'dt_import_date' => date("Y-m-d H:i:s"),
										  'int_total' => $total]);
		
		$ins_presensi = rtrim($ins_presensi, ',').';';
		$this->db->query($ins_presensi);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $total;
		}
	}

	public function delete($filter_start, $filter_end){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->t_absensi} WHERE dt_tanggal_trans BETWEEN '{$filter_start}' AND '{$filter_end}'");
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	function format_tanggal_excel($str_date){
		$int_date = $str_date - 2;
		$add_date = date('Y-m-d', strtotime('1900-01-01'. ' + '.$int_date.' days'));
		$strtotime = strtotime($add_date);
		return date('Y-m-d',$strtotime);
	}

	function format_jam_excel($str_time){
		$add_sec = 86400 * floatval($str_time);
		$datetime = date("1900-01-01 00:00:00");
		// Convert datetime to Unix timestamp
		$timestamp = strtotime($datetime);
		// Subtract time from datetime
		$time = $timestamp + $add_sec;
		// Date and time after subtraction
		return date("H:i:s", $time);
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
