<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Kontrak_model extends MY_Model {

	public function status_list(){
		$this->db->select("var_status_kerja")
			->from($this->m_karyawan_kontrak)
			//->where("var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'TETAP', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3', 'TETAP (EXPATRIAT)')")
			->where("var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3')")
			->group_by("var_status_kerja");
			return $this->db->order_by('var_status_kerja', 'ASC')->get()->result();
	}
		
	public function departement_list(){
		$this->db->select("var_departement")
			->from($this->m_karyawan_kontrak)
			->group_by("var_departement");
		return $this->db->order_by('var_departement', 'ASC')->get()->result();
	}
	
	public function section_list(){
		$this->db->select("var_section")
			->from($this->m_karyawan_kontrak)
			->group_by("var_section");
		return $this->db->order_by('var_section', 'ASC')->get()->result();
	}
	
	public function list($status_filter, $departement_filter, $section_filter, $filter_start_kontrak = "", $filter_end_kontrak = "", $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$expired_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'));

		$this->db->select("*")
				->from($this->m_karyawan_kontrak." mkk")
				//->where("var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'TETAP', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3', 'TETAP (EXPATRIAT)')")
				->where("var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3')")
				->where("var_status_mp = 'ACTIVE'")
				->where("int_status = 1");

		if($status_filter != ""){ // filter
			$this->db->where('mkk.var_status_kerja', $status_filter);
		}
		
		if($departement_filter != ""){ // filter
			$this->db->where('mkk.var_departement', $departement_filter);
		}

		if($section_filter != ""){ // filter
			$this->db->where('mkk.var_section', $section_filter);
		}

		if($filter_start_kontrak != "" && $filter_end_kontrak != ""){
			$this->db->where('((mkk.dt_tanggal_akhir_k1 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'") OR (mkk.dt_tanggal_akhir_k2 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'"))');
			//$this->whereBetweenDate('dt_periode_kontrak1_awal', $filter_start_kontrak, $filter_end_kontrak);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mkk.var_nim', $filter)
					->or_like('mkk.var_nik', $filter)
					->or_like('mkk.var_nama', $filter)
					->or_like('mkk.var_no_ktp', $filter)
					->group_end();
		}

		$order = 'var_nik ';
		switch($order_by){
			case 1 : $order = 'var_nik '; break;
			case 2 : $order = 'var_no_ktp'; break;
			case 3 : $order = 'var_nama'; break;
			case 4 : $order = 'var_departement'; break;
			case 5 : $order = 'var_section'; break;
			case 6 : $order = 'var_batch'; break;
			case 7 : $order = 'var_pekerjaan'; break;
			case 8 : $order = 'var_status_kerja'; break;
			case 9 : $order = 'dt_tanggal_akhir_k1,dt_tanggal_akhir_k2 '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($status_filter, $departement_filter, $section_filter, $filter_start_kontrak, $filter_end_kontrak, $filter = NULL){
		$expired_date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 30 days'));

		$this->db->select("*")
				->from($this->m_karyawan_kontrak." mkk")
				//->where("var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'TETAP', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3', 'TETAP (EXPATRIAT)')")
				->where("var_status_kerja IN ('KONTRAK 1', 'KONTRAK 2', 'KONTRAK 1 (IN CLASS)', 'KONTRAK 3')")
				->where("var_status_mp = 'ACTIVE'")
				->where("int_status = 1");
		
		if($status_filter != ""){ // filter
			$this->db->where('mkk.var_status_kerja', $status_filter);
		}
		
		if($departement_filter != ""){ // filter
			$this->db->where('mkk.var_departement', $departement_filter);
		}

		if($section_filter != ""){ // filter
			$this->db->where('mkk.var_section', $section_filter);
		}

		if(!empty($filter_start_kontrak) && !empty($filter_end_kontrak)){
			$this->db->where('((mkk.dt_tanggal_akhir_k1 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'") OR (mkk.dt_tanggal_akhir_k2 BETWEEN "'.$filter_start_kontrak.'" AND "'.$filter_end_kontrak.'"))');
			//$this->whereBetweenDate('dt_periode_kontrak1_awal', $filter_start_kontrak, $filter_end_kontrak);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mkk.var_nim', $filter)
					->or_like('mkk.var_nik', $filter)
					->or_like('mkk.var_nama', $filter)
					->or_like('mkk.var_no_ktp', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}
}
