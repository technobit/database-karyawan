<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Manpower_model extends MY_Model {

	public function departement_list(){
		$this->db->select("var_departement")
			->from($this->m_karyawan_kontrak)
			->group_by("var_departement");
		return $this->db->order_by('var_departement', 'ASC')->get()->result();
	}
	
	public function section_list(){
		$this->db->select("var_section")
			->from($this->m_karyawan_kontrak)
			->group_by("var_section");
		return $this->db->order_by('var_section', 'ASC')->get()->result();
	}
	
	public function batch_list(){
		$this->db->select("var_batch")
			->from($this->m_karyawan_kontrak)
			->group_by("var_batch");
		return $this->db->order_by('var_batch', 'ASC')->get()->result();
	}


    public function list($var_batch, $var_department, $var_section, $var_status_kerja, $limit, $offset){
        return $this->callProcedure("report_manpower_to_section(?,?,?,?,?,?)",
                                    [$var_batch, $var_department, $var_section, $var_status_kerja, $limit, $offset], 'result');
    }

    public function total($var_batch, $var_department, $var_section, $var_status_kerja){
		return $this->callProcedure("report_manpower_to_section_count(?,?,?,?)", [$var_batch, $var_department, $var_section, $var_status_kerja]);
    }
}