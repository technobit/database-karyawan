<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Karyawan_model extends MY_Model {

	public function status_kerja_list(){
		$this->db->select("var_status_kerja")
			->from($this->m_karyawan_kontrak)
			->group_by("var_status_kerja");
			return $this->db->order_by('var_status_kerja', 'ASC')->get()->result();
	}

	public function status_mp_list(){
		$this->db->select("var_status_mp")
			->from($this->m_karyawan_kontrak)
			->group_by("var_status_mp");
			return $this->db->order_by('var_status_mp', 'ASC')->get()->result();
	}

	public function batch_list(){
		$this->db->select("var_batch")
			->from($this->m_karyawan_kontrak)
			->group_by("var_batch");
			return $this->db->order_by('var_batch', 'ASC')->get()->result();
	}

	public function pekerjaan_list(){
		$this->db->select("var_pekerjaan")
			->from($this->m_karyawan_kontrak)
			->group_by("var_pekerjaan");
			return $this->db->order_by('var_pekerjaan', 'ASC')->get()->result();
	}

	public function departement_list(){
		$this->db->select("var_departement")
			->from($this->m_karyawan_kontrak)
			->group_by("var_departement");
		return $this->db->order_by('var_departement', 'ASC')->get()->result();
	}
	
	public function section_list(){
		$this->db->select("var_section")
			->from($this->m_karyawan_kontrak)
			->group_by("var_section");
		return $this->db->order_by('var_section', 'ASC')->get()->result();
	}
	
    public function list($var_status_kerja, $var_status_mp, $var_department, $var_section, $var_batch, $var_pekerjaan, $limit, $offset){
        return $this->callProcedure("report_rekap_data_karyawan(?,?,?,?,?,?,?,?)",
                                    [$var_status_kerja, $var_status_mp, $var_department, $var_section, $var_batch, $var_pekerjaan, $limit, $offset], 'result');
    }

    public function total($var_status_kerja, $var_status_mp, $var_department, $var_section, $var_batch, $var_pekerjaan){
		return $this->callProcedure("report_rekap_data_karyawan_count(?,?,?,?,?,?)", [$var_status_kerja, $var_status_mp, $var_department, $var_section, $var_batch, $var_pekerjaan]);
    }

}
