<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Absen_tk_model extends MY_Model {

	
	public function departement_list(){
		$this->db->select("var_departement")
			->from($this->m_karyawan_kontrak)
			->group_by("var_departement");
		return $this->db->order_by('var_departement', 'ASC')->get()->result();
	}
	
	public function batch_list(){
		$this->db->select("var_batch")
			->from($this->m_karyawan_kontrak)
			->group_by("var_batch");
		return $this->db->order_by('var_batch', 'ASC')->get()->result();
	}
	
    public function list($dt_awal, $dt_akhir){
		$migrate_tk = $this->migrate_tk();
        return $this->callProcedure("dashboard_notif_sp(?,?)",
                                    [$dt_awal, $dt_akhir], 'result');
    }

    function migrate_tk(){
        return $this->callProcedure("dashboard_migrate_absensi_tk_notif()", '');
    }
}
