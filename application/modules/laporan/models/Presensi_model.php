<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Presensi_model extends MY_Model {

	
	public function departement_list(){
		$this->db->select("var_departement")
			->from($this->m_karyawan_kontrak)
			->group_by("var_departement");
		return $this->db->order_by('var_departement', 'ASC')->get()->result();
	}
	
	public function batch_list(){
		$this->db->select("var_batch")
			->from($this->m_karyawan_kontrak)
			->group_by("var_batch");
		return $this->db->order_by('var_batch', 'ASC')->get()->result();
	}
	

    public function list($dt_awal, $dt_akhir, $var_batch, $var_department, $filter = '', $var_status_kerja, $var_status_mp){
        return $this->callProcedure("report_absensi_evaluasi_karyawan(?,?,?,?,?,?,?)",
                                    [$dt_awal, $dt_akhir, $var_batch, $var_department, $filter, $var_status_kerja, $var_status_mp], 'result');
    }

    public function list_tk($dt_awal, $dt_akhir, $var_batch, $var_department, $filter, $var_status_kerja, $var_status_mp){
        return $this->callProcedure("report_absensi_info_tk(?,?,?,?,?,?,?)",
                                    [$dt_awal, $dt_akhir, $var_batch, $var_department, $var_status_kerja, $var_status_mp, $filter], 'result');
    }
}
