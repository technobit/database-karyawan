<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
	
class Manpower extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'MANPOWER'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'laporan';
		$this->routeURL = 'manpower';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('manpower_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Manpower';
		$this->page->menu 	  = 'laporan';
		$this->page->submenu1 = 'manpower';
		$this->breadcrumb->title = 'Data Manpower';
		$this->breadcrumb->card_title = 'Data Karyawan';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Laporan', 'Manpower'];
		$this->js = true;
		$data['departement_list']	= $this->model->departement_list();
		$data['section_list']	= $this->model->section_list();
		$data['batch_list']	= $this->model->batch_list();
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}");
		$this->render_view('manpower/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		//$per_tanggal = $this->input->post('tanggal_filter', true);
		$total = $this->model->total($this->input->post('batch_filter', true), $this->input->post('departement_filter', true), $this->input->post('section_filter', true), $this->input->post('status_kerja_filter', true));
		$ldata = $this->model->list($this->input->post('batch_filter', true), $this->input->post('departement_filter', true), $this->input->post('section_filter', true), $this->input->post('status_kerja_filter', true), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = 0;
		foreach($ldata as $d){
			//if(date($per_tanggal) < date($d->dt_tanggal_out)){
				$i++;
				$data[] = array($i, $d->var_nim, $d->var_nik, $d->var_batch, $d->var_nama, $d->var_jenis_kelamin, $d->var_departement, $d->var_section, $d->var_status_foh, $d->var_jabatan, $d->var_pekerjaan, $d->var_status_kerja, $d->var_job, $d->var_shift);
			//}
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total->row_count,
								'iTotalDisplayRecords' => $total->row_count,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		$per_tanggal = date("Y-M-d H:i:s");//$this->input->post('tanggal_filter');

		$ldata = $this->model->list($this->input->post('batch_filter', true), $this->input->post('departement_filter', true), $this->input->post('section_filter', true), $this->input->post('status_kerja_filter', true), $this->input_post('length', true), $this->input_post('start', true));

        $title    = 'Data Aktif Karyawan';

        $filename = 'Data Aktif Karyawan - Per '.idn_date($per_tanggal, 'j F Y').'.xlsx';


		$input_file = 'assets/export/xlsx/data_aktif_karyawan.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 1;
        foreach($ldata as $d){
            $i++;
			$x++;

            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_nim);
            $sheet->setCellValue('C'.$x, $d->var_nik);
            $sheet->setCellValue('D'.$x, $d->var_batch);
            $sheet->setCellValue('E'.$x, $d->var_nama);
            $sheet->setCellValue('F'.$x, $d->var_jenis_kelamin);
            $sheet->setCellValue('G'.$x, $d->var_departement);
			$sheet->setCellValue('H'.$x, $d->var_section);
			$sheet->setCellValue('I'.$x, $d->var_status_foh);
			$sheet->setCellValue('J'.$x, $d->var_jabatan);
			$sheet->setCellValue('K'.$x, $d->var_pekerjaan);
			$sheet->setCellValue('L'.$x, $d->var_status_kerja);
			$sheet->setCellValue('M'.$x, $d->var_job);
			$sheet->setCellValue('N'.$x, $d->var_shift);		
        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
