<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Absen_tk extends MX_Controller {
	private $input_file_name = 'data_ktp';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'ABSEN-TK'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'laporan';
		$this->routeURL = 'absen_tk';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('absen_tk_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Absen Tanpa Keterangan';
		$this->page->menu 	  = 'laporan';
		$this->page->submenu1 = 'absen_tk';
		$this->breadcrumb->title = 'Absen Tanpa Kerangan';
		$this->breadcrumb->card_title = 'Data Karyawan Absen Tanpa Kerangan Lebih Dari 3x Berturut-turut';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Laporan', 'Absen Tanpa Kerangan'];
		$this->js = true;
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}");
		$this->render_view('absen_tk/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 
		if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$return_tk = array();
		$ldata = $this->model->list($filter_start, $filter_end);

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$arr_tgl = explode(",", str_replace(" ", "", $d->tanggal));
			$arr_kode = explode(",", str_replace(" ", "", $d->var_kode_pokjakd));
			$check_tk = $this->check_tk($arr_tgl, $arr_kode);

			if(!empty($check_tk)){
				$i++;
				$return_tk[] = $check_tk;
				$data[] = array($i, $d->var_batch, $d->var_departement, $d->var_nik, $d->var_nama, $check_tk['count_tk'].' Hari', implode("; ",str_replace(" ", "",$check_tk['tgl_tk'])), implode("; ",$check_tk['data_tk']));
			}
		}
		$count_kar_tk = $i;
		$this->set_json(array( 'stat' => TRUE,
								'aaData' => $data,
								'karyawan_tk' => $count_kar_tk,
								'return_tk' => $return_tk,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function check_tk($arr_tgl, $arr_kode){
		$val_tk = 0;
		$data  = array();
		$data_tk = array();
		$tgl_tk = array();

		$xlimit = count($arr_tgl);
		for ($x = 0 ; $x < $xlimit; $x++){
			$date_now = $arr_tgl[$x];

			if(isset($arr_tgl[$x-1])){ $date_mundur1 = $arr_tgl[$x-1];
				$cek_mundur1 = $this->diff_check($date_now, $date_mundur1);
			}else{ $cek_mundur1 = 0; }

			if(isset($arr_tgl[$x+1])){ $date_maju1 = $arr_tgl[$x+1];
				$cek_maju1 = $this->diff_check($date_now, $date_maju1);
			}else{ $cek_maju1 = 0; }

			if(isset($arr_tgl[$x-2])){ $date_mundur2 = $arr_tgl[$x-2];
				$cek_mundur2 = $this->diff_check($date_now, $date_mundur2);
			}else{ $cek_mundur2 = 0; }

			if(isset($arr_tgl[$x+2])){ $date_maju2 = $arr_tgl[$x+2];
				$cek_maju2 = $this->diff_check($date_now, $date_maju2);
			}else{ $date_maju2 = 0; }


			if(($cek_mundur1  == '1' && $cek_maju1 == '1') || ($cek_mundur1  == '1' && $cek_mundur2 == '2') || ($cek_maju1  == '1' && $cek_maju2 == '2')){
				array_push($data_tk,$arr_kode[$x]);
				array_push($tgl_tk, idn_date($arr_tgl[$x], "d/m/Y"));
				$val_tk = $val_tk + 1;
			}
		}

		if($val_tk >= 3){
			$count_arr = array_count_values($data_tk);
			
			if(isset($count_arr['TK'])){
				$count_tk = $count_arr['TK'];
				if($count_tk >= 3){
					$data['count_tk'] = $count_tk;
					$data['data_tk'] = $data_tk;
					$data['tgl_tk'] = $tgl_tk;
					return $data;
				}
			}
		}
		return false;
	}
	
	function diff_check($dt1 = '', $dt2 = ''){
		if($dt1 != '' || $dt2 != ''){
			$date1 = date_create($dt1);
			$date2 = date_create($dt2);
			$diff = date_diff($date1,$date2);
			return $diff->format("%a");
		}else{
			return 0;
		}
	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$ldata = $this->model->list($filter_start, $filter_end);

        $title    = 'Absen Tanpa Keterangan';

        $filename = 'Absen Tanpa Keterangan '.idn_date($filter_start, 'j F Y').' - '.idn_date($filter_end, 'j F Y').'.xlsx';


		$input_file = 'assets/export/xlsx/absen_tk.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 1;
		foreach($ldata as $d){
			$arr_tgl = explode(",", str_replace(" ", "", $d->tanggal));
			$arr_kode = explode(",", str_replace(" ", "", $d->var_kode_pokjakd));
			$check_tk = $this->check_tk($arr_tgl, $arr_kode);

			if(!empty($check_tk)){
				$i++;
				$x++;
	
				$sheet->setCellValue('A'.$x, $i);
				$sheet->setCellValue('B'.$x, $d->var_batch);
				$sheet->setCellValue('C'.$x, $d->var_nik);
				$sheet->setCellValue('D'.$x, $d->var_nama);
				$sheet->setCellValue('E'.$x, $d->var_departement);
				$sheet->setCellValue('F'.$x, $d->var_section);
				$sheet->setCellValue('G'.$x, $d->var_status_foh);
				$sheet->setCellValue('H'.$x, $d->var_jabatan);
				$sheet->setCellValue('I'.$x, $d->var_pekerjaan);
				$sheet->setCellValue('J'.$x, $check_tk['count_tk'].' Hari');
				$sheet->setCellValue('K'.$x, implode("; ",str_replace(" ", "",$check_tk['tgl_tk'])));
				$sheet->setCellValue('L'.$x, implode("; ",$check_tk['data_tk']));

				//$return_tk[] = $check_tk;
				//$data[] = array($i, $d->var_batch, $d->var_departement, $d->var_nik, $d->var_nama, $check_tk['count_tk'].' Hari', implode("; ",str_replace(" ", "",$check_tk['tgl_tk'])), implode("; ",$check_tk['data_tk']));
			}
		}

		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
