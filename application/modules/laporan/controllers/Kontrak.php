<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Kontrak extends MX_Controller {
	private $input_file_name = 'data_ktp';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'REKAP-KONTRAK'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'laporan';
		$this->routeURL = 'rekap_kontrak';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('kontrak_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Data Karyawan Kontrak';
		$this->page->menu 	  = 'laporan';
		$this->page->submenu1 = 'rekap_kontrak';
		$this->breadcrumb->title = 'Data Karyawan Kontrak';
		$this->breadcrumb->card_title = 'Data Karyawan Kontrak';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Laporan', 'Karyawan Kontrak'];
		$this->js = true;
		$data['status_list']	= $this->model->status_list();
		$data['departement_list']	= $this->model->departement_list();
		$data['section_list']	= $this->model->section_list();
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}");
		$this->render_view('kontrak/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		if($this->input->post('date_filter_kontrak') != ""){
			//if(strpos($this->input->post('date_filter_kontrak', true), '~') !== false){
				list($filter_start_kontrak, $filter_end_kontrak) = explode(' ~ ', $this->input->post('date_filter_kontrak', true));
			//}

			$filter_start_kontrak	= convertValidDate($filter_start_kontrak, 'd-m-Y', 'Y-m-d').' 00:00:00';
			$filter_end_kontrak		= convertValidDate($filter_end_kontrak, 'd-m-Y', 'Y-m-d').' 23:59:59';
		}else{
			$filter_start_kontrak	= "";
			$filter_end_kontrak		= "";	
		}

		$data  = array();
		$total = $this->model->listCount($this->input->post('status_filter', true), $this->input->post('departement_filter', true), $this->input->post('section_filter', true), $filter_start_kontrak, $filter_end_kontrak, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('status_filter', true), $this->input->post('departement_filter', true), $this->input->post('section_filter', true), $filter_start_kontrak, $filter_end_kontrak, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			if($d->var_status_kerja == 'KONTRAK 1'){
				$akhir_kontrak = idn_date($d->dt_tanggal_akhir_k1, "j F Y");
			}else if($d->var_status_kerja == 'KONTRAK 2'){
				$akhir_kontrak = idn_date($d->dt_tanggal_akhir_k2, "j F Y");
			}else{
				$akhir_kontrak = '-';
			}
			$data[] = array($i, $d->var_nik, $d->var_no_ktp, $d->var_nama, $d->var_departement, $d->var_section, $d->var_batch, $d->var_pekerjaan, $d->var_status_kerja, $akhir_kontrak);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		if($this->input->post('date_filter_kontrak') != ""){
			//if(strpos($this->input->post('date_filter_kontrak', true), '~') !== false){
				list($filter_start_kontrak, $filter_end_kontrak) = explode(' ~ ', $this->input->post('date_filter_kontrak', true));
			//}

			$filter_start_kontrak	= convertValidDate($filter_start_kontrak, 'd-m-Y', 'Y-m-d').' 00:00:00';
			$filter_end_kontrak		= convertValidDate($filter_end_kontrak, 'd-m-Y', 'Y-m-d').' 23:59:59';
		}else{
			$filter_start_kontrak	= "";
			$filter_end_kontrak		= "";	
		}
		
		$ldata = $this->model->list($this->input->post('status_filter', true), $this->input->post('departement_filter', true), $this->input->post('section_filter', true), $filter_start_kontrak, $filter_end_kontrak, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

        $title    = 'Karyawan Kontrak';

        $filename = 'Karyawan Kontrak - '.idn_date(date("Y-M-d H:i:s"), 'j F Y H.i.s').'.xlsx';


		$input_file = 'assets/export/xlsx/data_karyawan.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0);

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 1;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
            $i++;
			$x++;

            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_nim);
            $sheet->setCellValue('C'.$x, $d->var_nik);
            $sheet->setCellValue('D'.$x, $d->var_batch);
            $sheet->setCellValue('E'.$x, $d->var_nama);
            $sheet->setCellValue('F'.$x, $d->var_jabatan);
			$sheet->setCellValue('G'.$x, $d->var_pekerjaan);
			if($d->dt_tanggal_masuk == '1970-01-01'|| $d->dt_tanggal_masuk == '0000-00-00'){
				$sheet->setCellValue('H'.$x, '');
			}else{
				$sheet->setCellValue('H'.$x, date_diff(date_create($d->dt_tanggal_masuk),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('H'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			if($d->dt_tanggal_awal_k1 == '1970-01-01' || $d->dt_tanggal_awal_k1 == '0000-00-00'){
				$sheet->setCellValue('I'.$x, '');
			}else{
				$sheet->setCellValue('I'.$x, date_diff(date_create($d->dt_tanggal_awal_k1),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('I'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			if($d->dt_tanggal_akhir_k1 == '1970-01-01' || $d->dt_tanggal_akhir_k1 == '0000-00-00'){
				$sheet->setCellValue('J'.$x, '');
			}else{
				$sheet->setCellValue('J'.$x, date_diff(date_create($d->dt_tanggal_akhir_k1),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('J'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			if($d->dt_tanggal_awal_k2 == '1970-01-01' || $d->dt_tanggal_awal_k2 == '0000-00-00'){
				$sheet->setCellValue('K'.$x, '');
			}else{
				$sheet->setCellValue('K'.$x, date_diff(date_create($d->dt_tanggal_awal_k2),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('K'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			if($d->dt_tanggal_akhir_k2 == '1970-01-01' || $d->dt_tanggal_akhir_k2 == '0000-00-00'){
				$sheet->setCellValue('L'.$x, '');
			}else{
				$sheet->setCellValue('L'.$x, date_diff(date_create($d->dt_tanggal_akhir_k2),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('L'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			if($d->dt_tanggal_tetap == '1970-01-01' || $d->dt_tanggal_tetap == '0000-00-00'){
				$sheet->setCellValue('M'.$x, '');
			}else{
				$sheet->setCellValue('M'.$x, date_diff(date_create($d->dt_tanggal_tetap),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('M'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			$sheet->setCellValue('N'.$x, $d->var_status_kerja);
			$sheet->setCellValue('O'.$x, $d->var_status_mp);
			if($d->dt_tanggal_out == '1970-01-01' || $d->dt_tanggal_out == '0000-00-00'){
				$sheet->setCellValue('P'.$x, '');
			}else{
				$sheet->setCellValue('P'.$x, date_diff(date_create($d->dt_tanggal_out),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('P'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			$sheet->setCellValue('Q'.$x, $d->var_sumber_data_laporan_mp);
			$sheet->setCellValue('R'.$x, $d->var_alasan_resign);
			$sheet->setCellValue('S'.$x, $d->var_status_foh);
			$sheet->setCellValue('T'.$x, $d->var_departement);
			$sheet->setCellValue('U'.$x, $d->var_section);
			$sheet->setCellValue('V'.$x, $d->var_job);
			$sheet->setCellValue('W'.$x, $d->var_shift);
			$sheet->setCellValue('X'.$x, $d->var_area);
			$sheet->setCellValue('Y'.$x, $d->var_line);
			$sheet->setCellValue('Z'.$x, $d->var_conveyor);
			$sheet->setCellValue('AA'.$x, $d->var_dusun);
			$sheet->setCellValue('AB'.$x, $d->var_rt_rw);
			$sheet->setCellValue('AC'.$x, $d->var_desa);
			$sheet->setCellValue('AD'.$x, $d->var_kecamatan);
			$sheet->setCellValue('AE'.$x, $d->var_kab_kota);
			$sheet->setCellValue('AF'.$x, $d->var_alamat_ktp);
			$sheet->setCellValue('AG'.$x, $d->var_lokasi_lahir);
			if($d->dt_tanggal_lahir == '1970-01-01' || $d->dt_tanggal_lahir == '0000-00-00'){
				$sheet->setCellValue('AH'.$x, '');
			}else{
				$sheet->setCellValue('AH'.$x, date_diff(date_create($d->dt_tanggal_lahir),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('AH'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			$sheet->setCellValueExplicit('AI'.$x, $d->var_telepon,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
			$sheet->setCellValue('AJ'.$x, $d->var_jenis_kelamin);
			$sheet->setCellValue('AK'.$x, $d->var_status_perkawinan);
			$sheet->setCellValue('AL'.$x, $d->var_agama);
			$sheet->setCellValue('AM'.$x, $d->var_sekolah);
			$sheet->setCellValue('AN'.$x, $d->var_jurusan);
			$sheet->setCellValue('AO'.$x, $d->var_nama_ibu);
			$sheet->setCellValue('AP'.$x, $d->var_nama_ayah);
			$sheet->setCellValue('AQ'.$x, $d->var_ket_mp_hamil);
			$sheet->setCellValue('AR'.$x, $d->var_status_ck_clt);
			$sheet->setCellValue('AS'.$x, $d->var_keterangan_ck_clt);
			if($d->dt_tanggal_mulai_ck_clt == '1970-01-01' || $d->dt_tanggal_mulai_ck_clt == '0000-00-00'){
				$sheet->setCellValue('AT'.$x, '');
			}else{
				$sheet->setCellValue('AT'.$x, date_diff(date_create($d->dt_tanggal_mulai_ck_clt),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('AT'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			if($d->dt_perkiraan_selesai_ck_clt == '1970-01-01' || $d->dt_perkiraan_selesai_ck_clt == '0000-00-00'){
				//$sheet->setCellValue('AU'.$x, $d->dt_perkiraan_selesai_ck_clt);
				$sheet->setCellValue('AU'.$x, '');
			}else{
				$sheet->setCellValue('AU'.$x, date_diff(date_create($d->dt_perkiraan_selesai_ck_clt),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('AU'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			$sheet->setCellValue('AV'.$x, $d->var_sumber_data_ck_clt);
			$sheet->setCellValue('AW'.$x, $d->var_ket_masakerja_eks_sai);
			$sheet->setCellValue('AX'.$x, $d->var_job_eks);
			$sheet->setCellValue('AY'.$x, $d->var_ket_difabel);
			$sheet->setCellValueExplicit('AZ'.$x, $d->var_no_ktp,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
			$sheet->setCellValueExplicit('BA'.$x, $d->var_no_kk,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
			$sheet->setCellValue('BB'.$x, $d->var_alamat_tinggal);
			$sheet->setCellValueExplicit('BC'.$x, $d->var_npwp,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
			$sheet->setCellValue('BD'.$x, $d->var_nama_suami_istri);
			$sheet->setCellValue('BE'.$x, $d->var_nama_anak_1);
			$sheet->setCellValue('BF'.$x, $d->var_nama_anak_2);
			$sheet->setCellValue('BG'.$x, $d->var_nama_anak_3);
			$sheet->setCellValue('BH'.$x, $d->var_email);
			$sheet->setCellValue('BI'.$x, $d->var_jumlah_anak);
			$sheet->setCellValue('BJ'.$x, $d->var_pic_spv);
			$sheet->setCellValue('BK'.$x, $d->var_jumlah_sp);
			$sheet->setCellValue('BL'.$x, $d->var_jenis_sp);
			if($d->dt_tanggal_sp == '1970-01-01' || $d->dt_tanggal_sp == '0000-00-00'){
				//$sheet->setCellValue('BM'.$x, $d->dt_tanggal_sp);
				$sheet->setCellValue('BM'.$x, '');
			}else{
				$sheet->setCellValue('BM'.$x, date_diff(date_create($d->dt_tanggal_sp),date_create("1900-01-01"))->format("%a") + 2);
				$sheet->getStyle('BM'.$x)->getNumberFormat()
						->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);
			}

			$sheet->setCellValue('BN'.$x, $d->var_status_dl_idl);
        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
