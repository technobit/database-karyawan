<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Presensi extends MX_Controller {
	private $input_file_name = 'data_ktp';
	private $import_dir = 'assets/import';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'REKAP-PRESENSI'; // kode customer pada tabel customer, 1 customer : 1 controller
		$this->module   = 'laporan';
		$this->routeURL = 'rekap_presensi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('presensi_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Rekap Presensi Karyawan';
		$this->page->menu 	  = 'laporan';
		$this->page->submenu1 = 'rekap_presensi';
		$this->breadcrumb->title = 'Rekap Presensi Karyawan';
		$this->breadcrumb->card_title = 'Rekap Presensi Karyawan';
		$this->breadcrumb->icon = 'fas fa-upload';
		$this->breadcrumb->list = ['Laporan', 'Rekapitulasi Presensi'];
		$this->js = true;
		$data['batch_list']	= $this->model->batch_list();
		$data['departement_list']	= $this->model->departement_list();
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}");
		$this->render_view('presensi/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 
		if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$ldata = $this->model->list($filter_start, $filter_end, $this->input->post('batch_filter', true), $this->input->post('departement_filter', true),$this->input_post('search[value]', TRUE), $this->input->post('status_kerja_filter', true), $this->input->post('status_mp_filter', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_batch, $d->var_nik, $d->var_nama, $d->var_departement, $d->var_carline, $d->var_job, $d->var_pekerjaan, $d->i, $d->s, $d->sd, $d->so, $d->tk, $d->grand_total, $d->var_pic);
		}
		$this->set_json(array( 'stat' => TRUE,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

		$filter_start	= convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end		= convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$ldata = $this->model->list($filter_start, $filter_end, $this->input->post('batch_filter', true), $this->input->post('departement_filter', true),$this->input_post('search_value', TRUE), $this->input->post('status_kerja_filter', true), $this->input->post('status_mp_filter', true));
		$tkdata = $this->model->list_tk($filter_start, $filter_end, $this->input->post('batch_filter', true), $this->input->post('departement_filter', true),$this->input_post('search_value', TRUE), $this->input->post('status_kerja_filter', true), $this->input->post('status_mp_filter', true));

		if($this->input->post('status_kerja_filter') == 'KONTRAK 1' || $this->input->post('status_kerja_filter') == 'KONTRAK 1 (IN CLASS)'){
			$awal_kontrak = idn_date($ldata[0]->dt_tanggal_awal_k1, 'j F Y');
			$akhir_kontrak = idn_date($ldata[0]->dt_tanggal_akhir_k1, 'j F Y');
		}else if($this->input->post('status_kerja_filter') == 'KONTRAK 2'){
			$awal_kontrak = idn_date($ldata[0]->dt_tanggal_awal_k2, 'j F Y');
			$akhir_kontrak = idn_date($ldata[0]->dt_tanggal_akhir_k2, 'j F Y');
		}else{
			$awal_kontrak = '-';
			$akhir_kontrak = '-';
		}

		$title    = 'Absensi';

        $filename = 'Absensi - '.idn_date(date("Y-M-d H:i:s"), 'j F Y H.i.s').'.xlsx';


		$input_file = 'assets/export/xlsx/evaluasi_absensi.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('E1', 'ABSEN '.$ldata[0]->var_status_kerja)
					->setCellValue('J1', 'TGL : '.idn_date(date("Y-M-d H:i:s"), 'j F Y'))
					->setCellValue('A6', 'ABSEN BATCH '.$this->input->post('batch_filter'))
					->setCellValue('A7', 'PERIODE ABSEN : '.idn_date($filter_start, 'j F Y').' - '.idn_date($filter_end, 'j F Y'))
					->setCellValue('A8', 'PERIODE '.$ldata[0]->var_status_kerja.' : '.$awal_kontrak.' - '.$akhir_kontrak)
					->setCellValue('A9', 'DUE DATE EVALUASI KEMBALI KE IR : '.idn_date($this->input->post('due_date', true), 'j F Y'));

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 11;
        foreach($ldata as $d){
            $i++;
			$x++;

            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_batch);
            $sheet->setCellValue('C'.$x, $d->var_nama);
            $sheet->setCellValue('D'.$x, $d->var_nik);
            $sheet->setCellValue('E'.$x, $d->var_departement);
            $sheet->setCellValue('F'.$x, $d->var_carline);
			$sheet->setCellValue('G'.$x, $d->var_job);
			$sheet->setCellValue('H'.$x, $d->var_pekerjaan);
			$sheet->setCellValue('I'.$x, $d->i);
			$sheet->setCellValue('J'.$x, $d->s);
			$sheet->setCellValue('K'.$x, $d->sd);
			$sheet->setCellValue('L'.$x, $d->so);
			$sheet->setCellValue('M'.$x, $d->tk);
			$sheet->setCellValue('N'.$x, $d->grand_total);
			$sheet->setCellValue('O'.$x, $d->var_pic);
			$sheet->insertNewRowBefore($x + 1, 1);
			
        }
		
        $xtk = $x + 11;
        foreach($tkdata as $tk){
            //$i++;
			$xtk++;

            $sheet->setCellValue('C'.$xtk, $tk->var_nama);
            $sheet->setCellValue('D'.$xtk, $tk->var_nik);
            $sheet->setCellValue('E'.$xtk, $tk->jumlah_tk);
            $sheet->setCellValue('F'.$xtk, $tk->tanggal_tk);
			$sheet->insertNewRowBefore($xtk + 1, 1);
        }

		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
