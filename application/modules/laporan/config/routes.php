<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['manpower']['get']               = 'laporan/manpower';
$route['manpower']['post']              = 'laporan/manpower/list';
$route['export/manpower']['post']       = 'laporan/manpower/export';

$route['rekap_karyawan']['get']         = 'laporan/karyawan';
$route['rekap_karyawan']['post']        = 'laporan/karyawan/list';
$route['export/rekap_karyawan']['post'] = 'laporan/karyawan/export';

$route['rekap_kontrak']['get']          = 'laporan/kontrak';
$route['rekap_kontrak']['post']         = 'laporan/kontrak/list';
$route['export/rekap_kontrak']['post']  = 'laporan/kontrak/export';

$route['rekap_presensi']['get']         = 'laporan/presensi';
$route['rekap_presensi']['post']        = 'laporan/presensi/list';
$route['export/rekap_presensi']['post'] = 'laporan/presensi/export';

$route['absen_tk']['get']               = 'laporan/absen_tk';
$route['absen_tk']['post']              = 'laporan/absen_tk/list';
$route['export/absen_tk']['post']       = 'laporan/absen_tk/export';