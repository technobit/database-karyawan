<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    function exportData(th){
        var table = $('#table_data').DataTable();
        var info = table.page.info();   
        var data_per_page = info.length;
        var start_record = info.page * data_per_page;
        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                status_kerja_filter : $('.status_kerja_filter').val(),
                status_mp_filter : $('.status_mp_filter').val(),
                departement_filter : $('.departement_filter').val(),
                section_filter : $('.section_filter').val(),
                batch_filter : $('.batch_filter').val(),
                pekerjaan_filter : $('.pekerjaan_filter').val(),
                start : start_record,
                length : data_per_page
            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }

    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "searching": false,
            "ordering": false,
            "pageLength": 50,
            "lengthMenu": [[50, 100, 250, 500, 1000, -1], [50, 100, 250, 500, 1000, "All"]],
            "scrollY": 350,
            "scrollX": true,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.status_kerja_filter = $('.status_kerja_filter').val();
					d.status_mp_filter = $('.status_mp_filter').val();
					d.departement_filter = $('.departement_filter').val();
					d.section_filter = $('.section_filter').val();
					d.batch_filter = $('.batch_filter').val();
					d.pekerjaan_filter = $('.pekerjaan_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                }
            ]
        });

        $('.status_kerja_filter').change(function(){
            dataTable.draw();
        });

        $('.status_mp_filter').change(function(){
            dataTable.draw();
        });
        
        $('.departement_filter').change(function(){
            dataTable.draw();
        });
        
        $('.section_filter').change(function(){
            dataTable.draw();
        });

        $('.batch_filter').change(function(){
            dataTable.draw();
        });

        $('.pekerjaan_filter').change(function(){
            dataTable.draw();
        });
        
    });
</script>