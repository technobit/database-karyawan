<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-sm btn-success" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-download"></i> Export Excel <i class="fas fa-file-excel"></i></button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-6">
								<div class="form-group row text-sm mb-0">
									<label for="status_filter" class="col-md-5 col-form-label">Status Kontrak</label>
									<div class="col-md-7">
                                        <select id="status_filter" name="status_filter" class="form-control form-control-sm status_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA KONTRAK -</option>
                                            <?php 
                                                foreach($status_list as $sl){
                                                    echo '<option value="'.$sl->var_status_kerja.'">'.$sl->var_status_kerja.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
                            </div>
							<div class="col-md-6">
								<div class="form-group row text-sm mb-0">
									<label for="departement_filter" class="col-md-4 col-form-label">Dept.</label>
									<div class="col-md-8">
                                        <select id="departement_filter" name="departement_filter" class="form-control form-control-sm departement_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA DEPARTEMENT -</option>
                                            <option value="ACCOUNTING">ACCOUNTING</option>
                                            <option value="ENGINEERING">ENGINEERING</option>
                                            <option value="FA">FA</option>
                                            <option value="FINANCE">FINANCE</option>
                                            <option value="MAINTENANCE">MAINTENANCE</option>
                                            <option value="NYS">NYS</option>
                                            <option value="PGA">PGA</option>
                                            <option value="PPIC">PPIC</option>
                                            <option value="PRODUCTION PREPARATION">PRODUCTION PREPARATION</option>
                                            <option value="PRODUKSI">PRODUKSI</option>
                                            <option value="QUALITY ASSURANCE">QUALITY ASSURANCE</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-6">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="date_filter_kontrak" class="col-md-5 col-form-label">Akhir Kontrak</label>
									<div class="col-md-1 pt-1">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="tanggal_kontrak_checkbox">
                                            <label class="custom-control-label" for="tanggal_kontrak_checkbox"></label>
                                        </div>
                                    </div>
									<div class="col-md-6">
										<input type="text" name="date_filter_kontrak" id="date_filter_kontrak" class="form-control form-control-sm date_filter_kontrak date_filter_next" disabled>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row text-sm mb-0">
									<label for="section_filter" class="col-md-4 col-form-label">Section</label>
									<div class="col-md-8">
                                        <select id="section_filter" name="section_filter" class="form-control form-control-sm section_filter select2" style="width: 100%;">
                                            <option value="">- SEMUA SECTION -</option>
                                            <?php 
                                                foreach($section_list as $sc){
                                                    echo '<option value="'.$sc->var_section.'">'.$sc->var_section.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>NIK KTP</th>
                            <th>Nama</th>
                            <th>Departemen</th>
                            <th>Section</th>
                            <th>Batch</th>
                            <th>Pekerjaan</th>
                            <th>Status</th>
                            <th>Akhir Kontrak</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
