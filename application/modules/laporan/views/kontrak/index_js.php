<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    function exportData(th){
        $('.form-message').html('');
        if(document.getElementById("date_filter_kontrak").disabled){date_filter_kontrak_val = '';
        }else{date_filter_kontrak_val = $('.date_filter_kontrak').val();}

        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                status_filter : $('.status_filter').val(),
                date_filter_kontrak : date_filter_kontrak_val,
                departement_filter : $('.departement_filter').val(),
                section_filter : $('.section_filter').val()
                //int_karyawan_kontrak_id : '83'
            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }

    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 100,
            "lengthMenu": [[100, 250, 500, 1000, -1], [100, 250, 500, 1000, "All"]],
            "scrollY": 275,
            "scrollX": true,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.status_filter = $('.status_filter').val();
					//d.date_filter_kontrak = $('.date_filter_kontrak').val();
					d.departement_filter = $('.departement_filter').val();
					d.section_filter = $('.section_filter').val();
                    if(document.getElementById("date_filter_kontrak").disabled){d.date_filter_kontrak = '';
                    }else{d.date_filter_kontrak = $('.date_filter_kontrak').val();}

                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                }
            ]
        });

        $('.status_filter').change(function(){
            dataTable.draw();
        });

        $('.departement_filter').change(function(){
            dataTable.draw();
        });
        
        $('.section_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });

        $('#tanggal_kontrak_checkbox').change(function() {
            if(this.checked) {
                enable_input("date_filter_kontrak")
                dataTable.draw();
            }else{
                disable_input("date_filter_kontrak")
                dataTable.draw();
            }
        });

        function disable_input(elm_id) {
            document.getElementById(elm_id).disabled = true;
        }

        function enable_input(elm_id) {
            document.getElementById(elm_id).disabled = false;
        }

    });
</script>