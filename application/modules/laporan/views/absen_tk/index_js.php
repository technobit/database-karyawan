<script>
    function exportData(th){
        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                date_filter : $('.date_filter').val()
            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }
    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "paging":   false,
            "searching": false,
            "ordering": false,
            "info": false,
            "scrollY": 480,
            "scrollX": true,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
                },
                "dataSrc": function(json) {
                    //alert(json.karyawan_tk);
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                    //$('.form-message').html(json.karyawan_tk);
                    //alert(json.karyawan_tk);
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "100",
                    "sClass": 'text-right',
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                }
            ]
        });

        $('.departement_filter').change(function(){
            dataTable.draw();
        });
        
        $('.batch_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>