<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-sm btn-success" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-download"></i> Export Excel <i class="fas fa-file-excel"></i></button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="form-message text-center"></div>
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="date_filter" class="col-md-6 col-form-label">Tanggal</label>
									<div class="col-md-6">
										<input type="text" name="date_filter" class="form-control form-control-sm date_filter_kontrak date_filter">
									</div>
								</div>
							</div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Batch</th>
                            <th>Departement</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Jumlah TK</th>
                            <th>Tanggal</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
