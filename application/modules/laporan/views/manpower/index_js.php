<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    var get_search_value = '';

    function exportData(th){
        var table = $('#table_data').DataTable();
        var info = table.page.info();   
        var data_per_page = info.length;
        var start_record = info.page * data_per_page;

        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                //tanggal_filter : $('.tanggal_filter').val(),
                batch_filter : $('.batch_filter').val(),
                departement_filter : $('.departement_filter').val(),
                section_filter : $('.section_filter').val(),
                status_kerja_filter : $('.status_kerja_filter').val(),
                start : start_record,
                length : data_per_page,
                search_value : get_search_value
            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }
    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
		$('.date_picker').daterangepicker(datepickModal);
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "searching": false,
            "ordering": false,
            "pageLength": 100,
            "lengthMenu": [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, "All"]],
            "scrollY": 450,
            "scrollX": true,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					//d.tanggal_filter = $('.tanggal_filter').val();
					d.batch_filter = $('.batch_filter').val();
					d.departement_filter = $('.departement_filter').val();
					d.section_filter = $('.section_filter').val();
					d.status_kerja_filter = $('.status_kerja_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                }
            ]
        });

        $('.tanggal_filter').change(function(){
            dataTable.draw();
        });

        $('.batch_filter').change(function(){
            dataTable.draw();
        });
        
        $('.departement_filter').change(function(){
            dataTable.draw();
        });

        $('.section_filter').change(function(){
            dataTable.draw();
        });

        $('.status_kerja_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
                get_search_value = $(this).val();
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>