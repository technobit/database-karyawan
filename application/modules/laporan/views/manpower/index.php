<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-sm btn-success" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-download"></i> Export Excel <i class="fas fa-file-excel"></i></button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-3">
								<div class="form-group row text-sm mb-0">
									<label for="batch_filter" class="col-md-4 col-form-label">Batch</label>
									<div class="col-md-8">
                                        <select id="batch_filter" name="batch_filter" class="form-control form-control-sm batch_filter select2" style="width: 100%;">
                                            <option value="0">- SEMUA BATCH -</option>
                                            <?php 
                                                foreach($batch_list as $bl){
                                                    echo '<option value="'.$bl->var_batch.'">'.$bl->var_batch.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-3">
								<div class="form-group row text-sm mb-0">
									<label for="departement_filter" class="col-md-4 col-form-label">Dept.</label>
									<div class="col-md-8">
                                        <select id="departement_filter" name="departement_filter" class="form-control form-control-sm departement_filter select2" style="width: 100%;">
                                            <option value="0">- SEMUA DEPARTEMENT -</option>
                                            <option value="ACCOUNTING">ACCOUNTING</option>
                                            <option value="ENGINEERING">ENGINEERING</option>
                                            <option value="FA">FA</option>
                                            <option value="FINANCE">FINANCE</option>
                                            <option value="MAINTENANCE">MAINTENANCE</option>
                                            <option value="NYS">NYS</option>
                                            <option value="PGA">PGA</option>
                                            <option value="PPIC">PPIC</option>
                                            <option value="PRODUCTION PREPARATION">PRODUCTION PREPARATION</option>
                                            <option value="PRODUKSI">PRODUKSI</option>
                                            <option value="QUALITY ASSURANCE">QUALITY ASSURANCE</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <!--<div class="col-md-3"></div>-->
                            <div class="col-md-3">
								<div class="form-group row text-sm mb-0">
									<label for="status_kerja_filter" class="col-md-5 col-form-label">Status Kerja</label>
									<div class="col-md-7">
                                        <select id="status_kerja_filter" name="status_kerja_filter" class="form-control form-control-sm status_kerja_filter select2" style="width: 100%;">
                                            <option value="0">- SEMUA STATUS -</option>
                                            <option value="IN CLASS">IN CLASS</option>
                                            <option value="KONTRAK 1">KONTRAK 1</option>
                                            <option value="KONTRAK 1 (IN CLASS)">KONTRAK 1 (IN CLASS)</option>
                                            <option value="KONTRAK 2">KONTRAK 2</option>
                                            <option value="KONTRAK 3">KONTRAK 3</option>
                                            <option value="MAGANG">MAGANG</option>
                                            <option value="MAGANG 1">MAGANG 1</option>
                                            <option value="MAGANG 2">MAGANG 2</option>
                                            <option value="TETAP">TETAP</option>
                                            <option value="TETAP (EXPATRIAT)">TETAP (EXPATRIAT)</option>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row text-sm mb-0">
									<label for="section_filter" class="col-md-4 col-form-label">Section</label>
									<div class="col-md-8">
                                        <select id="section_filter" name="section_filter" class="form-control form-control-sm section_filter select2" style="width: 100%;">
                                            <option value="0">- SEMUA SECTION -</option>
                                            <option value="ACCOUNTING">ACCOUNTING</option>
                                            <option value="ACCOUNTING ( TAX )">ACCOUNTING ( TAX )</option>
                                            <option value="ACCOUNTING (PRICING)">ACCOUNTING (PRICING)</option>
                                            <option value="DESIGN & STANDARD ENGINEERING">DESIGN & STANDARD ENGINEERING</option>
                                            <option value="ENGINEERING">ENGINEERING</option>
                                            <option value="EQUIPMENT CONTROL">EQUIPMENT CONTROL</option>
                                            <option value="EXIM">EXIM</option>
                                            <option value="FA">FA</option>
                                            <option value="FINANCE">FINANCE</option>
                                            <option value="GENERAL AFFAIR">GENERAL AFFAIR</option>
                                            <option value="HUMAN RESOURCES">HUMAN RESOURCES</option>
                                            <option value="IC">IC</option>
                                            <option value="INDUSTRIAL RELATION">INDUSTRIAL RELATION</option>
                                            <option value="INFORMATION TECHNOLOGI">INFORMATION TECHNOLOGI</option>
                                            <option value="INTERPRETER">INTERPRETER</option>
                                            <option value="MAINTENANCE">MAINTENANCE</option>
                                            <option value="MAINTENANCE & EQUIPMENT CONTROL">MAINTENANCE & EQUIPMENT CONTROL</option>
                                            <option value="MPC">MPC</option>
                                            <option value="NYS">NYS</option>
                                            <option value="PGA">PGA</option>
                                            <option value="PPC">PPC</option>
                                            <option value="PPIC">PPIC</option>
                                            <option value="PPIC & PURCHASE">PPIC & PURCHASE</option>
                                            <option value="PROCESS ENGINEERING">PROCESS ENGINEERING</option>
                                            <option value="PRODUCTION ENGINEERING">PRODUCTION ENGINEERING</option>
                                            <option value="PRODUCTION PREPARATION">PRODUCTION PREPARATION</option>
                                            <option value="PRODUCTION PREPARATION ">PRODUCTION PREPARATION </option>
                                            <option value="PRODUKSI">PRODUKSI</option>
                                            <option value="PURCHASE ENGINEERING">PURCHASE ENGINEERING</option>
                                            <option value="Q.S.A">Q.S.A</option>
                                            <option value="QC - CHECK WH">QC - CHECK WH</option>
                                            <option value="QC - INPECTION">QC - INPECTION</option>
                                            <option value="QC - MSA">QC - MSA</option>
                                            <option value="QC.  MSA">QC.  MSA</option>
                                            <option value="QC. CHECK W/H">QC. CHECK W/H</option>
                                            <option value="QC. INSPECT STANDARD">QC. INSPECT STANDARD</option>
                                            <option value="QC. LABORATORY">QC. LABORATORY</option>
                                            <option value="QC. RECEIVING">QC. RECEIVING</option>
                                            <option value="QSA">QSA</option>
                                            <option value="QUALITY ASSURANCE">QUALITY ASSURANCE</option>
                                            <option value="SECURITY">SECURITY</option>
                                            <option value="TRAINEE IN CLASS">TRAINEE IN CLASS</option>
                                            <option value="TRAINING">TRAINING</option>
                                            <option value="WAREHOUSE">WAREHOUSE</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <!--<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="tanggal_filter" class="col-md-4 col-form-label">Per Tanggal</label>
									<div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm date_picker tanggal_filter text-right" id="tanggal_filter" placeholder="Tanggal Masuk" name="tanggal_filter" />
									</div>
								</div>
                            </div>-->
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIM</th>
                            <th>NIK</th>
                            <th>Batch</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Departemen</th>
                            <th>Section</th>
                            <th>Status FOH</th>
                            <th>Jabatan</th>
                            <th>Pekerjaan</th>
                            <th>Status Kerja</th>
                            <th>Job</th>
                            <th>Shift</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
