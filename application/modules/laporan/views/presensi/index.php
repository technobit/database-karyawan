<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-sm btn-success" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-download"></i> Export Excel <i class="fas fa-file-excel"></i></button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="form-message text-center"></div>
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="date_filter" class="col-md-4 col-form-label">Tanggal</label>
									<div class="col-md-8">
										<input type="text" name="date_filter" class="form-control form-control-sm date_filter_kontrak date_filter">
									</div>
								</div>
							</div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="batch_filter" class="col-md-4 col-form-label">Batch</label>
									<div class="col-md-8">
                                        <select id="batch_filter" name="batch_filter" class="form-control form-control-sm batch_filter select2" style="width: 100%;">
                                            <option value="0">- PILIH BATCH -</option>
                                            <?php 
                                                foreach($batch_list as $bl){
                                                    echo '<option value="'.$bl->var_batch.'">'.$bl->var_batch.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="departement_filter" class="col-md-4 col-form-label">Dept.</label>
									<div class="col-md-8">
                                        <select id="departement_filter" name="departement_filter" class="form-control form-control-sm departement_filter select2" style="width: 100%;">
                                            <option value="0">- SEMUA DEPARTEMENT -</option>
                                            <option value="ACCOUNTING">ACCOUNTING</option>
                                            <option value="ENGINEERING">ENGINEERING</option>
                                            <option value="FA">FA</option>
                                            <option value="FINANCE">FINANCE</option>
                                            <option value="MAINTENANCE">MAINTENANCE</option>
                                            <option value="NYS">NYS</option>
                                            <option value="PGA">PGA</option>
                                            <option value="PPIC">PPIC</option>
                                            <option value="PRODUCTION PREPARATION">PRODUCTION PREPARATION</option>
                                            <option value="PRODUKSI">PRODUKSI</option>
                                            <option value="QUALITY ASSURANCE">QUALITY ASSURANCE</option>
                                            <option value="-">-</option>
                                        </select>
									</div>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="status_kerja_filter" class="col-md-4 col-form-label">Status Kerja</label>
									<div class="col-md-8">
                                        <select id="status_kerja_filter" name="status_kerja_filter" class="form-control form-control-sm status_kerja_filter select2" style="width: 100%;">
                                            <option value="0">- SEMUA STATUS -</option>
                                            <option value="IN CLASS">IN CLASS</option>
                                            <option value="KONTRAK 1">KONTRAK 1</option>
                                            <option value="KONTRAK 1 (IN CLASS)">KONTRAK 1 (IN CLASS)</option>
                                            <option value="KONTRAK 2">KONTRAK 2</option>
                                            <option value="KONTRAK 3">KONTRAK 3</option>
                                            <option value="MAGANG">MAGANG</option>
                                            <option value="MAGANG 1">MAGANG 1</option>
                                            <option value="MAGANG 2">MAGANG 2</option>
                                            <option value="TETAP">TETAP</option>
                                            <option value="TETAP (EXPATRIAT)">TETAP (EXPATRIAT)</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="status_mp_filter" class="col-md-4 col-form-label">Status MP</label>
									<div class="col-md-8">
                                        <select id="status_mp_filter" name="status_mp_filter" class="form-control form-control-sm status_mp_filter select2" style="width: 100%;">
                                            <option value="0">- SEMUA -</option>
                                            <option value="ACTIVE">ACTIVE</option>
                                            <option value="FAILED">FAILED</option>
                                            <option value="RESIGN">RESIGN</option>
                                        </select>
									</div>
								</div>
							</div>
                            <div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="due_date" class="col-md-4 col-form-label">Due Date</label>
									<div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm date_picker due_date text-right" id="due_date" placeholder="Due Date" name="due_date" />
									</div>
								</div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="table_data">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Batch</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Departement</th>
                            <th>Carline</th>
                            <th>Job</th>
                            <th>I</th>
                            <th>S</th>
                            <th>SD</th>
                            <th>SO</th>
                            <th>TK</th>
                            <th>Total</th>
                            <th>PIC</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
