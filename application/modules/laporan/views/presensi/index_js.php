<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
    var get_search_value = '';

    function exportData(th){
        if($('.batch_filter').val() != "0" && $('.status_kerja_filter').val() != "0"){
            $('.form-message').html('');
            let blc = $(th).data('block');
            blockUI(blc);
            $.AjaxDownloader({
                url  : $(th).data('url'),
                data : {
                    <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                    date_filter : $('.date_filter').val(),
                    batch_filter : $('.batch_filter').val(),
                    departement_filter : $('.departement_filter').val(),
                    status_kerja_filter : $('.status_kerja_filter').val(),
                    status_mp_filter : $('.status_mp_filter').val(),
                    due_date : $('.due_date').val(),
                    search_value : get_search_value
                }
            });
            setTimeout(function(){unblockUI(blc)}, 8000);
        }else{
            formmsg = {};
            formmsg.stat = false;
            formmsg.msg = "Batch dan Status Kerja Harus Dipilih";
            setFormMessage('.form-message', formmsg);
            //alert('jenis pekerjaan')
        }
    }

    var dataTable;
    $(document).ready(function() {
		$('.date_picker').daterangepicker(datepickModal);
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "dom": '<"top row"<"col-6"l><"col-6"f><"col-6"i>>rt<"bottom"p><"clear">',
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "paging":   false,
            "searching": true,
            "ordering": false,
            "info": false,
            "scrollY": 380,
            "scrollX": true,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.batch_filter = $('.batch_filter').val();
					d.departement_filter = $('.departement_filter').val();
					d.status_kerja_filter = $('.status_kerja_filter').val();
					d.status_mp_filter = $('.status_mp_filter').val();
					d.due_date = $('.due_date').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                }
            ]
        });

        $('.departement_filter').change(function(){
            dataTable.draw();
        });
        
        $('.batch_filter').change(function(){
            dataTable.draw();
        });

        $('.status_kerja_filter').change(function(){
            dataTable.draw();
        });

        $('.status_mp_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
                get_search_value = $(this).val();
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>