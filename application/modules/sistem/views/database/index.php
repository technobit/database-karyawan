<div class="container-fluid">
	<div class="row">
		<section class="col-lg-6">
			<div class="card card-outline card-danger">
				<div class="card-header">
					<h3 class="card-title mt-1">
						<i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
						Data Presensi
					</h3>
				</div><!-- /.card-header -->
				<div class="card-body">
					<h5>Jumlah data kehadiran yang terdapat dalam database diatas 3 bulan yang lalu sebanyak</h5>
					<h3><?=number_format($data_absensi->count_absensi, 0, ',', '.' )?> Data</h3>
					<h5>Agar sistem berjalan lebih optimal, silahkan hapus data tersebut.</h5>
					<button type="button" data-block="body" class="btn btn-sm btn-danger ajax_modal mt-1" data-url="<?=$url ?>" ><i class="fas fa-ban"></i> Hapus Data</button>
					<a href="<?=$url_refresh?>" type="button" class="btn btn-sm btn-success mt-1" ><i class="fas fa-sync"></i> Refresh Data</a>
				</div>
			</div>
		</section>
	</div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
