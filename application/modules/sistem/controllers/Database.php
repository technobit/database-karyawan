<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Database extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'USER-ADMIN'; // kode menu pada tabel menu, 1 menu : 1 controller
		$this->module 	= 'sistem';
		$this->routeURL	= 's_hapus_presensi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('database_model', 'model');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Hapus Database Presensi';
		$this->page->menu 	  = 'sistem';
		$this->page->submenu1 = 's_hapus_presensi';
		$this->breadcrumb->title = 'Hapus Database Presensi';
		$this->breadcrumb->list = ['Sistem', 'Hapus Database Presensi'];
		//$this->js = true;
		$data_absensi = $this->model->get();
		$data['data_absensi'] = $data_absensi;
		$data['url'] = site_url("{$this->routeURL}/{$data_absensi->count_absensi}/del");	
		$data['url_refresh'] = site_url("{$this->routeURL}");	
		$this->render_view('database/index', $data, true);
	}


	public function confirm($data_absensi){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$data['url']	= site_url("{$this->routeURL}/del");
		$data['title']	= 'Hapus data absensi';
		$data['url_refresh'] = site_url("{$this->routeURL}");	
		$data['info']	= [	'Jumlah data yang akan dihapus' => number_format($data_absensi, 0, ',', '.' ).' Data'];
		$this->load_view('database/index_delete', $data);
	}

	public function delete(){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete();
		$this->set_json([  'stat' => $check, 
							'mc' => true, //modal close
							'msg' => ($check)? "Data berhasil dihapus" : "Data tidak dapat dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
							
		
	}
}
