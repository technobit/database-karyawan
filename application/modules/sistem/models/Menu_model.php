<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Menu_model extends MY_Model {

	public function list($level = null, $parent = null, $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$order_by   = strtolower($order_by); 
		$sort       = (strtolower(trim($sort)) == 'asc')? 'ASC' : 'DESC';

		$this->db->select("	a.int_menu_id as menu_id, a.var_kode as kode, a.var_nama as nama, a.var_url as url, a.int_level as level, 
		                    a.int_urutan as urutan, b.var_nama as parent_name, a.var_class as class, a.var_icon as icon, a.is_active as is_aktif,
							CASE WHEN a.int_parent_id IS NULL THEN '' ELSE a.int_parent_id END as parent ")
					->from($this->s_menu. ' a ')
					->join($this->s_menu. ' b ', 'a.int_parent_id = b.int_menu_id ', 'left');

        if(!empty($level) && ctype_digit($level)){
            $this->db->where('a.int_level', $level);
        }

        if(!empty($parent)){
            $this->db->where('a.int_parent_id', $parent);
        }

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('a.var_nama', $filter)
					->or_like('a.var_url', $filter)
					->group_end();
		}

		$order = 'a.txturutan ';
		switch($order_by){
			case 1 : $order = 'a.var_kode '; break;
			case 2 : $order = 'a.var_nama '; break;
			case 3 : $order = 'a.var_url '; break;
			case 4 : $order = 'a.int_urutan '; break;
			case 5 : $order = 'b.var_nama '; break;
			case 6 : $order = 'a.is_active '; break;
			default : $order = 'a.int_urutan '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($level = null, $parent = null, $filter = NULL){
		$this->db->from($this->s_menu. ' a')
					->join($this->s_menu. ' b', 'a.int_parent_id = b.int_menu_id', 'left');

		if(!empty($level) && ctype_digit($level)){
		    $this->db->where('a.int_level', $level);
        }

        if(!empty($parent)){
            $this->db->where('a.int_parent_id', $parent);
        }

		if(!empty($filter)){ // filters 
	        $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('a.var_nama', $filter)
					->or_like('a.var_url', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function create($in){
        $col['var_kode']		= strtoupper($in['kode']);
        $col['var_nama']		= $in['nama'];
        $col['var_url']		= empty($in['url'])? NULL : $in['url'];
        $col['int_level']	= $in['level'];
        $col['int_urutan']	= $in['urutan'];
        $col['int_parent_id']	= empty($in['parent'])? NULL : $in['parent'];
        $col['var_class']	= $in['class'];
        $col['var_icon']		= $in['icon'];
        $col['is_active']	= $in['is_aktif'];

		$this->db->insert($this->s_menu, $col);
	}

	public function getLevelMenu(){
        return $this->db->query("SELECT DISTINCT int_level as level FROM	{$this->s_menu} ORDER BY int_level ASC")->result();
    }

    public function getParentMenu(){
        return $this->db->query("SELECT int_menu_id as menu_id, var_nama as nama, var_kode kode, int_level level FROM {$this->s_menu} WHERE var_url IS NULL ORDER BY int_level ASC, int_urutan ASC")->result();
    }

	public function get($menu_id){
		return $this->db->query("	SELECT 	a.int_menu_id as menu_id, a.var_kode as kode, a.var_nama as nama, a.var_url as url, a.int_urutan as urutan, a.int_level as level, a.var_class as class, a.var_icon as icon, a.is_active as is_aktif, a.int_parent_id as parent
									FROM	{$this->s_menu} a  
									WHERE	a.int_menu_id = ?", [$menu_id])->row();
	}

	public function update($menu_id, $in){
        $col['var_nama']		= $in['nama'];
        $col['var_url']		= empty($in['url'])? NULL : $in['url'];
        $col['int_level']	= $in['level'];
        $col['int_urutan']	= $in['urutan'];
        $col['int_parent_id']	= empty($in['parent'])? NULL : $in['parent'];
        $col['var_class']	= $in['class'];
        $col['var_icon']		= $in['icon'];
        $col['is_active']	= $in['is_aktif'];

		$this->db->trans_begin();
		$this->db->where('int_menu_id', $menu_id);
		$this->db->update($this->s_menu, $col);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($menu_id){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->s_menu} WHERE int_menu_id = ?", [$menu_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function getMapMenu($group_id, $tag = '', $parent_id = null){
		$this->db->select("m.int_menu_id as menu_id, m.var_url as url, m.var_nama as nama, m.int_level as level, m.int_urutan as urutan, (SELECT COUNT(*) FROM {$this->s_menu} mm WHERE mm.int_parent_id = m.int_menu_id) as sub, gm.c, gm.r, gm.u, gm.d")
							->from($this->s_menu.' m')
							->join($this->s_group_menu.' gm', "m.int_menu_id = gm.int_menu_id AND gm.int_group_id = {$this->db->escape($group_id)}", 'left', false)
							->where('m.is_active', 1)
							->order_by('m.int_urutan');
		if(empty($parent_id)){
			$this->db->where("(m.int_parent_id is null or m.int_level = 1)");
		} else {
			$this->db->where("(m.int_parent_id = {$this->db->escape($parent_id)} and m.int_level > 1)");
		}

		$query = $this->db->get();
		if($query->num_rows() > 0){
			$data = $query->result();

			foreach ($data as $d) {
                if(empty($d->url)){
                    $tag .= '<tr><td><span class="tree-ml-'.$d->level.'">'.$d->nama.'</span></td>'.
                        '<td><div class="icheck-success d-inline"><input class="r_act" name="'.$d->menu_id.'[r]" value="1" type="checkbox" id="r_'.$d->menu_id.'"  '.(($d->r)? 'checked' : '').'><label for="r_'.$d->menu_id.'"></label></div></td>'.
                        '<td>-</td>'.
                        '<td>-</td>'.
                        '<td>-</td>'.
                        '<td>-</td>'.
                        '</tr>';
                }else{
                    $tag .= '<tr><td><span class="tree-ml-'.$d->level.'">'.$d->nama.'</span></td>'.
                        '<td><div class="icheck-success d-inline"><input class="r_act" name="'.$d->menu_id.'[r]" value="1" type="checkbox" id="r_'.$d->menu_id.'"  '.(($d->r)? 'checked' : '').'><label for="r_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-success d-inline"><input class="c_act" name="'.$d->menu_id.'[c]" value="1" type="checkbox" id="c_'.$d->menu_id.'"  '.(($d->c)? 'checked' : '').'><label for="c_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-success d-inline"><input class="u_act" name="'.$d->menu_id.'[u]" value="1" type="checkbox" id="u_'.$d->menu_id.'"  '.(($d->u)? 'checked' : '').'><label for="u_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-success d-inline"><input class="d_act" name="'.$d->menu_id.'[d]" value="1" type="checkbox" id="d_'.$d->menu_id.'"  '.(($d->d)? 'checked' : '').'><label for="d_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-warning d-inline"><input class="all_line" value="'.$d->menu_id.'" type="checkbox" id="line_'.$d->menu_id.'"><label for="line_'.$d->menu_id.'"></label></div></td>'.
                        '</tr>';
                }

                $tag = $this->getMapMenu($group_id, $tag, $d->menu_id);
			}
		}
		return $tag;
	}

	public function setGroupMenu($group_id, $menu){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->s_group_menu} WHERE int_group_id = ?", [$group_id]);

		if(!empty($menu) && is_array($menu)){
			$ins = array();
			foreach($menu as $menu_id => $act){
				if($menu_id != 'all_c' || $menu_id != 'all_r' || $menu_id != 'all_u' || $menu_id != 'all_d'){
				$ins[] = [	'int_group_id' => $group_id,
							'int_menu_id' => $menu_id,
							'c' => isset($act['c'])? 1 : 0,
							'r' => isset($act['r'])? 1 : 0,
							'u' => isset($act['u'])? 1 : 0,
							'd' => isset($act['d'])? 1 : 0,];
				}
			}
			$this->db->insert_batch($this->s_group_menu, $ins);
		}
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}

	}
}
