<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Database_model extends MY_Model {

	public function get(){
		$data =  $this->db->query("SELECT COUNT(int_absensi_id) AS count_absensi
									FROM {$this->t_absensi} 
									WHERE dt_tanggal_trans <= NOW() - INTERVAL 3 MONTH")->row_array();
		return (object) $data;									
	}

	public function delete(){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->t_absensi} WHERE dt_tanggal_trans <= NOW() - INTERVAL 3 MONTH");

		$delete_notif = $this->delete_notif();
		if ($this->db->trans_status() === FALSE || $delete_notif === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete_notif(){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->t_absensi_notif} WHERE dt_tanggal_trans <= NOW() - INTERVAL 3 MONTH");
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
