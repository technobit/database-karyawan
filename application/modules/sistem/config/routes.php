<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['s_user']['get']          				= 'sistem/user';
$route['s_user']['post']         				= 'sistem/user/list';
$route['s_user/add']['get']      				= 'sistem/user/add';
$route['s_user/save']['post']    				= 'sistem/user/save';
$route['s_user/([a-zA-Z0-9]+)']['get']     		= 'sistem/user/get/$1';
$route['s_user/([a-zA-Z0-9]+)']['post']    		= 'sistem/user/update/$1';
$route['s_user/([a-zA-Z0-9]+)/del']['get'] 		= 'sistem/user/confirm/$1';
$route['s_user/([a-zA-Z0-9]+)/del']['post']		= 'sistem/user/delete/$1';

$route['s_menu']['get']          				= 'sistem/menu';
$route['s_menu']['post']         				= 'sistem/menu/list';
$route['s_menu/add']['get']      				= 'sistem/menu/add';
$route['s_menu/save']['post']    				= 'sistem/menu/save';
$route['s_menu/([a-zA-Z0-9]+)']['get']     		= 'sistem/menu/get/$1';
$route['s_menu/([a-zA-Z0-9]+)']['post']    		= 'sistem/menu/update/$1';
$route['s_menu/([a-zA-Z0-9]+)/del']['get'] 		= 'sistem/menu/confirm/$1';
$route['s_menu/([a-zA-Z0-9]+)/del']['post']		= 'sistem/menu/delete/$1';

$route['s_group']['get']          				= 'sistem/group';
$route['s_group']['post']         				= 'sistem/group/list';
$route['s_group/add']['get']      				= 'sistem/group/add';
$route['s_group/save']['post']    				= 'sistem/group/save';
$route['s_group/([a-zA-Z0-9]+)']['get']     	= 'sistem/group/get/$1';
$route['s_group/([a-zA-Z0-9]+)']['post']    	= 'sistem/group/update/$1';
$route['s_group/([a-zA-Z0-9]+)/del']['get'] 	= 'sistem/group/confirm/$1';
$route['s_group/([a-zA-Z0-9]+)/del']['post']	= 'sistem/group/delete/$1';
$route['s_group/([a-zA-Z0-9]+)/menu']['get']    = 'sistem/group/menu_get/$1';
$route['s_group/([a-zA-Z0-9]+)/menu']['post']   = 'sistem/group/menu_save/$1';

$route['s_excel']['get']          				= 'sistem/excel';
$route['s_excel']['post']         				= 'sistem/excel/list';
$route['s_excel/add']['get']      				= 'sistem/excel/add';
$route['s_excel/save']['post']    				= 'sistem/excel/save';
$route['s_excel/([a-zA-Z0-9]+)']['get']         = 'sistem/excel/get/$1';
$route['s_excel/([a-zA-Z0-9]+)']['post']        = 'sistem/excel/update/$1';
$route['s_excel/([a-zA-Z0-9]+)/del']['get']     = 'sistem/excel/confirm/$1';
$route['s_excel/([a-zA-Z0-9]+)/del']['post']    = 'sistem/excel/delete/$1';

$route['s_email']['get']	= 'sistem/email';
$route['s_email']['post']	= 'sistem/email/update';

$route['s_hapus_presensi']['get']                       = 'sistem/database';
$route['s_hapus_presensi/([a-zA-Z0-9]+)/del']['get']	= 'sistem/database/confirm/$1';
$route['s_hapus_presensi/del']['post']                  = 'sistem/database/delete';
